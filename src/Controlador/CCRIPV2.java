/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Logica.Dispositivo;
import Logica.ProtocoloRipV2.TablaRipV2;
import Logica.Router;
import Logica.automatas.RipV2.Exit;
import Logica.automatas.RipV2.HelpRip;
import Logica.automatas.RipV2.Id_llave;
import Logica.automatas.RipV2.Llavero;
import Logica.automatas.RipV2.NetW;
import Logica.automatas.RipV2.Nom_llave;
import Logica.automatas.RipV2.RouteRip;
import Logica.automatas.RipV2.Show;
import Logica.automatas.RipV2.V2;
import Vista.Terminal;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Esta clase será la encargada de la ejecución y verificación de los comandos
 * disponibles para la configuración Rip v2
 *
 * @author gabrield
 */
public class CCRIPV2 {

    private Terminal terminal;
    private Dispositivo router;//router sobre el que se ejecutan los comandos
    private String salida;//Variable que contiene los mensajes a mostra de los sucesos ocurridos en la ejecucion de los comandos
    private TablaRipV2 tabla; //Variable para LLamar los métodos imprimirTabla y CrearTabla de esta clase
    private ArrayList<String> ips;//variable para ir almacenando las ip que se digitan junto con el comando Network

    /**
     * Crea una instancia del controlador de los comandos Rip Version 2
     *
     * @param terminal Instacia de terminal que captura los comandos
     * @param router router sobre el cual se aplicaran clos comandos
     */
    public CCRIPV2(Terminal terminal, Dispositivo router) {
        this.terminal = terminal;
        this.router = router;
        salida = "";
        tabla = new TablaRipV2();
        ips = new ArrayList<>();
        Controlador.NIVEL = 0;

    }

    /**
     * Recibe el comando en cuestión y lo ejecuta caso de ser un comando valido
     *
     * @param comando comando a ejecutar
     * @return Retorna los mensajes de lo ocurido en la ejecucion del comando
     */
    public String comandoRipV2(String comando) {
        salida = null;
        StringTokenizer tokens = new StringTokenizer(comando, " ");
        Router r = (Router) router;
//        TablaRipV2 tabla = new TablaRipV2();
        if (new RouteRip(comando).inicio() /*&& config*/) {
            salida = "<p>Router rip" + "</p>";
            Controlador.NIVEL = 1;
        } else if (new V2(comando).inicio()) {
            if (Controlador.NIVEL == 1) {
                salida = "<p>Version 2" + "</p>";
                Controlador.NIVEL = 2;
            } else {
                salida = "<p>Comando no Valido para este Nivel" + "</p>";
            }
        } else if (new Show(comando).inicio()) {
            if (Controlador.NIVEL == 0) {
                salida = "<p>---------Tabla de Enrutamiento Rip V2----------------</p><p>" + tabla.imprimirTabla(r) + "</p>";
            } else {
                salida = "<p>Comando no Valido para este Nivel" + "</p>";
            }
        } else if (new NetW(comando).inicio() /*&& config*/) {
            if (Controlador.NIVEL == 2) {
                tokens.nextToken();
                String ip = tokens.nextToken();

                if (PruebaPuertos(ip)) {
                    ips.add(ip);
                    salida = "<p>" + comando + "</p>";
                    tabla.CrearTabla();
                } else {
                    salida = "<p>Direccion Asignada no le corresponde a ninguno de los puertos" + "</p>";
                }
            } else {
                salida = "<p>Comando no Valido para este Nivel" + "</p>";
            }

        } else if (new Exit(comando).inicio() /*&& config*/) {
            if (Controlador.NIVEL > 0) {

                if (Controlador.NIVEL == 2) {
                    if (Prueba_ipsPuerto() != ips.size()) {
                        salida = "<p>Hay redes conectadas que no has Configurado En el Protocolo+ \"</p>\"";
                    } else {
                        salida = "<p>Ha Salido del nivel " + Controlador.NIVEL + "</p>";
                        Controlador.NIVEL--;
                        ips = new ArrayList<>();
                        Controlador.CUENTARIPV2++;
                    }
                } else {
                    salida = "<p>Ha Salido del nivel " + Controlador.NIVEL + "</p>";
                    Controlador.NIVEL--;
                }
            } else {
                salida = "<p>Te encuentras en el nivel Cero (0)  xD" + "</p>";
            }

        } else if (new Id_llave(comando).inicio() /*&& config*/) {
            if (Controlador.NIVEL == 2) {
            } else {
                salida = "<p>Expresion no valida para este nivel" + "</p>";
            }

        } else if (new Llavero(comando).inicio() /*&& config*/) {
            if (Controlador.NIVEL == 2) {
            } else {
                salida = "<p>Expresion no valida para este nivel" + "</p>";
            }

        } else if (new Nom_llave(comando).inicio() /*&& config*/) {
            if (Controlador.NIVEL == 2) {
                if (Controlador.AUTENTIC == 0) {
                    Controlador.AUTENTIC_PW = comando;
                    Controlador.AUTENTIC = 1;
                    salida = "<p>" + comando + "</p>";
                } else {
                    if (Controlador.AUTENTIC_PW.equalsIgnoreCase(comando)) {
                        salida = "<p>" + comando + "</p>";
                    } else {
                        salida = "<p>Este Key-String no esta en la red</p>";
                    }
                }
            } else {
                salida = "<p>Expresion no valida para este nivel" + "</p>";
            }

        } else if (new HelpRip(comando).inicio() /*&& config*/) {

            salida = ""
                    + "<table color=white>"
                    + "<tr>"
                    + "<td class=\"greenwhite\" >router rip</td><td>Se ingresa al modo de configuracion del protocolo (Nivel 1)</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td class=\"greenwhite\" >version 2</td><td>indica version del Protocolo (Nivel 2)</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td class=\"greenwhite\" >network</td><td>Para Tabular Una Ip Directamete conectada alRouter</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td class=\"greenwhite\" >show ip route</td><td>Muestra la Tabla de enrutamiento del router</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td class=\"greenwhite\" >key chain</td><td>Coloca nombre al llavero de autenticacion</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td class=\"greenwhite\" >key</td><td>Coloca numero identificador a la autenticacion (mas de 2 digitos)</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td class=\"greenwhite\" >key-string</td><td>password de autenticacion</td>"
                    + "</tr>"
                    + "</table>";
        }
        return salida;
    }

    /**
     * verifica que la dirección asignada por el comando Network sí se encuentre
     * conectada físicamente al router en cuestión
     *
     * @param ip ip digitada junto con el comando network
     * @return retorna verdadero o falso segun el caso
     *
     */
    public boolean PruebaPuertos(String ip) {
        boolean si = false;
        for (int i = 0; i < 4; i++) {
            if (this.router.getPuertos().get(i).getDireccion().getIp().equals(ip)) {
                si = true;
            }
        }
        return si;
    }

    /**
     * verificar si al salir del comando Network aun hay ip conectadas a alguno
     * de los puertos del router sin ingresar por medio de este comando
     *
     */
    public int Prueba_ipsPuerto() {
        int si = 0;
        for (int j = 0; j < 4; j++) {
            if (!this.router.getPuertos().get(j).isDisponible()) {
                si++;
            }
        }
        return si;
    }
}
