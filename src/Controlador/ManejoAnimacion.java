/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Logica.Constantes.Constantes;
import Logica.Linea;
import Vista.PaquteGrafico;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

/**
 *
 * @author JulioFer
 */
public class ManejoAnimacion extends SwingWorker<Double, Object> {

    ArrayList<Integer> puntosX = new ArrayList<>();
    ArrayList<Integer> puntosY = new ArrayList<>();
    ArrayList<PaquteGrafico> paquteGraficos;
    JLabel paquete;
    JPanel ventana;

    public ArrayList<PaquteGrafico> getPaquteGraficos() {
        return paquteGraficos;
    }

    public void setPaquteGraficos(ArrayList<PaquteGrafico> paquteGraficos) {
        paquete.setVisible(false);
        ventana.remove(paquete);
        this.paquteGraficos = paquteGraficos;
        for (PaquteGrafico p : paquteGraficos) {
            System.out.println("añadiendo paquete: " + p.getPaquete());
            p.getPaquete().setVisible(true);
            this.ventana.add(p.getPaquete());
        }
    }

    public ManejoAnimacion(JPanel panel) {
        this.ventana = panel;
        this.paquete = new JLabel();
        this.paquete.setSize(32, 32);
        this.paquete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imágenes/paquete.png")));
        this.paquete.setLocation(40, 100);
        this.paquete.setVisible(false);
        this.ventana.add(paquete);
        ventana.repaint();

    }

    @Override
    public Double doInBackground() throws Exception {
        if (Controlador.PROTOCOLO == Constantes.FLOODING) {
            floodng();
        } else {
            resto();
        }
        return 0.0;
    }

    public void floodng() {
        try {
            Controlador.simulando = true;
            ventana.setEnabled(false);
            ArrayList<MouseListener> listeners = new ArrayList<>();
            ArrayList<MouseMotionListener> mouseListeners = new ArrayList<>();
            for (int i = 0; i < Controlador.graficos.size(); i++) {
                listeners.add(Controlador.graficos.get(i).getImagen().getMouseListeners()[0]);
                mouseListeners.add(Controlador.graficos.get(i).getImagen().getMouseMotionListeners()[0]);
                Controlador.graficos.get(i).getImagen().removeMouseListener(listeners.get(i));
                Controlador.graficos.get(i).getImagen().removeMouseMotionListener(mouseListeners.get(i));
            }
            boolean mover = true;
            while (mover) {
                int con = 0;
                for (int j = 0; j < paquteGraficos.size(); j++) {
                    PaquteGrafico p = paquteGraficos.get(j);
                    if (!p.mover()) {
                        JLabel paquete1 = p.getPaquete();
                        con++;
                        paquete1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imágenes/" + ((!p.isRuta()) ? "x" : "exito") + ".png")));
                    }
                }
                if (con == paquteGraficos.size()) {
                    mover = false;
                }
                Thread.sleep(4);
            }
            chulito();
            ventana.repaint();
            ventana.setEnabled(true);
            for (int i = 0; i < Controlador.graficos.size(); i++) {
                Controlador.graficos.get(i).getImagen().addMouseListener(listeners.get(i));
                Controlador.graficos.get(i).getImagen().addMouseMotionListener(mouseListeners.get(i));
            }
            Controlador.simulando = false;
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void chulito() {
        for (int i = 0; i < 10; i++) {
            try {
                for (int j = 0; j < paquteGraficos.size(); j++) {
                    PaquteGrafico p = paquteGraficos.get(j);
                    if (p.isRuta()) {
                        JLabel paquete1 = p.getPaquete();
                        p.mover();
                        paquete1.setVisible(false);
                        paquete1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imágenes/exito.png")));
                        paquete1.setVisible(true);
                        paquete1.repaint();


                    }
                }
                Thread.currentThread().sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(ManejoAnimacion.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        for (int j = 0; j < paquteGraficos.size(); j++) {
            PaquteGrafico p = paquteGraficos.get(j);
            JLabel paquete1 = p.getPaquete();
            paquete1.setVisible(true);
            ventana.remove(paquete1);
        }
    }

    public void resto() {
        try {
            Controlador.simulando = true;
            ventana.setEnabled(false);
            ArrayList<MouseListener> listeners = new ArrayList<>();
            ArrayList<MouseMotionListener> mouseListeners = new ArrayList<>();

            for (int i = 0; i < Controlador.graficos.size(); i++) {
                listeners.add(Controlador.graficos.get(i).getImagen().getMouseListeners()[0]);
                mouseListeners.add(Controlador.graficos.get(i).getImagen().getMouseMotionListeners()[0]);
                Controlador.graficos.get(i).getImagen().removeMouseListener(listeners.get(i));
                Controlador.graficos.get(i).getImagen().removeMouseMotionListener(mouseListeners.get(i));
            }

            for (int i = 0; i < puntosX.size(); i++) {
                paquete.setLocation(puntosX.get(i), puntosY.get(i));
                paquete.setVisible(true);
                Thread.sleep(5);
            }
            for (int i = 0; i < 10; i++) {
                paquete.setVisible(false);
                paquete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imágenes/exito.png")));
                paquete.setVisible(true);
                paquete.repaint();
                Thread.currentThread().sleep(50);

            }
            paquete.setVisible(false);
            ventana.remove(paquete);

            for (int i = 0; i < Controlador.graficos.size(); i++) {

                Controlador.graficos.get(i).getImagen().addMouseListener(listeners.get(i));
                //System.out.println("puso el mouselistener de "+ Controlador.graficos.get(i).getDispositivo().getNombre());
                Controlador.graficos.get(i).getImagen().addMouseMotionListener(mouseListeners.get(i));
                //System.out.println("puso el mousemotionlistener de "+ Controlador.graficos.get(i).getDispositivo().getNombre());
            }

            ventana.repaint();
            ventana.setEnabled(true);
            Controlador.simulando = false;
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void rectaSimple2(Linea linea) {

        int x0 = linea.getX1();
        int y0 = linea.getY1();
        int x1 = linea.getX2();
        int y1 = linea.getY2();

        int dx = x1 - x0;
        int dy = y1 - y0;

        puntosX.add(x0);
        puntosY.add(y0);

        if (Math.abs(dx) > Math.abs(dy)) {          // pendiente < 1
            float m = (float) dy / (float) dx;
            float b = y0 - m * x0;
            if (dx < 0) {
                dx = -1;
            } else {
                dx = 1;
            }
            while (x0 != x1) {
                x0 += dx;
                y0 = Math.round(m * x0 + b);
                puntosX.add(x0);
                puntosY.add(y0);

            }
        } else if (dy != 0) {                              // slope >= 1
            float m = (float) dx / (float) dy;      // compute slope
            float b = x0 - m * y0;
            if (dy < 0) {
                dy = -1;
            } else {
                dy = 1;
            }
            while (y0 != y1) {
                y0 += dy;
                x0 = Math.round(m * y0 + b);
                puntosX.add(x0);
                puntosY.add(y0);
            }
        }
    }
}