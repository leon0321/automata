/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Logica.Constantes.Constantes;
import Logica.Direccion;
import Logica.Dispositivo;
import Logica.Linea;
import Logica.ProtocoloBGP.BGP;
import Logica.ProtocoloBGP.Rute;
import Logica.Puerto;
import Logica.Router;
import Logica.automatas.bgp.Neighbor_IP_remote_as_Number;
import Logica.automatas.bgp.Network_IP;
import Logica.automatas.bgp.Router_bgp_Number;
import Logica.automatas.bgp.Set_local_prefe_Number;
import Logica.automatas.bgp.Set_med_Number;
import Logica.automatas.bgp.Set_weight_Number;
import Logica.tools.Enlace;
import Vista.Terminal;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Esta clase será la encargada de la ejecución y verificación de los comandos
 * disponibles para la configuración bgp
 *
 * @author leonardo ortega hernandez
 */
public final class CCBGP {

    private Dispositivo router;//router sobre el que se ejecutan los comandos
    private String salida;//Variable que contiene los mensajes a mostra de los sucesos ocurridos en la ejecucion de los comandos

    /**
     * Crea una instancia del controlador de los comandos BGP
     *
     * @param terminal Instacia de terminal que captura los comandos
     * @param router router sobre el cual se aplicaran clos comandos
     */
    public CCBGP(Terminal terminal, Dispositivo router) {
        this.router = terminal.getD();
        salida = "";
    }

    /**
     * Recibe el comando en cuestión y lo ejecuta caso de ser un comando valido
     *
     * @param comando comando a ejecutar
     * @return Retorna los mensajes de lo ocurido en la ejecucion del comando
     */
    public String comandoBGP(String comando) {
        salida = null;
        StringTokenizer tokens = new StringTokenizer(comando, " ");
        Router r = (Router) router;
        if (new Neighbor_IP_remote_as_Number(comando).inicio() /*&& config*/) {
            tokens.nextToken();
            String ip = tokens.nextToken();
            tokens.nextToken();
            String as_number = tokens.nextToken();
            for (int i = 0; i < r.getVecinos().size(); i++) {
                if (r.getVecinos().get(i).getClass() == Router.class) {
                    Router r1 = (Router) r.getVecinos().get(i);
                    Linea l = Enlace.find(r, r1);
                    if (l.getFinall() == r1) {
                        if (l.getpInicial().getDireccion().getIp().equalsIgnoreCase(ip)) {
                            r1.getConfigBGP().setAs_number(Integer.parseInt(as_number));
                            salida = "<p>Neighbor configurado</p>";
                            return salida;
                        }
                    } else if (l.getInicial() == r1) {
                        if (l.getpFinal().getDireccion().getIp().equalsIgnoreCase(ip)) {
                            r1.getConfigBGP().setAs_number(Integer.parseInt(as_number));
                            salida = "<p>Neighbor configurado</p>";
                            return salida;
                        }
                    }
                }
                salida = "<p>No existe un puerto con la ip: " + ip + "<p>";
            }
        } else if (new Network_IP(comando).inicio() /*&& config*/) {
            tokens.nextToken();
            String ip = tokens.nextToken();
            if (!existe(ip)) {
                for (int i = 0; i < r.getPuertos().size(); i++) {
                    Puerto puerto = r.getPuertos().get(i);
                    if (ip.equalsIgnoreCase(puerto.getDireccion().getIp())) {
                        String mascara = puerto.calcularMascara(ip);
                        r.getConfigBGP().setIpnetwork(new Direccion(ip, mascara));
                        salida = "<p>Se a modificado la direcion de red a: " + ip + "</p>";
                        r.getConfigBGP().setEnabled(true);
                        return salida;
                    }
                }
                salida = "<p>No existe ningun puerto con ip = " + ip + "</p>";
            } else {
                salida = "<p>La direcion ip-network [" + ip + "] ya esta asignada a otro router" + "</p>";
            }

        } else if (new Router_bgp_Number(comando).inicio() /*&& config*/) {
            tokens.nextToken();
            tokens.nextToken();
            String as_number = tokens.nextToken();
            r.getConfigBGP().setAs_number(Integer.parseInt(as_number));
            r.getConfigBGP().setEnabled(!(r.getConfigBGP().getIpnetwork().getIp().isEmpty()));
            salida = "<p>EL NUMERO DEL AS SE A CAMBIADO A:" + r.getConfigBGP().getAs_number() + "</p>";

        } else if (new Set_local_prefe_Number(comando).inicio() /*&& config*/) {
            tokens.nextToken();
            String local_prefe = tokens.nextToken();
            r.getConfigBGP().setLocal_pref(Integer.parseInt(local_prefe));
            salida = "<p>PREFERENCIA LOCAL SE ESTABLACIDO COMO: " + r.getConfigBGP().getLocal_pref() + "</p>";

        } else if (new Set_med_Number(comando).inicio() /*&& config*/) {
            tokens.nextToken();
            String METRICA = tokens.nextToken();
            r.getConfigBGP().setMed(Integer.parseInt(METRICA));
            salida = "<p>METRICA SE CAMBIO A: " + r.getConfigBGP().getMed() + "</p>";

        } else if (new Set_weight_Number(comando).inicio() /*&& config*/) {
            tokens.nextToken();
            String WEIGHT = tokens.nextToken();
            r.getConfigBGP().setWeight(Integer.parseInt(WEIGHT));
            salida = "<p>WEIGHT SE A ESTABLECIDO COMO: " + r.getConfigBGP().getWeight() + "</p>";

        } else if ("help".equalsIgnoreCase(comando)) {
            salida = "<table>"
                    + "<tr>"
                    + "<td class=\"greenwhite\">router bgp</td><td>Estable o modifica el numero bajo AS bajo el cula esta el router</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td class=\"greenwhite\">network</td><td>Establece una direcion ip para el router</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td class=\"greenwhite\" >set-local-prefe</td><td>Establace un valor para la precerencia local del AS</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td class=\"greenwhite\" >set-med</td><td>Modifica el valor para la metrica del AS</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td class=\"greenwhite\" >set-weight</td><td>Modifica el valor de peso para el router</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td class=\"greenwhite\" >bgp-values</td><td>Muestra los valores de la configuracion BGP</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td class=\"greenwhite\" >ibgp protocol</td><td>Estable el protocolo de enrutamiento interno</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td class=\"greenwhite\" >neighbor remote-as</td><td>define un veciono bgp</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td class=\"greenwhite\" >show ip bgp</td><td>Muestra la tabla BGP del router</td>"
                    + "</tr>"
                    + "</table>";

        } else if ("bgp-values".equalsIgnoreCase(comando)) {
            String s = "";
            if (r.getConfigBGP().getiBGP() == Constantes.RIP1) {
                s = "Routing Information Protocol vesrion 1";
            } else if (r.getConfigBGP().getiBGP() == Constantes.OSPF) {
                s = "Open Shortest Path First";
            } else if (r.getConfigBGP().getiBGP() == Constantes.RIP2) {
                s = "Routing Information Protocol vesrion 2";
            }
            salida = "<table class=\"bblack\" >"
                    + "<tr><td>AS-NUMBER</td><td class=\"greenwhite\" >" + r.getConfigBGP().getAs_number() + "</td></tr>"
                    + "<tr><td>IP-NETWORK</td><td class=\"greenwhite\" >" + r.getConfigBGP().getIpnetwork().getIp() + "</td></tr>"
                    + "<tr><td>LOCAL_PREF</td><td class=\"greenwhite\" >" + r.getConfigBGP().getLocal_pref() + "</td></tr>"
                    + "<tr><td>METRIC</td><td class=\"greenwhite\" >" + r.getConfigBGP().getMed() + "</td></tr>"
                    + "<tr><td>WEIGHT</td><td class=\"greenwhite\" >" + r.getConfigBGP().getWeight() + "</td></tr>"
                    + "<tr><td>iBGP</td><td class=\"greenwhite\" >" + s + "</td></tr>"
                    + "</table>";
        } else if ("ibgp protocol ripv1".equalsIgnoreCase(comando)) {
            r.getConfigBGP().setiBGP(Constantes.RIP1);
            salida = "<table class=\"bblack\"><tr><td>Protocolo interno establecido como:</td><td class=\"greenwhite\">RIP version 1</td></tr></table>";
        } else if ("ibgp protocol ripv2".equalsIgnoreCase(comando)) {
            r.getConfigBGP().setiBGP(Constantes.RIP2);
            salida = "<table class=\"bblack\" ><tr><td>Protocolo interno establecido como:</td><td class=\"greenwhite\" >RIP version 2</td></tr></table>";
        } else if ("ibgp protocol ospf".equalsIgnoreCase(comando)) {
            r.getConfigBGP().setiBGP(Constantes.OSPF);
            salida = "<table class=\"bblack\" ><tr><td>Protocolo interno establecido como:</td><td class=\"greenwhite\" >OSPF</td></tr></table>";
        } else if ("show ip bgp".equalsIgnoreCase(comando)) {
            salida = "<table>"
                    + "<tr>"
                    + "<td>Network</td><td>Metric</td><td>LocPrf</td><td>Weight</td><td>AS-Number</td><td>Origin</td><td>AS-Path</td>"
                    + "</tr>";
            for (int i = 0; i < Controlador.allDispositivos.size(); i++) {
                if (Controlador.allDispositivos.get(i).getClass() == Router.class) {
                    Router r1 = (Router) Controlador.allDispositivos.get(i);
                    if (r1.getConfigBGP().isEnabled()) {
                        String color = (r1 == r) ? "class=\"greenwhite\"" : "";
                        ArrayList rutes = BGP.createPaths(r, r1);
                        if (!rutes.isEmpty()) {
                            Rute rute = (rutes.size() > 1) ? BGP.selectsPaths(rutes) : (Rute) rutes.get(0);
                            rute.setOrigin();
                            salida += "<tr " + color + ">"
                                    + "<td>" + r1.getConfigBGP().getIpnetwork().getIp() + "/" + r1.getConfigBGP().getIpnetwork().getMascara() + "</td>"
                                    + "<td>" + r1.getConfigBGP().getMed() + "</td>"
                                    + "<td>" + r1.getConfigBGP().getLocal_pref() + "</td>"
                                    + "<td>" + r1.getConfigBGP().getWeight() + "</td>"
                                    + "<td>" + r1.getConfigBGP().getAs_number() + "</td>"
                                    + "<td>" + ((rute.getOrigin() == 2) ? "e" : "i") + "</td>"
                                    + "<td>" + rute.getAs_path() + "</td>"
                                    + "</tr>";
                        }
                    }
                }
            }
            salida += "</table>";
        }
        return salida;
    }

    /**
     * Determina si la ip de red del router BGP ya existe
     *
     * @param ip direccion ip a comprobar
     * @return Retorna true si la ip ya existe en otro router de borde
     */
    public static boolean existe(String ip) {
        for (int i = 0; i < Controlador.allDispositivos.size(); i++) {
            if (Controlador.allDispositivos.get(i).getClass() == Router.class) {
                Router router = (Router) Controlador.allDispositivos.get(i);
                String ip1 = router.getConfigBGP().getIpnetwork().getIp();
                if (ip1.equalsIgnoreCase(ip)) {
                    return true;
                }
            }
        }
        return false;
    }
}
