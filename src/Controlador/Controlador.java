/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Logica.*;
import Logica.Constantes.Constantes;
import Logica.ProtocoloOSPF.OSPF;
import Logica.automatas.*;
import Vista.VentanaPrincipal;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.text.html.HTMLEditorKit;

/**
 * Clase encargada de controlar los eventos que sucedan en la ventana principal
 * como son los comandos en consola, eventos del mouse, entre otros.
 *
 * @author Jorge
 */
public class Controlador {

    //<editor-fold defaultstate="collapsed" desc="Declaración de variables">
    //lista que contiene todos los dispositivos
    public static ArrayList<Dispositivo> allDispositivos = new ArrayList<>();
    //lista que contiene las referencias a todos los dispositivos graficos.
    public static ArrayList<DispositivoGrafico> graficos = new ArrayList<>();
    //lista que contiene todas las lineas
    public static ArrayList<Linea> lineas = new ArrayList<>();
    //Variable control para creacion de lineas (Evento)
    public static boolean CREAR_LINEA = false;
    public static boolean CREAR_LINEA_GRAFICA = false;
    //area de salida para mostrar cambios.
    public static JTextPane areaSalida;
    private static HTMLEditorKit editor;
    public static JTextArea field;
    //Almacena el dispositivo inicial del cual se va a crear la linea
    public static DispositivoGrafico lineaInicial;
    public static int PROTOCOLO = 0;
    private VentanaPrincipal ventanaPrincipal;
    public static int indicePuerto = 0;
    public static boolean estadoAnimacion = false;
    public static int indicePuertoInicial = 0;
    public static int indicePuertoFinal = 0;
    public static Linea lineaIntermedia = new Linea();
    //variable para controlar que no se escriban comandos mientras se simula
    public static boolean simulando = false;
    public static boolean primeraVez = true;
    // para rip v2
    public static int NIVEL = 0;
    public static int CUENTARIPV2 = 0;
    public static int AUTENTIC = 0;
    public static String AUTENTIC_PW = "";
    public static String salida = "";
    //</editor-fold>
    //TODO Crear metodo para inicializar el controlador. Bueno para los archivos

    /**
     * Se utiliza Solamente en la clase Venana Principal
     *
     * @param areaS - JTextPane. Area que hace referencia a la consola
     * @param principal - VentanaPrincipal. Ventana inicial del programa.
     */
    public Controlador(JTextPane areaS, VentanaPrincipal principal) {
        ventanaPrincipal = principal;
        Controlador.areaSalida = areaS;
        areaSalida.setContentType("text/html");
        editor = (HTMLEditorKit) areaSalida.getEditorKit();
        field = new JTextArea();
    }

    /**
     * Este se utiliza en todas las clases diferentes a ventanaPrincipal
     */
    public Controlador() {
    }

    public VentanaPrincipal getVentanaPrincipal() {
        return ventanaPrincipal;
    }

    public void setVentanaPrincipal(VentanaPrincipal ventanaPrincipal) {
        this.ventanaPrincipal = ventanaPrincipal;
    }

    //<editor-fold defaultstate="collapsed" desc="Comando">
    /**
     * Método para evaluar un comando.
     *
     * @param cadena con el comando a evaluar.
     * @return String con el resultado de la operación.
     */
    public String Comando(String cadena) {
        if (simulando) {
            return Constantes.ERROR_SIMULANDO;
        } else {
            String salida = "";
            String palabra = "";
            StringTokenizer tokens = new StringTokenizer(cadena, " ");



            if (tokens.countTokens() > 1) {
                OUTER:
                while (tokens.hasMoreTokens()) {

                    String actual = tokens.nextToken();
                    String siguiente = tokens.nextToken();
                    System.out.println("" + actual + " " + siguiente);
                    if (actual.equals("SEND") || actual.equals("send") || actual.equals("PROPERTIES") || actual.equals("properties")) {
                        System.out.println("encontrar");
                        palabra = actual;
                        break;

                    } else if (actual.equals("CREATE") || actual.equals("create") || actual.equals("DELETE") || actual.equals("delete")) {
                        if (!siguiente.equals("RELATION") && !siguiente.equals("relation") && !siguiente.equals("NODE") && !siguiente.equals("node") && !siguiente.equals("ROUTER") && !siguiente.equals("router") && !siguiente.equals("SWITCH") && !siguiente.equals("switch")) {
                            palabra = actual;
                            break OUTER;
                        } else {
                            palabra = actual + " " + siguiente;
                        }
                        break;

                    } else {
                        palabra = actual + " " + siguiente;
                        break;
                    }
                }
            } else {
                palabra = cadena;
            }
            System.out.println("palabra:" + palabra);
            switch (palabra) {
                case Constantes.COMANDO_C_NODE_1:
                    salida = comandoCreate(cadena, "Nodo");
                    break;
                case Constantes.COMANDO_C_NODE_2:
                    salida = comandoCreate(cadena, "Nodo");
                    break;    
                case Constantes.COMANDO_C_ROUTER_1:
                    salida = comandoCreate(cadena, "Router");
                    break;
                case Constantes.COMANDO_C_ROUTER_2:
                    salida = comandoCreate(cadena, "Router");
                    break;    
                case Constantes.COMANDO_C_SWITCH_1:
                    salida = comandoCreate(cadena, "Switch");
                    break;
                case Constantes.COMANDO_C_SWITCH_2:
                    salida = comandoCreate(cadena, "Switch");
                    break;    
                case Constantes.COMANDO_CREATE_RELATION_1:
                    salida = comandoCreateRelation(cadena);
                    break;
                case Constantes.COMANDO_CREATE_RELATION_2:
                    salida = comandoCreateRelation(cadena);
                    break;    
                case Constantes.COMANDO_DELETE_1:
                    salida = comandoDelete(cadena);
                    break;
                case Constantes.COMANDO_DELETE_2:
                    salida = comandoDelete(cadena);
                    break;    
                case Constantes.COMANDO_DELETE_RELATION_1:
                    salida = comandoDeleteRelation(cadena);
                    break;
                case Constantes.COMANDO_DELETE_RELATION_2:
                    salida = comandoDeleteRelation(cadena);
                    break;    
                case Constantes.COMANDO_PROPERTIES_1:
                    salida = comandoProperties(cadena);
                    break;
                case Constantes.COMANDO_PROPERTIES_2:
                    salida = comandoProperties(cadena);
                    break;    
                case Constantes.COMANDO_SEND_1:
                    salida = comandoSend(cadena);
                    break;
                case Constantes.COMANDO_SEND_2:
                    salida = comandoSend(cadena);
                    break;    
                //Estas dos opciones cierran la ventana
                case Constantes.COMANDO_EXIT_1:
                    ventanaPrincipal.dispose();
                    break;
                case Constantes.COMANDO_EXIT_2:
                    ventanaPrincipal.dispose();
                    break;    
                default:
                    salida = palabra + Constantes.ERROR_COMANDO;
            }

            return salida;
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="crearDispositivo">
    /**
     * Crear dispositivo
     *
     * @param dispositivo
     * @return String con el resultado de la operacion
     */
    public String crearDispositivo(DispositivoGrafico dispositivo) {

        //System.out.println("entro a crear dispositivo");
        if (dispositivo.getDispositivo().getClass() == Nodo.class) {
            dispositivo.getImagen().setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imágenes/PC.png")));
        } else if (dispositivo.getDispositivo().getClass() == Router.class) {
            dispositivo.getImagen().setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imágenes/ROUTER2.png")));
        } else if (dispositivo.getDispositivo().getClass() == Switch.class) {
            dispositivo.getImagen().setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imágenes/SWITCH2.png")));
        }

        dispositivo.getDispositivo().setX(dispositivo.getImagen().getX());
        dispositivo.getDispositivo().setY(dispositivo.getImagen().getY());

        Controlador.graficos.add(dispositivo);

        dispositivo.getPanelTrabajo().add(dispositivo.getImagen());
        dispositivo.getPanelTrabajo().repaint();
        dispositivo.mostrarNombre();
        dispositivo.getFrame().repaint();
        allDispositivos.add(dispositivo.getDispositivo());
        Controlador.actualizarTooltips();
        return ("Dispositivo " + dispositivo.getDispositivo().getNombre() + " creado con exito");
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="eliminarDispositivo">
    /**
     * Metodo para eliminar un dispositivo
     *
     * @param dispositivo
     * @return String Resultado de la operacion
     */
    public String eliminarDispositivo(DispositivoGrafico dispositivo) {
        ArrayList<Linea> lineasBorrar = new ArrayList<>();
        dispositivo.getImagen().setVisible(false);
        dispositivo.getImagen().repaint();
        dispositivo.getFrame().remove(dispositivo.getImagen());


        for (Linea linea1 : lineas) {
            if (linea1.getInicial() == dispositivo.getDispositivo() || linea1.getFinall() == dispositivo.getDispositivo()) {
                lineasBorrar.add(linea1);
                for (Dispositivo dis : allDispositivos) {
                    if (dis == linea1.getInicial()) {
                        for (int i = 0; i < dis.getPuertos().size(); i++) {

                            if (dis.getPuertos().get(i) == linea1.getpInicial()) {
                                dis.getPuertos().get(i).setDisponible(true);
                                break;
                            }
                        }
                    } else if (dis == linea1.getFinall()) {
                        for (int i = 0; i < dis.getPuertos().size(); i++) {

                            if (dis.getPuertos().get(i) == linea1.getpFinal()) {
                                dis.getPuertos().get(i).setDisponible(true);
                                break;
                            }
                        }

                    }
                }
            }
        }
        for (Linea linea : lineasBorrar) {
            lineas.remove(linea);
            dispositivo.getPanelTrabajo().repaint();
        }
        for (Dispositivo dispositivo1 : Controlador.allDispositivos) {

            for (Dispositivo dispositivo2 : dispositivo1.getVecinos()) {

                if (dispositivo2 == dispositivo.getDispositivo()) {
                    dispositivo1.getVecinos().remove(dispositivo.getDispositivo());
                    break;
                }

            }

        }
        for (DispositivoGrafico dispositivoGrafico : Controlador.graficos) {
            for (Dispositivo dispositivo1 : dispositivoGrafico.getDispositivo().getVecinos()) {
                if (dispositivo1 == dispositivo.getDispositivo()) {
                    dispositivoGrafico.getDispositivo().getVecinos().remove(dispositivo.getDispositivo());
                }
            }

        }

        Controlador.graficos.remove(dispositivo);
        Controlador.allDispositivos.remove(dispositivo.getDispositivo());
        Controlador.actualizarTooltips();
        return ("dispositivo " + dispositivo.getDispositivo().getNombre() + " Eliminado con Éxito");
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="propiedadesDispositivo">
    /**
     * Método para obtener las propiedades de un dispositivo
     *
     * @param dispositivo
     * @return String con las propiedades del dispositivo
     */
    public String propiedadesDispositivo(DispositivoGrafico dispositivo) {
        return dispositivo.getDispositivo().Propiedades();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="crearRelacion">
    public static void crearRelacion(DispositivoGrafico inicial, DispositivoGrafico finall) {
        try {
            String showInputDialog = JOptionPane.showInputDialog(finall.getImagen(), "Ingrese Peso : ");
            int peso = Integer.parseInt(showInputDialog);

            if (peso > 0) {
                inicial.getDispositivo().getPuertos().get(indicePuertoInicial).setDisponible(false);
                finall.getDispositivo().getPuertos().get(indicePuertoFinal).setDisponible(false);
                Linea linea = new Linea(inicial.getDispositivo(), finall.getDispositivo(), peso);
                linea.setpInicial(Controlador.lineaIntermedia.getpInicial());
                linea.setpFinal(Controlador.lineaIntermedia.getpFinal());

                linea.setX1(inicial.getImagen().getX() + inicial.getImagen().getWidth() / 2);// -Dispositivo.lineaInicial.getImagen().getWidth()/2
                linea.setY1(inicial.getImagen().getY() + inicial.getImagen().getHeight() / 2);//-Dispositivo.lineaInicial.getImagen().getHeight()/2
                linea.setX2(finall.getImagen().getX() + finall.getImagen().getWidth() / 2);//-this.getImagen().getWidth()/2
                linea.setY2(finall.getImagen().getY() + finall.getImagen().getHeight() / 2);//-this.getImagen().getHeight()/2
                linea.setPintar(true);
                //Se agrega la linea al controlador
                Controlador.lineas.add(linea);
                inicial.getPanelTrabajo().repaint();
                finall.getDispositivo().getVecinos().add(inicial.getDispositivo());
                inicial.getDispositivo().getVecinos().add(finall.getDispositivo());
                inicial.getImagen().setToolTipText(inicial.getDispositivo().toolTip());
                finall.getImagen().setToolTipText(finall.getDispositivo().toolTip());
                DispositivoGrafico.lineaActiva = false;
                Controlador.CREAR_LINEA = false;
                Controlador.CREAR_LINEA_GRAFICA = false;
                Controlador.lineaIntermedia = new Linea();
                Controlador.indicePuertoInicial = 0;
                Controlador.indicePuertoFinal = 0;
            } else {
                JOptionPane.showMessageDialog(finall.getImagen(), "Ingrese cantidad válida");
            }
            //Se crea la linea para luego ser dibujada
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(inicial.getFrame(), "Ingresar cantidad válida");
            DispositivoGrafico.lineaActiva = false;
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="eliminarRelacion">
    public void eliminarRelacion(DispositivoGrafico inicial, DispositivoGrafico eliminar) {
        for (Linea linea : lineas) {
            if ((linea.getInicial() == inicial.getDispositivo() && linea.getFinall() == eliminar.getDispositivo()) || (linea.getFinall() == inicial.getDispositivo() && linea.getInicial() == eliminar.getDispositivo())) {
                for (Dispositivo dispositivo : allDispositivos) {
                    if (dispositivo == linea.getInicial()) {
                        for (int i = 0; i < dispositivo.getPuertos().size(); i++) {

                            if (dispositivo.getPuertos().get(i) == linea.getpInicial()) {
                                dispositivo.getPuertos().get(i).setDisponible(true);
                            }
                        }
                    } else if (dispositivo == linea.getFinall()) {
                        for (int i = 0; i < dispositivo.getPuertos().size(); i++) {

                            if (dispositivo.getPuertos().get(i) == linea.getpFinal()) {
                                dispositivo.getPuertos().get(i).setDisponible(true);
                            }
                        }

                    }
                }
                linea.getInicial().getVecinos().remove(linea.getFinall());
                linea.getFinall().getVecinos().remove(linea.getInicial());
                lineas.remove(linea);
                inicial.getPanelTrabajo().repaint();
                break;
            }
        }
        Controlador.actualizarTooltips();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Comandos">
    public String comandoCreate(String Cadena, String tipo) {
        String resultado = "";
        String nombre = "";
        boolean aceptado = false;
        switch (tipo) {
            case "Nodo":
                CreateNode automataNodo = new CreateNode(Cadena);
                aceptado = automataNodo.inicio();
                nombre = automataNodo.getNombre();
                if (!aceptado) {
                    JOptionPane.showMessageDialog(null, "Comando inválido", "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                    resultado += ("Comando inválido<br>");
                    resultado += ("Detalle del error: " + automataNodo.getSalida());
                } else {//Si comando es valido
                    if (existNombre(nombre)) {
                        JOptionPane.showMessageDialog(null, "Nombre ya existe", "ERROR",
                                JOptionPane.ERROR_MESSAGE);
                        resultado += ("Nombre ya existe");
                    } else {//si el nombre no existe
                        Nodo nodo = new Nodo(260, 100);
                        nodo.setNombre(nombre);
                        //                Se crea el label de el dispositivo
                        JLabel imagen = new JLabel();
                        imagen.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                        imagen.setVerticalAlignment(javax.swing.SwingConstants.TOP);
                        imagen.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
                        imagen.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
                        imagen.setBounds(260, 100, 48, 48);
                        DispositivoGrafico dg = new DispositivoGrafico(nodo, imagen, getVentanaPrincipal(), getVentanaPrincipal().getZonaTrabajo());
                        dg.mostrarNombre();
                        resultado = crearDispositivo(dg);
                    }
                }
                break;
            case "Router":
                CreateRouter cr = new CreateRouter(Cadena);
                aceptado = cr.inicio();
                nombre = cr.getNombre();

                if (!aceptado) {
                    JOptionPane.showMessageDialog(null, "Comando inválido", "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                    resultado += ("Comando inválido<br>");
                    resultado += ("Detalle del error: " + cr.getSalida());
                } else {
                    if (existNombre(nombre)) {
                        JOptionPane.showMessageDialog(null, "Nombre ya existe", "ERROR",
                                JOptionPane.ERROR_MESSAGE);
                        resultado += ("Nombre ya existe");

                    } else {
                        Router router = new Router(260, 100);
                        router.setNombre(nombre);

                        JLabel imagen = new JLabel();
                        imagen.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                        imagen.setVerticalAlignment(javax.swing.SwingConstants.TOP);
                        imagen.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
                        imagen.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
                        imagen.setBounds(260, 100, 48, 48);
                        DispositivoGrafico dg = new DispositivoGrafico(router, imagen, getVentanaPrincipal(), getVentanaPrincipal().getZonaTrabajo());
                        dg.mostrarNombre();
                        resultado = (crearDispositivo(dg));


                    }
                }



                break;
            case "Switch":
                CreateSwitch cs = new CreateSwitch(Cadena);
                aceptado = cs.inicio();
                nombre = cs.getNombre();

                if (!aceptado) {
                    JOptionPane.showMessageDialog(null, "Comando inválido", "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                    resultado += ("Comando inválido<br>");
                    resultado += ("Detalle del error: " + cs.getSalida());

                } else {
                    if (existNombre(nombre)) {
                        JOptionPane.showMessageDialog(null, "Nombre ya existe", "ERROR",
                                JOptionPane.ERROR_MESSAGE);
                        resultado += ("Nombre ya existe");

                    } else {
                        Switch s = new Switch(260, 100);
                        s.setNombre(nombre);

                        JLabel imagen = new JLabel();
                        imagen.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                        imagen.setVerticalAlignment(javax.swing.SwingConstants.TOP);
                        imagen.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
                        imagen.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
                        imagen.setBounds(260, 100, 48, 48);
                        DispositivoGrafico dg = new DispositivoGrafico(s, imagen, getVentanaPrincipal(), getVentanaPrincipal().getZonaTrabajo());
                        dg.mostrarNombre();
                        resultado = (crearDispositivo(dg));

                    }
                }

                break;
            default:
                break;
        }

        return resultado;
    }

    public boolean existNombre(String nombre) {
        boolean result = false;

        for (Dispositivo dispositivo : allDispositivos) {
            if (dispositivo.getNombre().equals(nombre)) {
                result = true;
                break;
            }
        }

        return result;
    }

    public String comandoDelete(String Cadena) {
        delete delet = new delete(Cadena);
        boolean aceptado = delet.inicio();
        String nombre = delet.getNombre();
        String resultado = "";

        if (!aceptado) {
            JOptionPane.showMessageDialog(null, "Comando inválido", "ERROR",
                    JOptionPane.ERROR_MESSAGE);
            resultado += ("Comando inválido<br>");
            resultado += ("Detalle del error: " + delet.getSalida());
        } else {
            if (existNombre(nombre)) {
                for (DispositivoGrafico dispositivoGrafico : graficos) {
                    if (dispositivoGrafico.getDispositivo().getNombre().equals(nombre)) {
                        resultado = (eliminarDispositivo(dispositivoGrafico));
                        break;
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "No existe un dispositivo Creado con ese nombre", "ERROR",
                        JOptionPane.ERROR_MESSAGE);
                resultado += ("No existe un dispositivo Creado con ese nombre");
            }
        }
        return resultado;
    }

    public String comandoCreateRelation(String Cadena) {
        createRelation relation = new createRelation(Cadena);
        boolean aceptado = relation.inicio();
        String resultado = "";
        String dispositivoInicial = relation.getDispositivoInicial();
        String dispositivoFinal = relation.getDispositivoFinal();
        String pInicial = relation.getPort1();
        String pFinal = relation.getPort2();

        DispositivoGrafico auxInicial = null;
        DispositivoGrafico auxFinal = null;

        if (!aceptado) {
            JOptionPane.showMessageDialog(null, "Comando inválido", "ERROR",
                    JOptionPane.ERROR_MESSAGE);
            resultado += ("Comando inválido<br>");
            resultado += ("Detalle del error: " + relation.getSalida() + "<br>");
        } else {
            if (existNombre(dispositivoInicial) && existNombre(dispositivoFinal)) {
                if (dispositivoInicial.equals(dispositivoFinal)) {
                    JOptionPane.showMessageDialog(null, "Dispositivos iguales", "ERROR",
                            JOptionPane.ERROR_MESSAGE);
                    resultado += ("Dispositivos iguales<br>");

                } else {
                    for (DispositivoGrafico dispositivoGrafico : graficos) {
                        if (dispositivoGrafico.getDispositivo().getNombre().equals(dispositivoInicial)) {
                            auxInicial = dispositivoGrafico;
                        } else if (dispositivoGrafico.getDispositivo().getNombre().equals(dispositivoFinal)) {
                            auxFinal = dispositivoGrafico;
                        }
                    }
                    if (existLinea(auxInicial.getDispositivo(), auxFinal.getDispositivo())) {
                        JOptionPane.showMessageDialog(null, "Relacion ya creada", "Información",
                                JOptionPane.INFORMATION_MESSAGE);
                        resultado += ("Relacion ya creadada");
                    } else {
                        if (auxInicial.getDispositivo().getClass().getSimpleName().equals("Nodo") && auxFinal.getDispositivo().getClass().getSimpleName().equals("Nodo")) {
                            if (Integer.parseInt(pFinal) == 0 && Integer.parseInt(pInicial) == 0) {
                                if (auxInicial.getDispositivo().getPuertos().get(Integer.parseInt(pInicial)).isDisponible() && auxFinal.getDispositivo().getPuertos().get(Integer.parseInt(pFinal)).isDisponible()) {
                                    auxFinal.getDispositivo().getPuertos().get(Integer.parseInt(pFinal)).setDisponible(false);
                                    auxInicial.getDispositivo().getPuertos().get(Integer.parseInt(pInicial)).setDisponible(false);
                                    crearRelacion(auxInicial, auxFinal);
                                    //TODO ACTUALIZAR TOLLTIP
                                    JOptionPane.showMessageDialog(null, "Relación creada con éxito ", "Información",
                                            JOptionPane.INFORMATION_MESSAGE);
                                    resultado += ("Relación entre : " + auxInicial.getDispositivo().getNombre() + " <-> " + auxFinal.getDispositivo().getNombre());
                                } else {
                                    JOptionPane.showMessageDialog(null, "Algún puerto no está disponible ", "Información",
                                            JOptionPane.INFORMATION_MESSAGE);
                                    resultado += ("Algún puerto no está disponible");
                                }

                            } else {
                                JOptionPane.showMessageDialog(null, "Puerto inválido ", "Información",
                                        JOptionPane.INFORMATION_MESSAGE);
                                resultado += ("Puerto Inválido");
                            }
                        } else if (auxInicial.getDispositivo().getClass().getSimpleName().equals("Nodo") && !auxFinal.getDispositivo().getClass().getSimpleName().equals("Nodo")) {
                            if (Integer.parseInt(pInicial) == 0) {
                                if (auxInicial.getDispositivo().getPuertos().get(Integer.parseInt(pInicial)).isDisponible() && auxFinal.getDispositivo().getPuertos().get(Integer.parseInt(pFinal)).isDisponible()) {
                                    auxFinal.getDispositivo().getPuertos().get(Integer.parseInt(pFinal)).setDisponible(false);
                                    auxInicial.getDispositivo().getPuertos().get(Integer.parseInt(pInicial)).setDisponible(false);
                                    crearRelacion(auxInicial, auxFinal);
                                    //TODO ACTUALIZAR TOLLTIP
                                    JOptionPane.showMessageDialog(null, "Relación creada con éxito ", "Información",
                                            JOptionPane.INFORMATION_MESSAGE);
                                    resultado += ("Relación entre : " + auxInicial.getDispositivo().getNombre() + " <-> " + auxFinal.getDispositivo().getNombre());
                                } else {
                                    JOptionPane.showMessageDialog(null, "Algún puerto no está disponible ", "Información",
                                            JOptionPane.INFORMATION_MESSAGE);
                                    resultado += ("Algún puerto no está disponible");
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "Puerto inválido ", "Información",
                                        JOptionPane.INFORMATION_MESSAGE);
                                resultado += ("Puerto Inválido");
                            }
                            //CREAR LINEA


                        } else if (auxFinal.getDispositivo().getClass().getSimpleName().equals("Nodo") && !auxInicial.getDispositivo().getClass().getSimpleName().equals("Nodo")) {
                            //CREAR LINEA
                            if (Integer.parseInt(pFinal) == 0) {
                                if (auxInicial.getDispositivo().getPuertos().get(Integer.parseInt(pInicial)).isDisponible() && auxFinal.getDispositivo().getPuertos().get(Integer.parseInt(pFinal)).isDisponible()) {
                                    auxFinal.getDispositivo().getPuertos().get(Integer.parseInt(pFinal)).setDisponible(false);
                                    auxInicial.getDispositivo().getPuertos().get(Integer.parseInt(pInicial)).setDisponible(false);
                                    crearRelacion(auxInicial, auxFinal);
                                    //TODO ACTUALIZAR TOLLTIP
                                    JOptionPane.showMessageDialog(null, "Relación creada con éxito ", "Información",
                                            JOptionPane.INFORMATION_MESSAGE);
                                    resultado += ("Relación entre : " + auxInicial.getDispositivo().getNombre() + " <-> " + auxFinal.getDispositivo().getNombre());
                                } else {
                                    JOptionPane.showMessageDialog(null, "Algún puerto no está disponible ", "Información",
                                            JOptionPane.INFORMATION_MESSAGE);
                                    resultado += ("Algún puerto no está disponible");
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "Puerto inválido ", "Información",
                                        JOptionPane.INFORMATION_MESSAGE);
                                resultado += ("Puerto Inválido");
                            }

                        } else {

                            if (auxInicial.getDispositivo().getPuertos().get(Integer.parseInt(pInicial)).isDisponible() && auxFinal.getDispositivo().getPuertos().get(Integer.parseInt(pFinal)).isDisponible()) {
                                auxFinal.getDispositivo().getPuertos().get(Integer.parseInt(pFinal)).setDisponible(false);
                                auxInicial.getDispositivo().getPuertos().get(Integer.parseInt(pInicial)).setDisponible(false);
                                crearRelacion(auxInicial, auxFinal);
                                //TODO ACTUALIZAR TOLLTIP
                                JOptionPane.showMessageDialog(null, "Relación creada con éxito ", "Información",
                                        JOptionPane.INFORMATION_MESSAGE);
                                resultado += ("Relación entre : " + auxInicial.getDispositivo().getNombre() + " <-> " + auxFinal.getDispositivo().getNombre());
                            } else {
                                JOptionPane.showMessageDialog(null, "Algún puerto no está disponible ", "Información",
                                        JOptionPane.INFORMATION_MESSAGE);
                                resultado += ("Algún puerto no está disponible");
                            }
                        }
                    }
                }

            } else {
                JOptionPane.showMessageDialog(null, "Verificar nombre de los dispositivos", "ERROR",
                        JOptionPane.ERROR_MESSAGE);
                resultado += ("Verificar nombre de los dispositivos");
            }
        }

        return resultado;
    }

    public String comandoDeleteRelation(String Cadena) {

        deleteRelation relation = new deleteRelation(Cadena);
        boolean aceptado = relation.inicio();
        String resultado = "";
        String dispositivoInicial = relation.getDispositivoInicial();
        String dispositivoFinal = relation.getDispositivoFinal();

        DispositivoGrafico auxInicial = null;
        DispositivoGrafico auxFinal = null;

        if (!aceptado) {
            JOptionPane.showMessageDialog(null, "Comando inválido", "ERROR",
                    JOptionPane.ERROR_MESSAGE);
            resultado += ("Comando inválido<br>");
            resultado += ("Detalle del error: " + relation.getSalida());
        } else {
            if (existNombre(dispositivoInicial) && existNombre(dispositivoFinal)) {
                for (DispositivoGrafico dispositivoGrafico : graficos) {
                    if (dispositivoGrafico.getDispositivo().getNombre().equals(dispositivoInicial)) {
                        auxInicial = dispositivoGrafico;
                    } else if (dispositivoGrafico.getDispositivo().getNombre().equals(dispositivoFinal)) {
                        auxFinal = dispositivoGrafico;
                    }
                }
                if (existLinea(auxInicial.getDispositivo(), auxFinal.getDispositivo())) {
                    eliminarRelacion(auxInicial, auxFinal);
                    JOptionPane.showMessageDialog(null, "Relación eliminada con éxito ", "Información",
                            JOptionPane.INFORMATION_MESSAGE);
                    resultado += ("Relación eliminada entre : " + auxInicial.getDispositivo().getNombre() + " <-> " + auxFinal.getDispositivo().getNombre());

                } else {
                    JOptionPane.showMessageDialog(null, "Relacion no existe", "Información",
                            JOptionPane.INFORMATION_MESSAGE);
                    resultado += ("Relacion NO existe");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Verificar nombre de los dispositivos", "ERROR",
                        JOptionPane.ERROR_MESSAGE);
                resultado += ("Verificar nombre de los dispositivos");
            }
        }
        return resultado;
    }

    public String comandoProperties(String Cadena) {
        properties p = new properties(Cadena);
        boolean aceptado = p.inicio();
        String name = p.getDispositivo();
        String resultado = "";

        if (!aceptado) {
            JOptionPane.showMessageDialog(null, "Comando inválido", "ERROR",
                    JOptionPane.ERROR_MESSAGE);
            resultado += ("Comando inválido<br>");
            resultado += ("Detalle del error: " + p.getSalida());
        } else {
            if (existNombre(name)) {
                for (DispositivoGrafico dispositivoGrafico : graficos) {
                    if (dispositivoGrafico.getDispositivo().getNombre().equals(name)) {
                        resultado = propiedadesDispositivo(dispositivoGrafico);
                        break;
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "No existe un dispositivo Creado con ese nombre", "ERROR",
                        JOptionPane.ERROR_MESSAGE);
                resultado += ("No existe un dispositivo Creado con ese nombre");
            }
        }

        return resultado;
    }
    //</editor-fold>

    //TODO implementar la parte de la vista reutilizando el codigo
    //TODO Se daña por lo de la animacion.
    //<editor-fold defaultstate="collapsed" desc="Comando Send">
    private String comandoSend(String cadena) {

        String resultado = "";

        boolean aceptado = false;
        comandoSend cs = new comandoSend(cadena);
        aceptado = cs.inicio();
        String paquetes = cs.getCantidadPaquetes();
        String origen = cs.getIpInicial();
        String destino = cs.getIpFinal();

        resultado += "<br>Comando a evaluar: " + cadena + "<br>";
        boolean bandera = false;
        //Si el comando no es aceptado por el autómata
        if (!aceptado) {
            JOptionPane.showMessageDialog(null, "Comando inválido", "ERROR",
                    JOptionPane.ERROR_MESSAGE);
            resultado += ("Comando inválido<br>");
            resultado += ("Detalle del error: " + cs.getSalida() + "<br>");
        } else {
            //En este caso el comando si fué aceptado

            //Las ip no deben ser iguales
            if (cs.getIpInicial().equals(cs.getIpFinal())) {
                JOptionPane.showMessageDialog(null, "Las IP de destino y origen"
                        + " no deben ser iguales", "ERROR",
                        JOptionPane.ERROR_MESSAGE);
                resultado += ("Detalle del error: ip origen y destino iguales<br>");
                bandera = true;
            }

            //Los paquetes deben ser mayores que 0 y no negativos
            if (Integer.parseInt(cs.getCantidadPaquetes()) <= 0) {
                JOptionPane.showMessageDialog(null, "El número de paquetes a"
                        + " enviar no debe"
                        + " ser negativo ni cero", "ERROR",
                        JOptionPane.ERROR_MESSAGE);
                resultado += ("Detalle del error: número de paquetes inválido<br>");
                bandera = true;
            } //Si todo está bien
            else {
                if (!bandera) {

                    resultado += ("Sintaxis ok...<br><br>");
                    resultado += ("IP origen: " + cs.getIpInicial() + "<br>");
                    resultado += ("IP destino: " + cs.getIpFinal() + "<br>");
                    resultado += ("Paquetes a enviar: " + cs.getCantidadPaquetes() + "<br><br>");
                    //Se realiza el proceso de envío de paquetes

                    //Se verifica que los dispositivos existan
                    int indice1 = -1;
                    int indice2 = -1;
                    //TODO arreglarlo para las puertas de enlace
                    for (int i = 0; i < allDispositivos.size(); i++) {
                        for (Puerto puerto : allDispositivos.get(i).getPuertos()) {
                            if (puerto.getDireccion().getIp().equals(cs.getIpInicial())) {
                                indice1 = i;
                            }
                            if (puerto.getDireccion().getIp().equals(cs.getIpFinal())) {
                                indice2 = i;
                            }
                        }
                    }

                    //Si no se encontraron dispositivos
                    if (indice1 == -1 || indice2 == -1) {
                        resultado += ("Las IP introducidas deben existir previamente"
                                + " en un dispositivo."
                                + " Por favor cree y configure la red<br>");
                    } else {

                        //Antes de enviar se debe verificar que se esté enviando desde PC y hacía
                        //PC                        
                        if (allDispositivos.get(indice1).getClass() == Nodo.class
                                && allDispositivos.get(indice2).getClass() == Nodo.class) {
                            //Si se encuentran se llama al método del protocolo OSPF para el envío
                            try {
                                int cantidadPaquetes = Integer.parseInt(paquetes);
                                if (cantidadPaquetes <= 0) {
                                    JOptionPane.showMessageDialog(null, "Ingrese una cantidad  Válida paquetes", "Error", JOptionPane.ERROR_MESSAGE);
                                } else {

                                    Dispositivo last = null;
                                    Dispositivo dispositivoOriginal = null;

                                    for (int i = 0; i < allDispositivos.size(); i++) {
                                        for (Puerto puerto : allDispositivos.get(i).getPuertos()) {
                                            if (puerto.getDireccion().getIp().equals(cs.getIpInicial())) {
                                                dispositivoOriginal = allDispositivos.get(i);
                                            }
                                            if (puerto.getDireccion().getIp().equals(cs.getIpFinal())) {
                                                last = allDispositivos.get(i);
                                            }
                                        }
                                    }
                                    if (last != null) {
                                        OSPF com = new OSPF(Controlador.allDispositivos);

                                        String salida = com.envio((Nodo) dispositivoOriginal, (Nodo) last, cantidadPaquetes);
                                        if (salida.equals(Constantes.ERROR_NODOS_INALCANZABLES)) {
                                            resultado += salida;
                                        } else if (salida.equals(Constantes.ERROR_NO_PUERTA_ENLACE_INICIAL) || salida.equals(Constantes.ERROR_NO_PUERTA_ENLACE_FINAL)) {
                                            resultado += salida;
                                            //TODO mostrarlo con joption pane.
                                        } else if (!salida.equals(Constantes.ERROR_NODOS_DESCONFIGURADOS)) {

                                            ArrayList<DispositivoGrafico> camino = com.getCamino();
                                            ArrayList<DispositivoGrafico> caminointermedio = new ArrayList<>();

                                            //para darle la vuelta al arraylist
                                            for (int i = camino.size() - 1; i >= 0; i--) {
                                                caminointermedio.add(camino.get(i));
                                            }
                                            camino = caminointermedio;

                                            int contPasadas = 1;
                                            DispositivoGrafico inicial = com.getCamino().get(0);
                                            DispositivoGrafico destino2 = com.getCamino().get(0);
                                            ArrayList<Linea> todasLineas = new ArrayList<>();
                                            ManejoAnimacion animacion = new ManejoAnimacion(destino2.getPanelTrabajo());

                                            for (int i = camino.size() - 1; i >= 0; i--) {
                                                if (contPasadas == 1) {
                                                    inicial = camino.get(i);
                                                } else if (contPasadas == 2) {
                                                    destino2 = camino.get(i);
                                                    Linea linea = new Linea(inicial.getDispositivo(), destino2.getDispositivo());
                                                    linea.setX1(inicial.getImagen().getX() + inicial.getImagen().getWidth() / 2);// -Dispositivo.lineaInicial.getImagen().getWidth()/2
                                                    linea.setY1(inicial.getImagen().getY() + inicial.getImagen().getHeight() / 2);//-Dispositivo.lineaInicial.getImagen().getHeight()/2
                                                    linea.setX2(destino2.getImagen().getX() + destino2.getImagen().getWidth() / 2);//-this.getImagen().getWidth()/2
                                                    linea.setY2(destino2.getImagen().getY() + destino2.getImagen().getHeight() / 2);//-this.getImagen().getHeight()/2
                                                    todasLineas.add(linea);
                                                    inicial = camino.get(i);
                                                    contPasadas = 1;
                                                }
                                                contPasadas++;

                                            }

                                            for (Linea linea : todasLineas) {
                                                animacion.rectaSimple2(linea);
                                            }
                                            animacion.execute();
                                            Controlador.estadoAnimacion = true;
                                        }
                                        resultado += salida;

                                        //JOptionPane.showMessageDialog(null, salida);
                                    } else {
                                        JOptionPane.showMessageDialog(null, "Debe seleccionar un nodo", "Informacion", JOptionPane.INFORMATION_MESSAGE);
                                    }
                                }

                            } catch (NumberFormatException e) {
                                JOptionPane.showMessageDialog(null, "Ingrese una cantidad Válida", "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "Solo se puede enviar de host a host",
                                    "ERROR", JOptionPane.ERROR_MESSAGE);
                            resultado += ("Solo se puede enviar de host a host<br>");
                        }
                    }
                }
            }
        }

        return resultado;
    }
    //</editor-fold> 

    //<editor-fold defaultstate="collapsed" desc="MostrarEnConsola">
    /**
     * Método para mostrar en consola
     *
     * @param s. Cadena simple a mostrar en la consola
     */
    public void mostrarEnConsola(String s) {
        try {
            field.setBackground(Color.white);
            field.setEditable(false);
            areaSalida.setCaretPosition(areaSalida.getStyledDocument().getLength());

            //se escribe el resultado
            s = "<font color=black face=Tahoma>" + s;
            s += "<br>------------------------------------------------------------------------------------------------------------<br>";
            s += "Consola>></font>";

            StringReader reader = new StringReader(s);            //
            editor.read(reader, areaSalida.getDocument(), areaSalida.getDocument().getLength());

            // insertamos un JTextArea
            JTextArea consola = new JTextArea();
            //editamos los atributos del area
            if (!primeraVez) {
                consola.setLineWrap(true);
                consola.setWrapStyleWord(true);
            }
            consola.setBackground(Color.white);
            consola.setForeground(Color.black);
            consola.setFont(new Font("Tahoma", Font.PLAIN, 15));
            field = consola;
            consola.requestFocus();
            areaSalida.insertComponent(consola);
            consola.requestFocus();
            //areaSalida.setCaretPosition(areaSalida.getDocument().getLength());
            consola.addKeyListener(new java.awt.event.KeyAdapter() {
                @Override
                public void keyPressed(java.awt.event.KeyEvent evt) {

                    campoComandoKeyPressed(evt);
                }
            });
        } catch (Exception ex) {
            System.out.println("Error al mostrar en consola");
            ex.printStackTrace();
        }
    }

    public void mostrarEnConsola(String s, String color) {
        try {
            field.setBackground(Color.white);
            field.setEditable(false);
            areaSalida.setCaretPosition(areaSalida.getStyledDocument().getLength());

            //se escribe el resultado
            s = "<font color=" + color + " face=Tahoma>" + s;
            s += "<br>------------------------------------------------------------------------------------------------------------<br>";
            s += "Consola>></font>";
            //s = "<font color=" + color + " >" + s + "</font>";

            StringReader reader = new StringReader(s);            //
            editor.read(reader, areaSalida.getDocument(), areaSalida.getDocument().getLength());

            // insertamos un JTextArea
            JTextArea consola = new JTextArea();
            //editamos los atributos del area
            if (!primeraVez) {
                consola.setLineWrap(true);
                consola.setWrapStyleWord(true);
            }
            consola.setBackground(Color.white);
            consola.setForeground(Color.black);
            consola.setFont(new Font("Tahoma", Font.PLAIN, 15));
            field = consola;
            consola.requestFocus();
            areaSalida.insertComponent(consola);
            consola.requestFocus();
            //areaSalida.setCaretPosition(areaSalida.getDocument().getLength());
            consola.addKeyListener(new java.awt.event.KeyAdapter() {
                @Override
                public void keyPressed(java.awt.event.KeyEvent evt) {

                    campoComandoKeyPressed(evt);
                }
            });
        } catch (Exception ex) {
            System.out.println("Error al mostrar en consola");
            ex.printStackTrace();
        }
    }
    //</editor-fold>

    public void campoComandoKeyPressed(java.awt.event.KeyEvent evt) {
        String comando = "";
        int key = evt.getKeyCode();
        //se compara la tecla ingresada con la tecla enter
        if (key == KeyEvent.VK_ENTER) {
            comando = field.getText();
            comando = comando.trim();
            field.setEditable(false);
            //areaSalida.setCaretPosition(areaSalida.getDocument().getLength());
            if (!comando.equals("")) {
                mostrarEnConsola(Comando(comando));
            } else {
                mostrarEnConsola("");
            }
            //campoComando.setText("");
        }

    }

    //<editor-fold defaultstate="collapsed" desc="Método existLinea">
    /**
     * Método para determinar si existe una <b>linea</b> entre dos
     * <b><i>dispositivos</i></b>
     *
     * @param inicial Dispositivo
     * @param finall Dispositivo
     * @return boolean que indica si existe linea entre los dispositivos
     */
    public static boolean existLinea(Dispositivo inicial, Dispositivo finall) {
        boolean result = false;
        for (Linea linea1 : Controlador.lineas) {
            if ((linea1.getInicial() == inicial && linea1.getFinall() == finall) || (linea1.getInicial() == finall && linea1.getFinall() == inicial)) {
                result = true;
            }
        }
        return result;
    }
    //</editor-fold>

    public static void actualizarTooltips() {
        for (DispositivoGrafico dispositivoGrafico : graficos) {
            dispositivoGrafico.getImagen().setToolTipText(dispositivoGrafico.getDispositivo().toolTip());
        }
    }
}