/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista.tools;

import Controlador.Controlador;
import Logica.Constantes.Constantes;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

/**
 * <p align="justify">
 * Esta clase se encarga de guardar información referente a la ubicación de los
 * pdf que sarán mostrados en la opción de ayuda de la aplicación y además del
 * proceso de abrir el archivo pdf para que el usuario pueda ver la información
 * </p>
 * <p align="justify">
 * El contenido de esta clase es una lista con las direcciones de los archivos
 * pdf y un método que es capaz de abrir el archivo para que se visto por el
 * usuario
 * </p>
 *
 *
 * @author Leonardo Ortega Hernandez
 */
public class PDFview extends ArrayList {

    private static PDFview pview;

    public PDFview() {
    }


    public String abrirpdf(String file) {
        String salida = "";
        try {
            ClassLoader classLoader = PDFview.class.getClassLoader();
            URL url = classLoader.getResource(file);
            File path = new File(url.getFile());
            Desktop.getDesktop().open(path);
        } catch (Exception ex) {
            salida = "ERROR: " + ex.getMessage() + ""
                    + "\nEste error se puede deber a que la dirección del archivo no es la correcta \no el archivo no existe";
        }
        return salida;
    }

    public static PDFview get() {
        if (pview == null) {
            pview = new PDFview();
        }
        return pview;
    }
}
