/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Logica.Linea;
import java.util.ArrayList;
import javax.swing.JLabel;

/**
 * Esta clase representa en forma gráfica el paquete que se envía durante la
 * animación
 *
 * @author Leonardo Ortega Hernandez <leon9894@gmail.com>
 */
public class PaquteGrafico {

    ArrayList<Integer> x = new ArrayList<>();//lista de cordenadas x por donde se delplala la animaciom
    ArrayList<Integer> y = new ArrayList<>();//lista de cordenadas y por donde se delplala la animaciom
    boolean ruta = false;//atributo que si es true quiere decir que la ruta de este paquete es la correcta
    int i = 0;//recorre las posiciones de las lista de (x,y)
    JLabel paquete;//Label que es el objeto que se mueve en la animacion

    /**
     * Constructor por defecto
     */
    public PaquteGrafico() {
        this.paquete = new JLabel();
        this.paquete.setSize(32, 32);
        this.paquete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imágenes/paquete.png")));
        this.paquete.setLocation(40, 100);
        this.paquete.setVisible(false);
    }

    /**
     * devuelve el array de X
     *
     * @return Retorna un array de integer
     */
    public ArrayList<Integer> getX() {
        return x;
    }

    /**
     * Modifica el array de X
     *
     * @param x nuevo valor del array
     */
    public void setX(ArrayList<Integer> x) {
        this.x = x;
    }

    /**
     * devuelve el array de Y
     *
     * @return Retorna un array de integer
     */
    public ArrayList<Integer> getY() {
        return y;
    }

    /**
     * Modifica el array de Y
     *
     * @param y nuevo lalor del array
     */
    public void setY(ArrayList<Integer> y) {
        this.y = y;
    }

    /**
     * Retorna el objeto que se dibuja
     *
     * @return Retorna un Label
     */
    public JLabel getPaquete() {
        return paquete;
    }

    /**
     * Modifica la instacia de el objeto paquete
     *
     * @param paquete nuevo paquete label
     */
    public void setPaquete(JLabel paquete) {
        this.paquete = paquete;
    }

    /**
     * Genera dos listas con las X y Y hay en el recorrido entre dos puntos en
     * el plano de la animacion.
     *
     * @param linea Objeto linea
     * @see Linea
     */
    public void rectaSimple2(Linea linea) {

        int x0 = linea.getX1();
        int y0 = linea.getY1();
        int x1 = linea.getX2();
        int y1 = linea.getY2();

        int dx = x1 - x0;
        int dy = y1 - y0;

        x.add(x0);
        y.add(y0);

        if (Math.abs(dx) > Math.abs(dy)) {          // pendiente < 1
            float m = (float) dy / (float) dx;
            float b = y0 - m * x0;
            if (dx < 0) {
                dx = -1;
            } else {
                dx = 1;
            }
            while (x0 != x1) {
                x0 += dx;
                y0 = Math.round(m * x0 + b);
                x.add(x0);
                y.add(y0);

            }
        } else if (dy != 0) {                              // slope >= 1
            float m = (float) dx / (float) dy;      // compute slope
            float b = x0 - m * y0;
            if (dy < 0) {
                dy = -1;
            } else {
                dy = 1;
            }
            while (y0 != y1) {
                y0 += dy;
                x0 = Math.round(m * y0 + b);
                x.add(x0);
                y.add(y0);
            }
        }
    }

    /**
     * Genera todos los X y Y que hay en un camino o varios
     *
     * @param lineas Array de lines
     * @see Linea
     */
    public void retas(ArrayList<Linea> lineas) {
        for (int i = 0; i < lineas.size(); i++) {
            rectaSimple2(lineas.get(i));
        }
    }

    /**
     * Aumenta el contador de los arraya que contienen las posiciones X y Y para
     * para que el paquete se vaya desplazando en la ventana
     *
     * @return Retorna true si quedan puntos por recorre y false si ya no se
     * podra seguir moviendo mas
     */
    public boolean mover() {
        if (i < x.size() && i < y.size()) {
            paquete.setLocation(x.get(i), y.get(i));
            i++;
            return true;
        }
        return false;
    }

    /**
     * Indica si la ruta es correcta al destinmo
     *
     * @return Retorna true si la ruta el correcta y false si no es el detino
     * final
     */
    public boolean isRuta() {
        return ruta;
    }

    /**
     * Modifica el valor valor de la ruta
     *
     * @param ruta nuevo valor para indicar si la ruta es correcta o no
     */
    public void setRuta(boolean ruta) {
        this.ruta = ruta;
    }
}
