/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas;

/**
 *
 * @author Jorge
 */
public class AutomataIP {
    
    private int cont;
    private char[] car;
    private String c;
    private boolean aceptado = false;
    
    public AutomataIP(String c){
        this.c = c;
        car = c.toCharArray();   
    }
    
    public boolean inicio(){
        cont = 0;
        q0();
        return aceptado;
    }
    
    public void q0(){
        
        //System.out.println(" En estado q0 ");
       
            if(cont < car.length){
                if(car[cont] == '0'){
                    qError();
                }else if(car[cont] == '1'){
                    cont++;
                    q1();
                }else if(car[cont] == '2'){
                    cont++;
                    q2();
                }else if((car[cont] >= '3') && (car[cont] <= '9')){
                    cont++;
                    q3();
                }else{
                    qError();
                }
            }else
                qError();
        
    }
    
    public void q1(){
        //System.out.println(" En estado q1 ");
        
                if(cont < car.length){
                    if((car[cont] >= '0') && (car[cont] <= '9')){
                        cont++;
                        q3();
                    }else if(car[cont] == '.'){
                        cont++;
                        q10();
                    }else{
                        qError();
                    }
                }else{
                    qError();
                }
            
        
    }
    
    public void q2(){
        
        //System.out.println(" En estado q2 ");
            if(cont < car.length){
                if((car[cont] >= '0') && (car[cont] <= '4')){
                    cont++;
                    q4();
                }else if(car[cont] == '5'){
                    cont++;
                    q5();
                }else if((car[cont] >= '6') && (car[cont] <= '9')){
                    cont++;
                    q9();
                }else if(car[cont] == '.'){
                    cont++;
                    q10();
                }else{
                    qError();
                }
            }else{
                qError();
            }
        
    }
    
    public void q3(){
        //System.out.println(" En estado q3 ");
        
            if(cont < car.length){
                if((car[cont] >= '0') && (car[cont] <= '9')){
                    cont++;
                    q6();
                }else if(car[cont] == '.'){
                    cont++;
                    q10();
                }else{
                    qError();
                }
            }else{
                qError();
            }
        
    }
    
    public void q4(){
        //System.out.println(" En estado q4 ");
        
            if(cont < car.length){
                if((car[cont] >= '0') && (car[cont] <= '9')){
                    cont++;
                    q7();
                }else if(car[cont] == '.'){
                    cont++;
                    q10();
                }
                else{
                    qError();
                }
            }else{
                qError();
            }
        
    }
    
    public void q5(){
        //System.out.println(" En estado q5 ");
        
        if(cont < car.length){
                if((car[cont] >= '0') && (car[cont] <= '5')){
                    cont++;
                    q8();
                }else if(car[cont] == '.'){
                    cont++;
                    q10();
                }else{
                    qError();
                }
         }else
            qError();
    }
    
    public void q6(){
        //System.out.println(" En estado q6 ");
        
                if(cont < car.length){
                    if(car[cont] == '.'){
                        cont++;
                        q10();
                    }
                }else{
                    qError();
                }
    }
    
    public void q7(){
        //System.out.println(" En estado q7 ");
            
        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q10();
            }
        } else {
            qError();
        }
    }
    
    public void q8(){
        //System.out.println(" En estado q8 ");
        
        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q10();
            }
        } else {
            qError();
        }
    }
    
    public void q9(){
        //System.out.println(" En estado q9 ");
        
         if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q10();
            }
        } else {
            qError();
        }
    }
    
    public void q10(){
        //System.out.println(" En estado q10 ");
        
        if(cont < car.length){
                if(car[cont] == '0'){
                    cont++;
                    q40();
                }else if(car[cont] == '1'){
                    cont++;
                    q11();
                }else if(car[cont] == '2'){
                    cont++;
                    q12();
                }else if((car[cont] >= '3') && (car[cont] <= '9')){
                    cont++;
                    q13();
                }else{
                    qError();
                }
         }else
            qError();
    }
    
    public void q11(){
        //System.out.println(" En estado q11 ");
        
        if(cont < car.length){
                    if((car[cont] >= '0') && (car[cont] <= '9')){
                        cont++;
                        q13();
                    }else if(car[cont] == '.'){
                        cont++;
                        q20();
                    }else{
                        qError();
                    }
                }else{
                    qError();
                }
                
    }
    
    public void q12(){
     //System.out.println(" En estado q12 ");
     
            if(cont < car.length){
                if((car[cont] >= '0') && (car[cont] <= '4')){
                    cont++;
                    q14();
                }else if(car[cont] == '5'){
                    cont++;
                    q15();
                }else if((car[cont] >= '6') && (car[cont] <= '9')){
                    cont++;
                    q19();
                }else if(car[cont] == '.'){
                    cont++;
                    q20();
                }else{
                    qError();
                }
            }else{
                qError();
            }   
    }
    
    public void q13(){
        //System.out.println(" En estado q13 ");
        
            if(cont < car.length){
                if((car[cont] >= '0') && (car[cont] <= '9')){
                    cont++;
                    q16();
                }else if(car[cont] == '.'){
                    cont++;
                    q20();
                }else{
                    qError();
                }
            }else{
                qError();
            }
    }
    
    public void q14(){
        //System.out.println(" En estado q14 ");
        
            if(cont < car.length){
                if((car[cont] >= '0') && (car[cont] <= '9')){
                    cont++;
                    q17();
                }else if(car[cont] == '.'){
                    cont++;
                    q20();
                }
                else{
                    qError();
                }
            }else{
                qError();
            }
    }
    
    public void q15(){
        //System.out.println(" En estado q15 ");
        
        if(cont < car.length){
                if((car[cont] >= '0') && (car[cont] <= '5')){
                    cont++;
                    q18();
                }else if(car[cont] == '.'){
                    cont++;
                    q20();
                }
                else{
                    qError();
                }
            }else{
                qError();
            }
    }
    
    public void q16(){
        //System.out.println(" En estado q16 ");
        
        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q20();
            }
        } else {
            qError();
        }
         
    }
    
    public void q17(){
        //System.out.println(" En estado q17 ");
     
        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q20();
            }
        } else {
            qError();
        }
    }
    
    public void q18(){
        //System.out.println(" En estado q18 ");
        
         if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q20();
            }
        } else {
            qError();
        }
    }
    
    public void q19(){
        //System.out.println(" En estado q19 ");
        
         if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q20();
            }
        } else {
            qError();
        }
    }
    
    public void q20(){
        //System.out.println(" En estado q20 ");
        
        if(cont < car.length){
                if(car[cont] == '0'){
                    cont++;
                    q41();
                }else if(car[cont] == '1'){
                    cont++;
                    q21();
                }else if(car[cont] == '2'){
                    cont++;
                    q22();
                }else if((car[cont] >= '3') && (car[cont] <= '9')){
                    cont++;
                    q23();
                }else{
                    qError();
                }
         }else
            qError();
    }
    
    public void q21() {
        //System.out.println(" En estado q21 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q23();
            } else if (car[cont] == '.') {
                cont++;
                q30();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q22() {
        //System.out.println(" En estado q22 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '4')) {
                cont++;
                q24();
            } else if (car[cont] == '5') {
                cont++;
                q25();
            } else if ((car[cont] >= '6') && (car[cont] <= '9')) {
                cont++;
                q29();
            } else if (car[cont] == '.') {
                cont++;
                q30();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q23() {
        //System.out.println(" En estado q23 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q26();
            } else if (car[cont] == '.') {
                cont++;
                q30();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q24() {
        //System.out.println(" En estado q24 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q27();
            } else if (car[cont] == '.') {
                cont++;
                q30();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q25() {
        //System.out.println(" En estado q25 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '5')) {
                cont++;
                q28();
            } else if (car[cont] == '.') {
                cont++;
                q30();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q26() {
        //System.out.println(" En estado q26 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q30();
            }
        } else {
            qError();
        }

    }

    public void q27() {
        //System.out.println(" En estado q27 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q30();
            }
        } else {
            qError();
        }
    }

    public void q28() {
        //System.out.println(" En estado q28 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q30();
            }
        } else {
            qError();
        }
    }

    public void q29() {
        //System.out.println(" En estado q29 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q30();
            }
        } else {
            qError();
        }
    }
    
    public void q30(){
        
        //System.out.println(" En estado q30 ");
       
            if(cont < car.length){
                if(car[cont] == '0'){
                    qError();
                }else if(car[cont] == '1'){
                    cont++;
                    q31();
                }else if(car[cont] == '2'){
                    cont++;
                    q32();
                }else if((car[cont] >= '3') && (car[cont] <= '9')){
                    cont++;
                    q33();
                }else{
                    qError();
                }
            }else
                qError();
        
    }
    
    public void q31(){
        //System.out.println(" En estado q31 ");
        
                if(cont < car.length){
                    if((car[cont] >= '0') && (car[cont] <= '9')){
                        cont++;
                        q33();
                    }else{
                        qError();
                    }
                }else if(cont == car.length){
                        q42();
                }else{
                    qError();
                }
            
        
    }
    
    public void q32(){
        
        //System.out.println(" En estado q32 ");
            if(cont < car.length){
                if((car[cont] >= '0') && (car[cont] <= '4')){
                    cont++;
                    q34();
                }else if(car[cont] == '5'){
                    cont++;
                    q35();
                }else if((car[cont] >= '6') && (car[cont] <= '9')){
                    cont++;
                    q39();
                }else{
                    qError();
                }
            }else if(cont == car.length){
                        q42();
            }else{
                    qError();
            }
        
    }
    
    public void q33(){
        //System.out.println(" En estado q33 ");
        
            if(cont < car.length){
                if((car[cont] >= '0') && (car[cont] <= '9')){
                    cont++;
                    q36();
                }else{
                    qError();
                }
            }else if(cont == car.length){
                        q42();
            }else{
                    qError();
            }
        
    }
    
    public void q34(){
        //System.out.println(" En estado q34 ");
        
            if(cont < car.length){
                if((car[cont] >= '0') && (car[cont] <= '9')){
                    cont++;
                    q37();
                }
                else{
                    qError();
                }
            }else if(cont == car.length){
                        q42();
            }else{
                    qError();
            }
        
    }
    
    public void q35(){
        //System.out.println(" En estado q35 ");
        
        if(cont < car.length){
                if((car[cont] >= '0') && (car[cont] <= '5')){
                    cont++;
                    q38();
                }else{
                    qError();
                }
         }else if(cont == car.length){
                        q42();
         }else{
                    qError();
         }
    }
    
    public void q36(){
        //System.out.println(" En estado q36 ");
        
                if(cont == car.length){
                    q42();
                }else{
                    qError();
                }
    }
    
    public void q37(){
        //System.out.println(" En estado q37 ");
            
        if (cont == car.length) {
            q42();
        } else {
            qError();
        }
    }
    
    public void q38(){
        //System.out.println(" En estado q38 ");
        
        if (cont == car.length) {
            q42();
        } else {
            qError();
        }
    }
    
    public void q39(){
        //System.out.println(" En estado q39 ");
        
         if (cont == car.length) {
            q42();
        } else {
            qError();
        }
    }
    
    public void q40(){
        //System.out.println(" En estado q40");
        
        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q20();
            }
        } else 
            qError();
           
    }
    
    public void q41(){
        //System.out.println(" En estado q41");
        
        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q30();
            }
        } else 
            qError();
           
    }
    
    public void q42(){
        //System.out.println(" En estado q42 - aceptación ");
        System.out.println(" Expresión valida ");
        aceptado = true;
    }
    
    public void qError(){
        System.out.println(" En estado de error - No válido");
        aceptado = false;
    }
    
}
