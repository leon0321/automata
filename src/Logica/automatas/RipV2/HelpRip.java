/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas.RipV2;

/**
 * Automata que validad la palabra help. Este comando es utilizado en Rip v2
 * mostrarle al usuario qué comandos puede utilizar al encontrarse sobre este
 * protocolo. Puede utilizarce en cualquier nivel de la terminal.
 *
 * El constructor de este autómata recibe como parametro una cadena string que
 * en este caso corresponde al comando digitado por el usuario en la terminal
 * (shell) del aplicativo.
 *
 * @author TavoRojas
 *
 */
public class HelpRip {

    private int cont;
    private char[] car;
    private String c;
    private boolean aceptado = false;

    public HelpRip(String c) {
        this.c = c;
        car = c.toCharArray();
    }

    public boolean inicio() {
        cont = 0;
        q0();
        return aceptado;
    }

    private void q0() {
        if (cont < car.length) {
            if (car[cont] != 'h' && car[cont] != 'H') {
                qError();
            } else if (car[cont] == 'h' || car[cont] == 'H') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void qError() {
        aceptado = false;
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 'l' && car[cont] != 'L') {
                qError();
            } else if (car[cont] == 'l' || car[cont] == 'L') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != 'p' && car[cont] != 'P') {
                qError();
            } else if (car[cont] == 'p' || car[cont] == 'P') {
                cont++;
                q4();
            }
        } else {
            qError();
        }
    }

    private void q4() {
        System.out.println(" En estado q9 - aceptación ");
        System.out.println(" Expresión valida ");
        aceptado = true;
    }
}
