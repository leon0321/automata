package Logica.automatas.RipV2;

/**
 * Automata que validad la palabra key-string, seguida de un numero de 3 digitos
 * como mínimo. Este comando es utilizado en Rip v2 para darle un nombre a la
 * llave (password) utilizada en la autenticación. En sí, este número será el
 * password que permita comunicacion entre routers autenticados.
 *
 * El constructor de este autómata recibe como parametro una cadena string que
 * en este caso corresponde al comando digitado por el usuario en la terminal
 * (shell) del aplicativo.
 *
 * @author TavoRojas
 *
 */
public class Nom_llave {

    private int cont;
    private char[] car;
    private String c;
    private boolean aceptado = false;

    public Nom_llave(String c) {
        this.c = c;
        car = c.toCharArray();
    }

    public boolean inicio() {
        cont = 0;
        q0();
        return aceptado;
    }

    private void q0() {
        if (cont < car.length) {
            if (car[cont] != 'k' && car[cont] != 'K') {
                qError();
            } else if (car[cont] == 'k' || car[cont] == 'K') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void qError() {
        aceptado = false;
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 'y' && car[cont] != 'Y') {
                qError();
            } else if (car[cont] == 'y' || car[cont] == 'Y') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != '-' && car[cont] != '-') {
                qError();
            } else if (car[cont] == '-' || car[cont] == '-') {
                cont++;
                q4();
            }
        } else {
            qError();
        }
    }

    private void q4() {
        if (cont < car.length) {
            if (car[cont] != 's' && car[cont] != 'S') {
                qError();
            } else if (car[cont] == 's' || car[cont] == 'S') {
                cont++;
                q5();
            }
        } else {
            qError();
        }
    }

    private void q5() {
        if (cont < car.length) {
            if (car[cont] != 't' && car[cont] != 'T') {
                qError();
            } else if (car[cont] == 't' || car[cont] == 'T') {
                cont++;
                q6();
            }
        } else {
            qError();
        }
    }

    private void q6() {
        if (cont < car.length) {
            if (car[cont] != 'r' && car[cont] != 'R') {
                qError();
            } else if (car[cont] == 'r' || car[cont] == 'R') {
                cont++;
                q7();
            }
        } else {
            qError();
        }
    }

    private void q7() {
        if (cont < car.length) {
            if (car[cont] != 'i' && car[cont] != 'I') {
                qError();
            } else if (car[cont] == 'i' || car[cont] == 'I') {
                cont++;
                q8();
            }
        } else {
            qError();
        }
    }

    private void q8() {
        if (cont < car.length) {
            if (car[cont] != 'n' && car[cont] != 'N') {
                qError();
            } else if (car[cont] == 'n' || car[cont] == 'N') {
                cont++;
                q9();
            }
        } else {
            qError();
        }
    }

    private void q9() {
        if (cont < car.length) {
            if (car[cont] != 'g' && car[cont] != 'G') {
                qError();
            } else if (car[cont] == 'g' || car[cont] == 'G') {
                cont++;
                q20();
            }
        } else {
            qError();
        }
    }

    private void q20() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == ' ') {
                cont++;
                q20();
            } else if (car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                cont++;
                q10();
            }
        } else {
            qError();
        }
    }

    private void q10() {
        if (cont < car.length) {
            if (car[cont] != '0' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                cont++;
                q11();
            }
        } else {
            qError();
        }

    }

    private void q11() {
        if (cont < car.length) {
            if (car[cont] != '0' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                cont++;
                q12();
            }
        } else {
            qError();
        }
    }

    private void q12() {
        if (cont < car.length) {
            if (car[cont] != '0' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                q11();
                System.out.println(" por aqui ");
            }
        } else {
            System.out.println(" Expresión valida ");
            aceptado = true;
        }
    }
}
