package Logica.automatas.RipV2;

/**
 * Automata que validad la palabra exit este comando es utilizado en Rip v2 para
 * cambiar de nivel en la terminal (shell). Se le recuerda que cada nvel acepta
 * comandos propios.
 *
 * El constructor de este autómata recibe como parametro una cadena string que
 * en este caso corresponde al comando digitado por el usuario en la terminal
 * (shell) del aplicativo.
 *
 * @author TavoRojas
 *
 */
public class Exit {

    private int cont;
    private char[] car;
    private String c;
    private boolean aceptado = false;

    public Exit(String c) {
        this.c = c;
        car = c.toCharArray();
    }

    public boolean inicio() {
        cont = 0;
        q0();
        return aceptado;
    }

    private void q0() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void qError() {
        aceptado = false;
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'x' && car[cont] != 'X') {
                qError();
            } else if (car[cont] == 'x' || car[cont] == 'X') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 'i' && car[cont] != 'I') {
                qError();
            } else if (car[cont] == 'i' || car[cont] == 'I') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != 't' && car[cont] != 'T') {
                qError();
            } else if (car[cont] == 't' || car[cont] == 'T') {
                cont++;
                q4();
            }
        } else {
            qError();
        }
    }

    private void q4() {
        System.out.println(" En estado q9 - aceptación ");
        System.out.println(" Expresión valida ");
        aceptado = true;
    }
}
