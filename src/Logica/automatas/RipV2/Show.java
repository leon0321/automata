/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas.RipV2;

/**
 * Automata que validad la frase show ip router. Este comando es utilizado en
 * Rip v2 para mostrar en la terminal (shell) la tabla RIP que pertenece al
 * router en cuestión. Sólo es valido en el nivel cero (0).
 *
 * El constructor de este autómata recibe como parametro una cadena string que
 * en este caso corresponde al comando digitado por el usuario en la terminal
 * (shell) del aplicativo.
 *
 * @author TavoRojas
 *
 */
public class Show {

    private int cont;
    private char[] car;
    private String c;
    private boolean aceptado = false;

    public Show(String c) {
        this.c = c;
        car = c.toCharArray();
    }

    public boolean inicio() {
        cont = 0;
        q0();
        return aceptado;
    }

    private void q0() {
        if (cont < car.length) {
            if (car[cont] != 's' && car[cont] != 'S') {
                qError();
            } else if (car[cont] == 's' || car[cont] == 'S') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void qError() {
        aceptado = false;
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'h' && car[cont] != 'H') {
                qError();
            } else if (car[cont] == 'h' || car[cont] == 'H') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 'o' && car[cont] != 'O') {
                qError();
            } else if (car[cont] == 'o' || car[cont] == 'O') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != 'w' && car[cont] != 'W') {
                qError();
            } else if (car[cont] == 'w' || car[cont] == 'W') {
                cont++;
                q4();
            }
        } else {
            qError();
        }
    }

    private void q4() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] != ' ') {
                qError();
            } else if (car[cont] == ' ' || car[cont] == ' ') {
                cont++;
                q5();
            }
        } else {
            qError();
        }
    }

    private void q5() {
        if (cont < car.length) {
            if (car[cont] != 'i' && car[cont] != 'I') {
                qError();
            } else if (car[cont] == 'i' || car[cont] == 'I') {
                cont++;
                q6();
            }
        } else {
            qError();
        }
    }

    private void q6() {
        if (cont < car.length) {
            if (car[cont] != 'p' && car[cont] != 'P') {
                qError();
            } else if (car[cont] == 'p' || car[cont] == 'P') {
                cont++;
                q7();
            }
        } else {
            qError();
        }
    }

    private void q7() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] != ' ') {
                qError();
            } else if (car[cont] == ' ' || car[cont] == ' ') {
                cont++;
                q8();
            }
        } else {
            qError();
        }
    }

    private void q8() {
        if (cont < car.length) {
            if (car[cont] != 'r' && car[cont] != 'R') {
                qError();
            } else if (car[cont] == 'r' || car[cont] == 'R') {
                cont++;
                q9();
            }
        } else {
            qError();
        }
    }

    private void q9() {
        if (cont < car.length) {
            if (car[cont] != 'o' && car[cont] != 'O') {
                qError();
            } else if (car[cont] == 'o' || car[cont] == 'O') {
                cont++;
                q10();
            }
        } else {
            qError();
        }
    }

    private void q10() {
        if (cont < car.length) {
            if (car[cont] != 'u' && car[cont] != 'U') {
                qError();
            } else if (car[cont] == 'u' || car[cont] == 'U') {
                cont++;
                q11();
            }
        } else {
            qError();
        }
    }

    private void q11() {
        if (cont < car.length) {
            if (car[cont] != 't' && car[cont] != 'T') {
                qError();
            } else if (car[cont] == 't' || car[cont] == 'T') {
                cont++;
                q12();
            }
        } else {
            qError();
        }
    }

    private void q12() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q13();
            }
        } else {
            qError();
        }
    }

    private void q13() {
        if (cont < car.length) {
            if (car[cont] != 'r' && car[cont] != 'R') {
                qError();
            } else if (car[cont] == 'r' || car[cont] == 'R') {
                cont++;
                q14();
            }
        } else {
            qError();
        }
    }

    private void q14() {
        System.out.println(" En estado q6 - aceptación ");
        System.out.println(" Expresión valida ");
        aceptado = true;
    }
}
