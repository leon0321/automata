package Logica.automatas.RipV2;

/**
 * Automata que validad la palabra key chain, seguida de cualquier cadena de
 * caracteres. Este comando es utilizado en Rip v2 para darle nombre a un
 * conjunto de llaves (password's) utilizadas en la autenticación.
 *
 * El constructor de este autómata recibe como parametro una cadena string que
 * en este caso corresponde al comando digitado por el usuario en la terminal
 * (shell) del aplicativo.
 *
 * @author TavoRojas
 *
 */
public class Llavero {

    private int cont;
    private char[] car;
    private String c;
    private boolean aceptado = false;

    public Llavero(String c) {
        this.c = c;
        car = c.toCharArray();
    }

    public boolean inicio() {
        cont = 0;
        q0();
        return aceptado;
    }

    private void q0() {
        if (cont < car.length) {
            if (car[cont] != 'k' && car[cont] != 'K') {
                qError();
            } else if (car[cont] == 'k' || car[cont] == 'K') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void qError() {
        aceptado = false;
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 'y' && car[cont] != 'Y') {
                qError();
            } else if (car[cont] == 'y' || car[cont] == 'Y') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] != ' ') {
                qError();
            } else if (car[cont] == ' ' || car[cont] == ' ') {
                cont++;
                q4();
            }
        } else {
            qError();
        }
    }

    private void q4() {
        if (cont < car.length) {
            if (car[cont] != 'c' && car[cont] != 'C') {
                qError();
            } else if (car[cont] == 'c' || car[cont] == 'C') {
                cont++;
                q5();
            }
        } else {
            qError();
        }
    }

    private void q5() {
        if (cont < car.length) {
            if (car[cont] != 'h' && car[cont] != 'H') {
                qError();
            } else if (car[cont] == 'h' || car[cont] == 'H') {
                cont++;
                q6();
            }
        } else {
            qError();
        }
    }

    private void q6() {
        if (cont < car.length) {
            if (car[cont] != 'a' && car[cont] != 'A') {
                qError();
            } else if (car[cont] == 'a' || car[cont] == 'A') {
                cont++;
                q7();
            }
        } else {
            qError();
        }
    }

    private void q7() {
        if (cont < car.length) {
            if (car[cont] != 'i' && car[cont] != 'I') {
                qError();
            } else if (car[cont] == 'i' || car[cont] == 'I') {
                cont++;
                q8();
            }
        } else {
            qError();
        }
    }

    private void q8() {
        if (cont < car.length) {
            if (car[cont] != 'n' && car[cont] != 'N') {
                qError();
            } else if (car[cont] == 'n' || car[cont] == 'N') {
                cont++;
                q9();
            }
        } else {
            qError();
        }
    }

    private void q9() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] != ' ') {
                qError();
            } else if (car[cont] == ' ' || car[cont] == ' ') {
                cont++;
                q10();
            }
        } else {
            qError();
        }
    }

    private void q10() {
        System.out.println(" En estado q9 - aceptación ");
        System.out.println(" Expresión valida ");
        aceptado = true;
    }
}
