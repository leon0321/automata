/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas.RipV2;

/**
 * Automata que validad la palabra router rip. Este comando es utilizado en Rip
 * v2 para indicar que se inicia la configuracion del protocolo RIP sobre el
 * router donde se abre la terminal (shell).
 *
 * El constructor de este autómata recibe como parametro una cadena string que
 * en este caso corresponde al comando digitado por el usuario en la terminal
 * (shell) del aplicativo.
 *
 * @author TavoRojas
 *
 */
public class RouteRip {

    private int cont;
    private char[] car;
    private String c;
    private boolean aceptado = false;

    public RouteRip(String c) {
        this.c = c;
        car = c.toCharArray();
    }

    public boolean inicio() {
        cont = 0;
        q0();
        return aceptado;
    }

    private void q0() {
        if (cont < car.length) {
            if (car[cont] != 'r' && car[cont] != 'R') {
                qError();
            } else if (car[cont] == 'r' || car[cont] == 'R') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void qError() {
        aceptado = false;
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'o' && car[cont] != 'O') {
                qError();
            } else if (car[cont] == 'o' || car[cont] == 'O') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 'u' && car[cont] != 'U') {
                qError();
            } else if (car[cont] == 'u' || car[cont] == 'U') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != 't' && car[cont] != 'T') {
                qError();
            } else if (car[cont] == 't' || car[cont] == 'T') {
                cont++;
                q4();
            }
        } else {
            qError();
        }
    }

    private void q4() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q5();
            }
        } else {
            qError();
        }
    }

    private void q5() {
        if (cont < car.length) {
            if (car[cont] != 'r' && car[cont] != 'R') {
                qError();
            } else if (car[cont] == 'r' || car[cont] == 'R') {
                cont++;
                q6();
            }
        } else {
            qError();
        }
    }

    private void q6() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] != ' ') {
                qError();
            } else if (car[cont] == ' ' || car[cont] == ' ') {
                cont++;
                q7();
            }
        } else {
            qError();
        }
    }

    private void q7() {
        if (cont < car.length) {
            if (car[cont] != 'r' && car[cont] != 'R') {
                qError();
            } else if (car[cont] == 'r' || car[cont] == 'R') {
                cont++;
                q8();
            }
        } else {
            qError();
        }
    }

    private void q8() {
        if (cont < car.length) {
            if (car[cont] != 'i' && car[cont] != 'I') {
                qError();
            } else if (car[cont] == 'i' || car[cont] == 'I') {
                cont++;
                q9();
            }
        } else {
            qError();
        }
    }

    private void q9() {
        if (cont < car.length) {
            if (car[cont] != 'p' && car[cont] != 'P') {
                qError();
            } else if (car[cont] == 'p' || car[cont] == 'P') {
                cont++;
                q10();
            }
        } else {
            qError();
        }
    }

    private void q10() {
        System.out.println(" En estado q6 - aceptación ");
        System.out.println(" Expresión valida ");
        aceptado = true;
    }
}
