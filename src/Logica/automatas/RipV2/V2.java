/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas.RipV2;

/**
 * Automata que validad la frase version 2. Este comando es utilizado en Rip v2
 * para indicar que nos encontramos en la version 2 del protocolo y no en rip
 * version 1.
 *
 * El constructor de este autómata recibe como parametro una cadena string que
 * en este caso corresponde al comando digitado por el usuario en la terminal
 * (shell) del aplicativo.
 *
 * @author TavoRojas
 *
 */
public class V2 {

    private int cont;
    private char[] car;
    private String c;
    private boolean aceptado = false;

    public V2(String c) {
        this.c = c;
        car = c.toCharArray();
    }

    public boolean inicio() {
        cont = 0;
        q0();
        return aceptado;
    }

    private void q0() {
        if (cont < car.length) {
            if (car[cont] != 'v' && car[cont] != 'V') {
                qError();
            } else if (car[cont] == 'v' || car[cont] == 'V') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void qError() {
        aceptado = false;
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 'r' && car[cont] != 'R') {
                qError();
            } else if (car[cont] == 'r' || car[cont] == 'R') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != 's' && car[cont] != 'S') {
                qError();
            } else if (car[cont] == 's' || car[cont] == 'S') {
                cont++;
                q4();
            }
        } else {
            qError();
        }
    }

    private void q4() {
        if (cont < car.length) {
            if (car[cont] != 'i' && car[cont] != 'I') {
                qError();
            } else if (car[cont] == 'i' || car[cont] == 'I') {
                cont++;
                q5();
            }
        } else {
            qError();
        }
    }

    private void q5() {
        if (cont < car.length) {
            if (car[cont] != 'o' && car[cont] != 'O') {
                qError();
            } else if (car[cont] == 'o' || car[cont] == 'O') {
                cont++;
                q6();
            }
        } else {
            qError();
        }
    }

    private void q6() {
        if (cont < car.length) {
            if (car[cont] != 'n' && car[cont] != 'N') {
                qError();
            } else if (car[cont] == 'n' || car[cont] == 'N') {
                cont++;
                q7();
            }
        } else {
            qError();
        }
    }

    private void q7() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] != ' ') {
                qError();
            } else if (car[cont] == ' ' || car[cont] == ' ') {
                cont++;
                q8();
            }
        } else {
            qError();
        }
    }

    private void q8() {
        if (cont < car.length) {
            if (car[cont] != '2' && car[cont] != '2') {
                qError();
            } else if (car[cont] == '2' || car[cont] == '2') {
                cont++;
                q9();
            }
        } else {
            qError();
        }
    }

    private void q9() {
        System.out.println(" En estado q9 - aceptación ");
        System.out.println(" Expresión valida ");
        aceptado = true;
    }
}
