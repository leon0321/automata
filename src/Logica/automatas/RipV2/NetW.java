package Logica.automatas.RipV2;

/**
 * Automata que validad la palabra network, seguida de una direccion ip valida.
 * Este comando es utilizado en Rip v2 para ingresar las direcciones ip de las
 * redes que estan conectadas directamente al router donde se abre la terminal
 * (shell) para luego mostrarlas en la tabla de ruteo del router.
 *
 * El constructor de este autómata recibe como parametro una cadena string que
 * en este caso corresponde al comando digitado por el usuario en la terminal
 * (shell) del aplicativo.
 *
 * @author TavoRojas
 *
 */
public class NetW {

    private int cont;
    private char[] car;
    private String c;
    private boolean aceptado = false;

    public NetW(String c) {
        this.c = c;
        car = c.toCharArray();
    }

    public boolean inicio() {
        cont = 0;
        q0();
        return aceptado;
    }

    private void q0() {
        if (cont < car.length) {
            if (car[cont] != 'n' && car[cont] != 'N') {
                qError();
            } else if (car[cont] == 'n' || car[cont] == 'N') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void qError() {
        aceptado = false;
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 't' && car[cont] != 'T') {
                qError();
            } else if (car[cont] == 't' || car[cont] == 'T') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != 'w' && car[cont] != 'W') {
                qError();
            } else if (car[cont] == 'w' || car[cont] == 'W') {
                cont++;
                q4();
            }
        } else {
            qError();
        }
    }

    private void q4() {
        if (cont < car.length) {
            if (car[cont] != 'o' && car[cont] != 'O') {
                qError();
            } else if (car[cont] == 'o' || car[cont] == 'O') {
                cont++;
                q5();
            }
        } else {
            qError();
        }
    }

    private void q5() {
        if (cont < car.length) {
            if (car[cont] != 'r' && car[cont] != 'R') {
                qError();
            } else if (car[cont] == 'r' || car[cont] == 'R') {
                cont++;
                q6();
            }
        } else {
            qError();
        }
    }

    private void q6() {
        if (cont < car.length) {
            if (car[cont] != 'k' && car[cont] != 'K') {
                qError();
            } else if (car[cont] == 'k' || car[cont] == 'K') {
                cont++;
                System.out.println("bien");
                q7();
            }
        } else {
            qError();
        }
    }

    private void q7() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] != ' ') {
                qError();
            } else if (car[cont] == ' ' || car[cont] == ' ') {
                cont++;
                System.out.println("bien");
                q8();
            }
        } else {
            qError();
        }
    }

//----comienza a validar para ip aceptando la terminada en .0 ----------------------
    public void q8() {

        //System.out.println(" En estado q0 ");

        if (cont < car.length) {
            if (car[cont] == '0') {
                qError();
            } else if (car[cont] == '1') {
                cont++;
                q9();
            } else if (car[cont] == '2') {
                cont++;
                q10();
            } else if ((car[cont] >= '3') && (car[cont] <= '9')) {
                cont++;
                q11();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q9() {
        //System.out.println(" En estado q1 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q11();
            } else if (car[cont] == '.') {
                cont++;
                q18();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q10() {

        //System.out.println(" En estado q2 ");
        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '4')) {
                cont++;
                q12();
            } else if (car[cont] == '5') {
                cont++;
                q13();
            } else if ((car[cont] >= '6') && (car[cont] <= '9')) {
                cont++;
                q17();
            } else if (car[cont] == '.') {
                cont++;
                q18();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q11() {
        //System.out.println(" En estado q3 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q14();
            } else if (car[cont] == '.') {
                cont++;
                q18();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q12() {
        //System.out.println(" En estado q4 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q15();
            } else if (car[cont] == '.') {
                cont++;
                q18();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q13() {
        //System.out.println(" En estado q5 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '5')) {
                cont++;
                q16();
            } else if (car[cont] == '.') {
                cont++;
                q18();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q14() {
        //System.out.println(" En estado q6 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q18();
            }
        } else {
            qError();
        }
    }

    public void q15() {
        //System.out.println(" En estado q7 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q18();
            }
        } else {
            qError();
        }
    }

    public void q16() {
        //System.out.println(" En estado q8 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q18();
            }
        } else {
            qError();
        }
    }

    public void q17() {
        //System.out.println(" En estado q9 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q18();
            }
        } else {
            qError();
        }
    }

    public void q18() {
        //System.out.println(" En estado q10 ");

        if (cont < car.length) {
            if (car[cont] == '0') {
                cont++;
                q48();
            } else if (car[cont] == '1') {
                cont++;
                q19();
            } else if (car[cont] == '2') {
                cont++;
                q20();
            } else if ((car[cont] >= '3') && (car[cont] <= '9')) {
                cont++;
                q21();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q19() {
        //System.out.println(" En estado q11 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q21();
            } else if (car[cont] == '.') {
                cont++;
                q28();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q20() {
        //System.out.println(" En estado q12 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '4')) {
                cont++;
                q22();
            } else if (car[cont] == '5') {
                cont++;
                q23();
            } else if ((car[cont] >= '6') && (car[cont] <= '9')) {
                cont++;
                q27();
            } else if (car[cont] == '.') {
                cont++;
                q28();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q21() {
        //System.out.println(" En estado q13 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q24();
            } else if (car[cont] == '.') {
                cont++;
                q28();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q22() {
        //System.out.println(" En estado q14 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q25();
            } else if (car[cont] == '.') {
                cont++;
                q28();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q23() {
        //System.out.println(" En estado q15 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '5')) {
                cont++;
                q26();
            } else if (car[cont] == '.') {
                cont++;
                q28();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q24() {
        //System.out.println(" En estado q16 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q28();
            }
        } else {
            qError();
        }

    }

    public void q25() {
        //System.out.println(" En estado q17 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q28();
            }
        } else {
            qError();
        }
    }

    public void q26() {
        //System.out.println(" En estado q18 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q28();
            }
        } else {
            qError();
        }
    }

    public void q27() {
        //System.out.println(" En estado q19 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q28();
            }
        } else {
            qError();
        }
    }

    public void q28() {
        //System.out.println(" En estado q20 ");

        if (cont < car.length) {
            if (car[cont] == '0') {
                cont++;
                q49();
            } else if (car[cont] == '1') {
                cont++;
                q29();
            } else if (car[cont] == '2') {
                cont++;
                q30();
            } else if ((car[cont] >= '3') && (car[cont] <= '9')) {
                cont++;
                q31();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q29() {
        //System.out.println(" En estado q21 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q31();
            } else if (car[cont] == '.') {
                cont++;
                q38();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q30() {
        //System.out.println(" En estado q22 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '4')) {
                cont++;
                q32();
            } else if (car[cont] == '5') {
                cont++;
                q33();
            } else if ((car[cont] >= '6') && (car[cont] <= '9')) {
                cont++;
                q37();
            } else if (car[cont] == '.') {
                cont++;
                q38();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q31() {
        //System.out.println(" En estado q23 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q34();
            } else if (car[cont] == '.') {
                cont++;
                q38();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q32() {
        //System.out.println(" En estado q24 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q35();
            } else if (car[cont] == '.') {
                cont++;
                q38();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q33() {
        //System.out.println(" En estado q25 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '5')) {
                cont++;
                q36();
            } else if (car[cont] == '.') {
                cont++;
                q38();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q34() {
        //System.out.println(" En estado q26 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q38();
            }
        } else {
            qError();
        }

    }

    public void q35() {
        //System.out.println(" En estado q27 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q38();
            }
        } else {
            qError();
        }
    }

    public void q36() {
        //System.out.println(" En estado q28 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q38();
            }
        } else {
            qError();
        }
    }

    public void q37() {
        //System.out.println(" En estado q29 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q38();
            }
        } else {
            qError();
        }
    }

    public void q38() {

        //System.out.println(" En estado q30 ");

        if (cont < car.length) {
            if (car[cont] == '0') {
                qError();
            } else if (car[cont] == '1') {
                cont++;
                q39();
            } else if (car[cont] == '2') {
                cont++;
                q40();
            } else if ((car[cont] >= '3') && (car[cont] <= '9')) {
                cont++;
                q41();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q39() {
        //System.out.println(" En estado q31 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q41();
            } else {
                qError();
            }
        } else if (cont == car.length) {
            q50();
        } else {
            qError();
        }


    }

    public void q40() {

        //System.out.println(" En estado q32 ");
        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '4')) {
                cont++;
                q42();
            } else if (car[cont] == '5') {
                cont++;
                q43();
            } else if ((car[cont] >= '6') && (car[cont] <= '9')) {
                cont++;
                q47();
            } else {
                qError();
            }
        } else if (cont == car.length) {
            q50();
        } else {
            qError();
        }

    }

    public void q41() {
        //System.out.println(" En estado q33 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q44();
            } else {
                qError();
            }
        } else if (cont == car.length) {
            q50();
        } else {
            qError();
        }

    }

    public void q42() {
        //System.out.println(" En estado q34 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q45();
            } else {
                qError();
            }
        } else if (cont == car.length) {
            q50();
        } else {
            qError();
        }

    }

    public void q43() {
        //System.out.println(" En estado q35 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '5')) {
                cont++;
                q46();
            } else {
                qError();
            }
        } else if (cont == car.length) {
            q50();
        } else {
            qError();
        }
    }

    public void q44() {
        //System.out.println(" En estado q36 ");

        if (cont == car.length) {
            q50();
        } else {
            qError();
        }
    }

    public void q45() {
        //System.out.println(" En estado q37 ");

        if (cont == car.length) {
            q50();
        } else {
            qError();
        }
    }

    public void q46() {
        //System.out.println(" En estado q38 ");

        if (cont == car.length) {
            q50();
        } else {
            qError();
        }
    }

    public void q47() {
        //System.out.println(" En estado q39 ");

        if (cont == car.length) {
            q50();
        } else {
            qError();
        }
    }

    public void q48() {
        //System.out.println(" En estado q40");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q28();
            }
        } else {
            qError();
        }

    }

    public void q49() {
        //System.out.println(" En estado q41");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q38();
            }
        } else {
            qError();
        }

    }

    public void q50() {
        //System.out.println(" En estado q42 - aceptación ");
        System.out.println(" Expresión valida ");
        aceptado = true;
    }
}
