/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas;

/**
 *
 * @author Jorge
 */

//Se utiliza para saber si un nombre asignado a un dispositivo es una palabra
//reservada
public class validarNombre {
    
    
    private String[] palabrasReservadas = {"CREATE", "create", "DELETE", "delete", 
                                           "CONFIGURE", "configure", "SEND", "send",
                                           "RELATION", "relation", "NODE", "node",
                                            "SWITCH", "switch", "ROUTER", "router",
                                            "TO", "to", "FROM", "from",
                                            "PROPERTIES", "properties", "IP", "ip",
                                            "MASK", "mask", "EXIT", "exit"};
    
    public validarNombre(){
    }
    
    public boolean isReservada(String n){
        
        for (int i = 0; i < palabrasReservadas.length; i++) {
            if(n.equals(palabrasReservadas[i])){
                return true;
            }
            
        }
        return false;
    }
    
}
