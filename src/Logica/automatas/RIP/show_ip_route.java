/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas.RIP;

import Logica.automatas.Automata;

/**
 *
 * @author Estudiante
 */
public class show_ip_route extends Automata{
    
    public show_ip_route(String c) {
        super(c);
    }

    @Override
    protected void q0() {
        if (cont < car.length) {
            if (car[cont] != 's' && car[cont] != 'S') {
                qError();
            } else if (car[cont] == 's' || car[cont] == 'S') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'h' && car[cont] != 'H') {
                qError();
            } else if (car[cont] == 'h' || car[cont] == 'H') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 'o' && car[cont] != 'O') {
                qError();
            } else if (car[cont] == 'o' || car[cont] == 'O') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != 'w' && car[cont] != 'W') {
                qError();
            } else if (car[cont] == 'w' || car[cont] == 'W') {
                cont++;
                q6();
            }
        } else {
            qError();
        }
    }
    
    private void q6() {
        if (cont < car.length) {
            if (car[cont] != 'i' && car[cont] != 'I' && car[cont] != ' ') {
                qError();
            } else if (car[cont] == 'i' || car[cont] == 'I') {
                cont++;
                q7();
            } else if (car[cont] == ' ') {
                cont++;
                q6();
            }
        } else {
            qError();
        }
    }
    
    private void q7() {
        if (cont < car.length) {
            if (car[cont] != 'p' && car[cont] != 'P') {
                qError();
            } else if (car[cont] == 'p' || car[cont] == 'P') {
                cont++;
                q9();
            }
        } else {
            qError();
        }
    }
    
    private void q9() {
        if (cont < car.length) {
            if (car[cont] != 'r' && car[cont] != 'R' && car[cont] != ' ') {
                qError();
            } else if (car[cont] == 'r' || car[cont] == 'R') {
                cont++;
                q11();
            } else if (car[cont] == ' ') {
                cont++;
                q9();
            }
        } else {
            qError();
        }
    }
    private void q11() {
        if (cont < car.length) {
            if (car[cont] != 'o' && car[cont] != 'O') {
                qError();
            } else if (car[cont] == 'o' || car[cont] == 'O') {
                cont++;
                q12();
            }
        } else {
            qError();
        }
    }

    private void q12() {
        if (cont < car.length) {
            if (car[cont] != 'u' && car[cont] != 'U') {
                qError();
            } else if (car[cont] == 'u' || car[cont] == 'U') {
                cont++;
                q13();
            }
        } else {
            qError();
        }
    }

    private void q13() {
        if (cont < car.length) {
            if (car[cont] != 't' && car[cont] != 'T') {
                qError();
            } else if (car[cont] == 't' || car[cont] == 'T') {
                cont++;
                q14();
            }
        } else {
            qError();
        }
    }

    private void q14() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q15();
            }
        } else {
            qError();
        }
    }

    private void q15() {
        System.out.println(" En estado q14 - aceptación ");
        System.out.println(" Expresión valida ");
        aceptado = true;
    }
}
