/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas.RIP;

import Logica.automatas.Automata;

/**
 *
 * @author Estudiante
 */
public class clear_ip extends Automata{
    
    public clear_ip(String c) {
        super(c);
    }

    @Override
    protected void q0() {
        if (cont < car.length) {
            if (car[cont] != 'c' && car[cont] != 'C') {
                qError();
            } else if (car[cont] == 'c' || car[cont] == 'C') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'l' && car[cont] != 'L') {
                qError();
            } else if (car[cont] == 'l' || car[cont] == 'L') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != 'a' && car[cont] != 'A') {
                qError();
            } else if (car[cont] == 'a' || car[cont] == 'A') {
                cont++;
                q4();
            }
        } else {
            qError();
        }
    }

    private void q4() {
        if (cont < car.length) {
            if (car[cont] != 'r' && car[cont] != 'R') {
                qError();
            } else if (car[cont] == 'r' || car[cont] == 'R') {
                cont++;
                q6();
            }
        } else {
            qError();
        }
    }
    
    private void q6() {
        if (cont < car.length) {
            if (car[cont] != 'i' && car[cont] != 'I' && car[cont] != ' ') {
                qError();
            } else if (car[cont] == 'i' || car[cont] == 'I') {
                cont++;
                q7();
            } else if (car[cont] == ' ') {
                cont++;
                q6();
            }
        } else {
            qError();
        }
    }
    
    private void q7() {
        if (cont < car.length) {
            if (car[cont] != 'p' && car[cont] != 'P') {
                qError();
            } else if (car[cont] == 'p' || car[cont] == 'P') {
                cont++;
                q9();
            }
        } else {
            qError();
        }
    }
    
    private void q9() {
        if (cont < car.length) {
            if (car[cont] != 'r' && car[cont] != 'R' && car[cont] != ' ') {
                qError();
            } else if (car[cont] == 'r' || car[cont] == 'R') {
                cont++;
                q11();
            } else if (car[cont] == ' ') {
                cont++;
                q9();
            }
        } else {
            qError();
        }
    }

    private void q11() {
        if (cont < car.length) {
            if (car[cont] != 'o' && car[cont] != 'O') {
                qError();
            } else if (car[cont] == 'o' || car[cont] == 'O') {
                cont++;
                q12();
            }
        } else {
            qError();
        }
    }

    private void q12() {
        if (cont < car.length) {
            if (car[cont] != 'u' && car[cont] != 'U') {
                qError();
            } else if (car[cont] == 'u' || car[cont] == 'U') {
                cont++;
                q13();
            }
        } else {
            qError();
        }
    }

    private void q13() {
        if (cont < car.length) {
            if (car[cont] != 't' && car[cont] != 'T') {
                qError();
            } else if (car[cont] == 't' || car[cont] == 'T') {
                cont++;
                q14();
            }
        } else {
            qError();
        }
    }

    private void q14() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q15();
            }
        } else {
            qError();
        }
    }

    private void q15() {
        System.out.println(" En estado q14 - aceptación ");
        System.out.println(" Expresión valida ");
        aceptado = true;
    }

    private void q22() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] == '0') {
                qError();
                System.out.println("aqui es 7");
            } else if (car[cont] == ' ') {
                cont++;
                q22();
            } else if (car[cont] == '1') {
                q23();
            } else if (car[cont] == '2') {
                cont++;
                q24();
            } else if ((car[cont] >= '3') && (car[cont] <= '9')) {
                qError();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q23() {
        System.out.println(" En estado q23 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q25();
            } else if (car[cont] == '.') {
                cont++;
                q32();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q24() {

        System.out.println(" En estado q24 ");
        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '4')) {
                cont++;
                q26();
            } else if (car[cont] == '5') {
                cont++;
                q27();
            } else if ((car[cont] >= '6') && (car[cont] <= '9')) {
                cont++;
                q31();
            } else if (car[cont] == '.') {
                cont++;
                q32();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q25() {
        System.out.println(" En estado q25 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q28();
            } else if (car[cont] == '.') {
                cont++;
                q25();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q26() {
        System.out.println(" En estado q26 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q29();
            } else if (car[cont] == '.') {
                cont++;
                q32();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q27() {
        System.out.println(" En estado q27 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '5')) {
                cont++;
                q30();
            } else if (car[cont] == '.') {
                cont++;
                q32();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q28() {
        System.out.println(" En estado q28 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q32();
            } else if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q31();
            }
        } else {
            qError();
        }
    }

    public void q29() {
        System.out.println(" En estado q22 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q32();
            }
        } else {
            qError();
        }
    }

    public void q30() {
        System.out.println(" En estado q29 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q32();
            }
        } else {
            qError();
        }
    }

    public void q31() {
        System.out.println(" En estado q30 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q32();
            }
        } else {
            qError();
        }
    }

    public void q32() {
        System.out.println(" En estado q32 ");

        if (cont < car.length) {
            if (car[cont] == '0') {
                cont++;
                q62();
            } else if (car[cont] == '1') {
                cont++;
                q33();
            } else if (car[cont] == '2') {
                cont++;
                q34();
            } else if ((car[cont] >= '3') && (car[cont] <= '9')) {
                cont++;
                q35();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q33() {
        System.out.println(" En estado q33 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q35();
            } else if (car[cont] == '.') {
                cont++;
                q42();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q34() {
        System.out.println(" En estado q34 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '4')) {
                cont++;
                q36();
            } else if (car[cont] == '5') {
                cont++;
                q37();
            } else if ((car[cont] >= '6') && (car[cont] <= '9')) {
                cont++;
                q41();
            } else if (car[cont] == '.') {
                cont++;
                q42();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q35() {
        System.out.println(" En estado q35 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q38();
            } else if (car[cont] == '.') {
                cont++;
                q42();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q36() {
        System.out.println(" En estado q36 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q39();
            } else if (car[cont] == '.') {
                cont++;
                q42();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q37() {
        System.out.println(" En estado q37 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '5')) {
                cont++;
                q40();
            } else if (car[cont] == '.') {
                cont++;
                q42();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q38() {
        System.out.println(" En estado q38 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q42();
            }
        } else {
            qError();
        }

    }

    public void q39() {
        System.out.println(" En estado q397 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q42();
            }
        } else {
            qError();
        }
    }

    public void q40() {
        System.out.println(" En estado q40 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q42();
            }
        } else {
            qError();
        }
    }

    public void q41() {
        System.out.println(" En estado q41 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q42();
            }
        } else {
            qError();
        }
    }

    public void q42() {
        System.out.println(" En estado q42 ");

        if (cont < car.length) {
            if (car[cont] == '0') {
                cont++;
                q63();
            } else if (car[cont] == '1') {
                cont++;
                q43();
            } else if (car[cont] == '2') {
                cont++;
                q44();
            } else if ((car[cont] >= '3') && (car[cont] <= '9')) {
                cont++;
                q45();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q43() {
        System.out.println(" En estado q43 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q45();
            } else if (car[cont] == '.') {
                cont++;
                q52();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q44() {
        System.out.println(" En estado q44 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '4')) {
                cont++;
                q46();
            } else if (car[cont] == '5') {
                cont++;
                q47();
            } else if ((car[cont] >= '6') && (car[cont] <= '9')) {
                cont++;
                q51();
            } else if (car[cont] == '.') {
                cont++;
                q52();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q45() {
        System.out.println(" En estado q45 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q48();
            } else if (car[cont] == '.') {
                cont++;
                q52();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q46() {
        System.out.println(" En estado q46 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q49();
            } else if (car[cont] == '.') {
                cont++;
                q52();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q47() {
        System.out.println(" En estado q47 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '5')) {
                cont++;
                q50();
            } else if (car[cont] == '.') {
                cont++;
                q52();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q48() {
        System.out.println(" En estado q48 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q52();
            }
        } else {
            qError();
        }

    }

    public void q49() {
        System.out.println(" En estado q49 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q52();
            }
        } else {
            qError();
        }
    }

    public void q50() {
        System.out.println(" En estado q50 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q52();
            }
        } else {
            qError();
        }
    }

    public void q51() {
        System.out.println(" En estado q51 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q52();
            }
        } else {
            qError();
        }
    }

    public void q52() {

        System.out.println(" En estado q52 ");

        if (cont < car.length) {
            if (car[cont] == '0') {
                cont++;
                q58();
            } else if (car[cont] == '1') {
                cont++;
                q53();
            } else if (car[cont] == '2') {
                cont++;
                q54();
            } else if ((car[cont] >= '3') && (car[cont] <= '9')) {
                cont++;
                q55();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q53() {
        System.out.println(" En estado q53 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q55();
            } else {
                qError();
            }
        } else if (cont == car.length) {
            q64();
        } else {
            qError();
        }


    }

    public void q54() {

        System.out.println(" En estado q54 ");
        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '4')) {
                cont++;
                q56();
            } else if (car[cont] == '5') {
                cont++;
                q57();
            } else if ((car[cont] >= '6') && (car[cont] <= '9')) {
                cont++;
                q61();
            } else {
                qError();
            }
        } else if (cont == car.length) {
            q64();
        } else {
            qError();
        }

    }

    public void q55() {
        System.out.println(" En estado q55 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q58();
            } else {
                qError();
            }
        } else if (cont == car.length) {
            q64();
        } else {
            qError();
        }

    }

    public void q56() {
        System.out.println(" En estado q56 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q59();
            } else {
                qError();
            }
        } else if (cont == car.length) {
            q64();
        } else {
            qError();
        }

    }

    public void q57() {
        System.out.println(" En estado q57 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '5')) {
                cont++;
                q60();
            } else {
                qError();
            }
        } else if (cont == car.length) {
            q64();
        } else {
            qError();
        }
    }

    public void q58() {
        System.out.println(" En estado q58 ");

        if (cont == car.length) {
            q64();
        } else {
            qError();
        }
    }

    public void q59() {
        System.out.println(" En estado q59 ");

        if (cont == car.length) {
            q64();
        } else {
            qError();
        }
    }

    public void q60() {
        System.out.println(" En estado q60 ");

        if (cont == car.length) {
            q64();
        } else {
            qError();
        }
    }

    public void q61() {
        System.out.println(" En estado q61 ");

        if (cont == car.length) {
            q64();
        } else {
            qError();
        }
    }

    public void q62() {
        System.out.println(" En estado q62");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q42();
            }
        } else {
            qError();
        }

    }

    public void q63() {
        System.out.println(" En estado q63");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q52();
            }
        } else {
            qError();
        }

    }

    public void q64() {
        System.out.println(" En estado q64 - aceptación ");
        System.out.println(" Expresión valida ");
        aceptado = true;
    }
}
