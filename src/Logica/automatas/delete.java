/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas;

/**
 *
 * @author Jorge
 */
public class delete {
    
    private int cont;
    private char[] car;
    private String c;
    private boolean aceptado = false;
    private String salida;
    private String nombre;
    
    public delete(String c){
        this.c = c;
        car = c.toCharArray();
        salida = "";
        nombre = "";
    }

    public boolean inicio(){
        cont = 0;
        q0();
        return aceptado;
    }
    
    public void q0(){
        System.out.println(" En q0 ");
        
        if(cont < car.length){
            if(car[cont] == 'D' || car[cont]=='d'){
                cont++;
                q1();
            }else
                qError(cont);
        }
    }
    
    public void q1(){
        System.out.println(" En q1 ");
        
        if(cont < car.length){
            if(car[cont] == 'E' || car[cont]=='e'){
                cont++;
                q2();
            }else
                qError(cont);
        }
    }
    
    public void q2(){
        System.out.println(" En q2 ");
        
        if(cont < car.length){
            if(car[cont] == 'L' || car[cont]=='l'){
                cont++;
                q3();
            }else
                qError(cont);
        }
    }
    
    public void q3(){
        System.out.println(" En q3 ");
        
        if(cont < car.length){
            if(car[cont] == 'E' || car[cont]=='e'){
                cont++;
                q4();
            }else
                qError(cont);
        }
    }
    
    public void q4(){
        System.out.println(" En q4 ");
        
        if(cont < car.length){
            if(car[cont] == 'T' || car[cont]=='t'){
                cont++;
                q5();
            }else
                qError(cont);
        }
    }
    
    public void q5(){
        System.out.println(" En q5 ");
        
        if(cont < car.length){
            if(car[cont] == 'E' || car[cont]=='e'){
                cont++;
                q6();
            }else
                qError(cont);
        }
    }
    
    public void q6(){
        System.out.println(" En q6 ");
        
        if(cont < car.length){
            if(car[cont] == ' '){
                cont++;
                q7();
            }else
                qError(cont);
        }
    }
    
    public void q7(){
        System.out.println(" En q7 ");
        
        if(cont < car.length){
                nombre += car[cont];
                cont++;
                q7();
        }
        else if(cont == car.length){
            aceptado = true;
        }
        else
            qError(cont);
    }
    
   public void qError(int indice){
       
       if((indice > 0) && (indice < car.length-1)){
            
            salida += c.substring(0, indice);
            salida += "<u>" + String.valueOf(c.charAt(indice)) + "</u>";
            salida += c.substring(indice+1) + "\n";
            
        }
        //Si el indice es 0
        else if(indice == 0){
            salida += "<u>" + String.valueOf(c.charAt(indice));
            salida += "</u>" + c.substring(indice+1);
            
            
        }
        // Si el indice está al final
        else if (indice == car.length-1){
            salida += c.substring(0, indice);
            salida += "<u>" + String.valueOf(c.charAt(indice)) + "</u>";
            
        }
       
       aceptado = false;
   }
    
    /**
     * @return the cont
     */
    public int getCont() {
        return cont;
    }

    /**
     * @param cont the cont to set
     */
    public void setCont(int cont) {
        this.cont = cont;
    }

    /**
     * @return the car
     */
    public char[] getCar() {
        return car;
    }

    /**
     * @param car the car to set
     */
    public void setCar(char[] car) {
        this.car = car;
    }

    /**
     * @return the c
     */
    public String getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(String c) {
        this.c = c;
    }

    /**
     * @return the aceptado
     */
    public boolean isAceptado() {
        return aceptado;
    }

    /**
     * @param aceptado the aceptado to set
     */
    public void setAceptado(boolean aceptado) {
        this.aceptado = aceptado;
    }

    /**
     * @return the salida
     */
    public String getSalida() {
        return salida;
    }

    /**
     * @param salida the salida to set
     */
    public void setSalida(String salida) {
        this.salida = salida;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
