/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas;

/**
 *
 * @author Jorge
 */
public class configureNode {

    private int cont;
    private char[] car;
    private String c;
    private boolean aceptado = false;
    private String salida;
    private String nombreActual;
    private String ip;
    private String mascara;
    private String nuevoNombre;

    public configureNode(String c) {
        this.c = c;
        car = c.toCharArray();
        salida = "";
        nombreActual = "";
        ip = "";
        mascara = "";
        nuevoNombre = "";
    }

    public boolean inicio() {
        cont = 0;
        q0();
        return aceptado;
    }

    public void q0() {
        System.out.println(" En q0 ");

        if (cont < car.length) {
            if (car[cont] == 'C' || car[cont]=='c') {
                cont++;
                q1();
            } else {
                qError(cont);
            }
        }
    }

    public void q1() {
        System.out.println(" En q1 ");

        if (cont < car.length) {
            if (car[cont] == 'O' || car[cont]=='o') {
                cont++;
                q2();
            } else {
                qError(cont);
            }
        }
    }

    public void q2() {
        System.out.println(" En q2 ");

        if (cont < car.length) {
            if (car[cont] == 'N' || car[cont]=='n') {
                cont++;
                q3();
            } else {
                qError(cont);
            }
        }
    }

    public void q3() {
        System.out.println(" En q3 ");

        if (cont < car.length) {
            if (car[cont] == 'F' || car[cont]=='f') {
                cont++;
                q4();
            } else {
                qError(cont);
            }
        }
    }

    public void q4() {
        System.out.println(" En q4 ");

        if (cont < car.length) {
            if (car[cont] == 'I' || car[cont]=='i') {
                cont++;
                q5();
            } else {
                qError(cont);
            }
        }
    }

    public void q5() {
        System.out.println(" En q5 ");

        if (cont < car.length) {
            if (car[cont] == 'G' || car[cont]=='g') {
                cont++;
                q6();
            } else {
                qError(cont);
            }
        }
    }

    public void q6() {
        System.out.println(" En q6 ");

        if (cont < car.length) {
            if (car[cont] == 'U' || car[cont]=='u') {
                cont++;
                q7();
            } else {
                qError(cont);
            }
        }
    }

    public void q7() {
        System.out.println(" En q7 ");

        if (cont < car.length) {
            if (car[cont] == 'R' || car[cont]=='r') {
                cont++;
                q8();
            } else {
                qError(cont);
            }
        }
    }

    public void q8() {
        System.out.println(" En q8 ");

        if (cont < car.length) {
            if (car[cont] == 'E' || car[cont]=='e') {
                cont++;
                q9();
            } else {
                qError(cont);
            }
        }
    }

    public void q9() {
        System.out.println(" En q9 ");

        if (cont < car.length) {
            if (car[cont] == ' ') {
                cont++;
                q10();
            } else {
                qError(cont);
            }
        }
    }

    public void q10() {
        System.out.println(" En q10 ");

        if (cont < car.length) {
            if (car[cont] == 'N' || car[cont]=='n') {
                cont++;
                q11();
            } else {
                qError(cont);
            }
        }
    }

    public void q11() {
        System.out.println(" En q11 ");

        if (cont < car.length) {
            if (car[cont] == 'O' || car[cont]=='o') {
                cont++;
                q12();
            } else {
                qError(cont);
            }
        }
    }

    public void q12() {
        System.out.println(" En q12 ");

        if (cont < car.length) {
            if (car[cont] == 'D' || car[cont]=='d') {
                cont++;
                q13();
            } else {
                qError(cont);
            }
        }
    }

    public void q13() {
        System.out.println(" En q13 ");

        if (cont < car.length) {
            if (car[cont] == 'E' || car[cont]=='e') {
                cont++;
                q14();
            } else {
                qError(cont);
            }
        }
    }

    public void q14() {
        System.out.println(" En q14 ");

        if (cont < car.length) {
            if (car[cont] == ' ') {
                cont++;
                q15();
            } else {
                qError(cont);
            }
        }
    }

    //Aquí se obtiene el nombre del nodo a configurar
    public void q15() {
        System.out.println(" En q15 ");


        if (cont < car.length) {

            if (car[cont] != ' ') {
                nombreActual += car[cont];
                System.out.println(nombreActual);
                cont++;
                q15();

            } else if (car[cont] == ' ') {


                //Se valida si ya el nombre terminó
                if ((car[cont+ 1]  == 'I' || car[cont]=='i') && (car[cont+ 2]  == 'P' || car[cont]=='p') && car[cont+ 3]  == ' ') {
                    cont++;
                    q16();
                }
                else {
                    nombreActual += car[cont];
                    System.out.println(nombreActual);
                    cont++;
                    q15();
                }
            }
        }

    }

    public void q16() {
        System.out.println(" En q16 ");

        if (cont < car.length) {
            if (car[cont] == 'I' || car[cont]=='i') {
                cont++;
                q17();
            } else {
                qError(cont);
            }
        }
    }

    public void q17() {
        System.out.println(" En q17 ");

        if (cont < car.length) {
            if (car[cont] == 'P' || car[cont]=='p') {
                cont++;
                q18();
            } else {
                qError(cont);
            }
        }
    }

    public void q18() {
        System.out.println(" En q18 ");

        if (cont < car.length) {
            if (car[cont] == ' ') {
                cont++;
                q19();
            } else {
                qError(cont);
            }
        }
    }

    //Aquí se valida la ip con el autómata de las ip, si es válida sigue sino, error
    public void q19() {
        System.out.println(" En q19 ");

        if (cont < car.length) {
            //Se forma la ip
            if (car[cont] != ' ') {
                ip += car[cont];
                cont++;
                q19();
                //se válida para pasar al siguiente estado    
            } else if (car[cont] == ' ') {
                AutomataIP aip = new AutomataIP(ip);
                if (aip.inicio()) {
                    cont++;
                    q20();
                } else {
                    //Se manda un -1 a estado de error para saber que es
                    //error de ip
                    qError(-1);
                }
            } else {
                qError(cont);
            }
        }
    }

    public void q20() {
        System.out.println(" En q20 ");

        if (cont < car.length) {
            if (car[cont] == 'M' || car[cont]=='m') {
                cont++;
                q21();
            } else {
                qError(cont);
            }
        }
    }

    public void q21() {
        System.out.println(" En q21 ");

        if (cont < car.length) {
            if (car[cont] == 'A' || car[cont]=='a') {
                cont++;
                q22();
            } else {
                qError(cont);
            }
        }
    }

    public void q22() {
        System.out.println(" En q22 ");

        if (cont < car.length) {
            if (car[cont] == 'S' || car[cont]=='s') {
                cont++;
                q23();
            } else {
                qError(cont);
            }
        }
    }

    public void q23() {
        System.out.println(" En q23 ");

        if (cont < car.length) {
            if (car[cont] == 'K' || car[cont]=='k') {
                cont++;
                q24();
            } else {
                qError(cont);
            }
        }
    }

    public void q24() {
        System.out.println(" En q24 ");

        if (cont < car.length) {
            if (car[cont] == ' ') {
                cont++;
                q25();
            } else {
                qError(cont);
            }
        }
    }

    //Similar a como se validó la ip, se hace con la máscara, usando el 
    //respectivo autómata.
    public void q25() {
        System.out.println(" En q25 ");

        if (cont < car.length) {
            //Se forma la mascara
            if (car[cont] != ' ') {

                mascara += car[cont];
                cont++;
                q25();

            } else if (car[cont] == ' ') {

                AutomataMascara am = new AutomataMascara(mascara);

                if (am.inicio()) {

                    cont++;
                    q26();

                } else //-2 indica que es error de máscara
                {
                    qError(-2);
                }
            } else {
                qError(cont);
            }
        }
    }

    public void q26() {
        System.out.println(" En q26 ");

        if (cont < car.length) {
            if (car[cont] == 'N' || car[cont]=='n') {
                cont++;
                q27();
            } else {
                qError(cont);
            }
        }
    }

    public void q27() {
        System.out.println(" En q27 ");

        if (cont < car.length) {
            if (car[cont] == 'A' || car[cont]=='a') {
                cont++;
                q28();
            } else {
                qError(cont);
            }
        }
    }

    public void q28() {
        System.out.println(" En q28 ");

        if (cont < car.length) {
            if (car[cont] == 'M' || car[cont]=='m') {
                cont++;
                q29();
            } else {
                qError(cont);
            }
        }
    }

    public void q29() {
        System.out.println(" En q29 ");

        if (cont < car.length) {
            if (car[cont] == 'E' || car[cont]=='e') {
                cont++;
                q30();
            } else {
                qError(cont);
            }
        }
    }

    public void q30() {
        System.out.println(" En q30 ");

        if (cont < car.length) {
            if (car[cont] == ' ') {
                cont++;
                q31();
            } else {
                qError(cont);
            }
        }
    }

    //Aquí se forma el nuevo nombre del dispositivo
    // y es estado de aceptación
    public void q31() {
        System.out.println(" En q31 ");

        if (cont < car.length) {
            nuevoNombre += car[cont];
            cont++;
            q31();
        } else if (cont == car.length) {
            validarNombre vn = new validarNombre();

            if (!vn.isReservada(nuevoNombre)) {
                aceptado = true;
            } else {
                qError(-3);
            }
        }
    }

    public void qError(int indice) {

        if ((indice > 0) && (indice < car.length - 1)) {

            salida += c.substring(0, indice);
            salida += "<u>" + String.valueOf(c.charAt(indice)) + "</u>";
            salida += c.substring(indice + 1) + "\n";

        } //Si el indice es 0
        else if (indice == 0) {
            salida += "<u>" + String.valueOf(c.charAt(indice));
            salida += "</u>" + c.substring(indice + 1);


        } // Si el indice está al final
        else if (indice == car.length - 1) {
            salida += c.substring(0, indice);
            salida += "<u>" + String.valueOf(c.charAt(indice)) + "</u>";

        } //Si el indice es -1 el error es de ip
        else if (indice == -1) {
            salida += "Dirección ip inválida";
        } //Si el indice es -2 el error es de máscara
        else if (indice == -2) {
            salida += "Máscara inválida";
        } //Si el indice es -3 el error es porque el nombre es palabra reservada
        else if (indice == -3) {
            salida += "El nombre del dispositivo no debe ser una palabra"
                    + " reservada del sistema";
        }

        aceptado = false;
    }

    /**
     * @return the cont
     */
    public int getCont() {
        return cont;
    }

    /**
     * @param cont the cont to set
     */
    public void setCont(int cont) {
        this.cont = cont;
    }

    /**
     * @return the car
     */
    public char[] getCar() {
        return car;
    }

    /**
     * @param car the car to set
     */
    public void setCar(char[] car) {
        this.car = car;
    }

    /**
     * @return the c
     */
    public String getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(String c) {
        this.c = c;
    }

    /**
     * @return the aceptado
     */
    public boolean isAceptado() {
        return aceptado;
    }

    /**
     * @param aceptado the aceptado to set
     */
    public void setAceptado(boolean aceptado) {
        this.aceptado = aceptado;
    }

    /**
     * @return the salida
     */
    public String getSalida() {
        return salida;
    }

    /**
     * @param salida the salida to set
     */
    public void setSalida(String salida) {
        this.salida = salida;
    }

    /**
     * @return the nombreActual
     */
    public String getNombreActual() {
        return nombreActual;
    }

    /**
     * @param nombreActual the nombreActual to set
     */
    public void setNombreActual(String nombreActual) {
        this.nombreActual = nombreActual;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the mascara
     */
    public String getMascara() {
        return mascara;
    }

    /**
     * @param mascara the mascara to set
     */
    public void setMascara(String mascara) {
        this.mascara = mascara;
    }

    /**
     * @return the nuevoNombre
     */
    public String getNuevoNombre() {
        return nuevoNombre;
    }

    /**
     * @param nuevoNombre the nuevoNombre to set
     */
    public void setNuevoNombre(String nuevoNombre) {
        this.nuevoNombre = nuevoNombre;
    }
}
