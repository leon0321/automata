/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas;

/**
 * Esta clase extiende de la clase Autómata y se encarga de validar la correcta
 * escritura de el comando ping
 *
 * @author leonardo ortega hernandez
 * @see Automata
 */
public class APing extends Automata {

    public APing(String c) {
        super(c);
    }

    @Override
    protected void q0() {
        if (cont < car.length) {
            if (car[cont] != 'p' && car[cont] != 'P') {
                qError();
            } else if (car[cont] == 'p' || car[cont] == 'P') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'i' && car[cont] != 'I') {
                qError();
            } else if (car[cont] == 'i' || car[cont] == 'I') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 'n' && car[cont] != 'N') {
                qError();
            } else if (car[cont] == 'n' || car[cont] == 'N') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != 'g' && car[cont] != 'G') {
                qError();
            } else if (car[cont] == 'g' || car[cont] == 'G') {
                cont++;
                q4();
            }
        } else {
            qError();
        }
    }

    private void q4() {
        System.out.println(" En estado q4 - aceptación ");
        System.out.println(" Expresión valida ");
        aceptado = false;
        if (cont == car.length) {
            aceptado = true;
        }
    }
}
