/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas;

/**
 *
 * @author Jorge
 */
public class createRelation {
    
    private int cont;
    private char[] car;
    private String c;
    private String port1;
    private String port2;
    private boolean aceptado = false;
    private String salida;
    private String dispositivoInicial;
    private String dispositivoFinal;

    public createRelation(String c){
        this.c = c;
        car = c.toCharArray();
        salida = "";
        dispositivoInicial = "";
        dispositivoFinal = "";
        port1 = "";
        port2 = "";
    }
    
    public boolean inicio(){
        cont = 0;
        q0();
        return aceptado;
    }
    
    public void q0(){
        System.out.println(" En q0 ");
        
        if(cont < car.length){
            if(car[cont] == 'C' || car[cont]=='c'){
                cont++;
                q1();
            }else
                qError(cont);
        }
    }
    
    public void q1(){
        System.out.println(" En q1 ");
        
        if(cont < car.length){
            if(car[cont] == 'R' || car[cont]=='r'){
                cont++;
                q2();
            }else
                qError(cont);
        }
    }
    
    public void q2(){
        System.out.println(" En q2 ");
        
        if(cont < car.length){
            if(car[cont] == 'E' || car[cont]=='e'){
                cont++;
                q3();
            }else
                qError(cont);
        }
    }
    
    public void q3(){
        System.out.println(" En q3 ");
        
        if(cont < car.length){
            if(car[cont] == 'A' || car[cont]=='a'){
                cont++;
                q4();
            }else
                qError(cont);
        }
    }
    
    public void q4(){
        System.out.println(" En q4 ");
        
        if(cont < car.length){
            if(car[cont] == 'T' || car[cont]=='t'){
                cont++;
                q5();
            }else
                qError(cont);
        }
    }
    
    public void q5(){
        System.out.println(" En q5 ");
        
        if(cont < car.length){
            if(car[cont] == 'E' || car[cont]=='e'){
                cont++;
                q6();
            }else
                qError(cont);
        }
    }
    
    public void q6(){
        System.out.println(" En q6 ");
        
        if(cont < car.length){
            if(car[cont] == ' '){
                cont++;
                q7();
            }else
                qError(cont);
        }
    }
    
    public void q7(){
        System.out.println(" En q7 ");
        
        if(cont < car.length){
            if(car[cont] == 'R' || car[cont]=='r'){
                cont++;
                q8();
            }else
                qError(cont);
        }
    }
    
    public void q8(){
        System.out.println(" En q8 ");
        
        if(cont < car.length){
            if(car[cont] == 'E' || car[cont]=='e'){
                cont++;
                q9();
            }else
                qError(cont);
        }
    }
    
    public void q9(){
        System.out.println(" En q9 ");
        
        if(cont < car.length){
            if(car[cont] == 'L' || car[cont]=='l'){
                cont++;
                q10();
            }else
                qError(cont);
        }
    }
    
    public void q10(){
        System.out.println(" En q10 ");
        
        if(cont < car.length){
            if(car[cont] == 'A' || car[cont]=='a'){
                cont++;
                q11();
            }else
                qError(cont);
        }
    }
    
    public void q11(){
        System.out.println(" En q11 ");
        
        if(cont < car.length){
            if(car[cont] == 'T' || car[cont]=='t'){
                cont++;
                q12();
            }else
                qError(cont);
        }
    }
    
    public void q12(){
        System.out.println(" En q12 ");
        
        if(cont < car.length){
            if(car[cont] == 'I' || car[cont]=='i'){
                cont++;
                q13();
            }else
                qError(cont);
        }
    }
    
    public void q13(){
        System.out.println(" En q13 ");
        
        if(cont < car.length){
            if(car[cont] == 'O' || car[cont]=='o'){
                cont++;
                q14();
            }else
                qError(cont);
        }
    }
    
    public void q14(){
        System.out.println(" En q14 ");
        
        if(cont < car.length){
            if(car[cont] == 'N' || car[cont]=='n'){
                cont++;
                q15();
            }else
                qError(cont);
        }
    }
    
    public void q15(){
        System.out.println(" En q15 ");
        
        if(cont < car.length){
            if(car[cont] == ' '){
                cont++;
                q16();
            }else
                qError(cont);
        }
    }
    
    public void q16(){
        System.out.println(" En q16 ");
        
        if(cont < car.length){
            if(car[cont] == 'F' || car[cont]=='f'){
                cont++;
                q17();
            }else
                qError(cont);
        }
    }
    
    public void q17(){
        System.out.println(" En q17 ");
        
        if(cont < car.length){
            if(car[cont] == 'R' || car[cont]=='r'){
                cont++;
                q18();
            }else
                qError(cont);
        }
    }
    
    public void q18(){
        System.out.println(" En q18 ");
        
        if(cont < car.length){
            if(car[cont] == 'O' || car[cont]=='o'){
                cont++;
                q19();
            }else
                qError(cont);
        }
    }
    
    public void q19(){
        System.out.println(" En q19 ");
        
        if(cont < car.length){
            if(car[cont] == 'M' || car[cont]=='m'){
                cont++;
                q20();
            }else
                qError(cont);
        }
    }
    
    public void q20(){
        System.out.println(" En q20 ");
        
        if(cont < car.length){
            if(car[cont] == ' '){
                cont++;
                q21();
            }else
                qError(cont);
        }
    }
    
    //Aquí se obtiene el nombre del dispositivo1
    
    public void q21(){
        System.out.println(" En q21 ");
        
        if(cont < car.length){
            if(car[cont] != ' '){
                dispositivoInicial += car[cont];
                cont++;
                q21();
                
            }else if(car[cont] == ' '){
                
                //Se valida si ya el nombre terminó
                if ((car[cont+ 1]  == 'P' || car[cont]=='p') && (car[cont+ 2]  == 'O' || car[cont]=='o') && (car[cont+ 3]  == 'R' || car[cont]=='r') && (car[cont + 4] == 'T' || car[cont]=='t')
                        && car[cont + 5] == ' ') {
                    cont++;
                    q22();
                }
                else {
                    dispositivoInicial += car[cont];
                    cont++;
                    q21();
                }
            }
        }
    }
    
    public void q22(){
        System.out.println(" En q22 ");
        
        if(cont < car.length){
            if(car[cont] == 'P'  || car[cont]=='p'){
                cont++;
                q23();
            }else
                qError(cont);
        }
    }
    
    public void q23(){
        System.out.println(" En q23 ");
        
        if(cont < car.length){
            if(car[cont] == 'O'  || car[cont]=='o'){
                cont++;
                q24();
            }else
                qError(cont);
        }
    }
    
    public void q24(){
        System.out.println(" En q24 ");
        
        if(cont < car.length){
            if(car[cont] == 'R' || car[cont]=='r'){
                cont++;
                q25();
            }else
                qError(cont);
        }
    }
    
    public void q25(){
        System.out.println(" En q25 ");
        
        if(cont < car.length){
            if(car[cont] == 'T' || car[cont]=='t'){
                cont++;
                q26();
            }else
                qError(cont);
        }
    }
    
    public void q26(){
        System.out.println(" En q26 ");
        
        if(cont < car.length){
            if(car[cont] == ' '){
                cont++;
                q27();
            }else
                qError(cont);
        }
    }
    
    public void q27(){
        System.out.println(" En q27 ");
        
        if(cont < car.length){
            if(car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3'){
                System.out.println(car[cont]);
                port1 += car[cont];
                cont++;
                q28();
            }else
                qError(-1);
        }
    }
    
    public void q28(){
        System.out.println(" En q28 ");
        
        if(cont < car.length){
            if(car[cont] == ' '){
                cont++;
                q29();
            }else
                qError(cont);
        }
    }
    
    public void q29(){
        System.out.println(" En q29 ");
        
        if(cont < car.length){
            if(car[cont] == 'T' || car[cont]=='t'){
                cont++;
                q30();
            }else
                qError(cont);
        }
    }
    
    public void q30(){
        System.out.println(" En q30 ");
        
        if(cont < car.length){
            if(car[cont] == 'O' || car[cont]=='o'){
                cont++;
                q31();
            }else
                qError(cont);
        }
    }
    
    public void q31(){
        System.out.println(" En q32 ");
        
        if(cont < car.length){
            if(car[cont] == ' '){
                cont++;
                q32();
            }else
                qError(cont);
        }
    }
    
    //Aquí se obtiene el nombre del dispositivo 2
    
    public void q32(){
        System.out.println(" En q32 ");
        
        if(cont < car.length){
            if(car[cont] != ' '){
                dispositivoFinal += car[cont];
                cont++;
                q32();
                
            }else if(car[cont] == ' '){
                
                //Se valida si ya el nombre terminó
                if ((car[cont+ 1]  == 'P' || car[cont]=='p') && (car[cont+ 2]  == 'O' || car[cont]=='o') && (car[cont+ 3]  == 'R' || car[cont]=='r') && (car[cont + 4] == 'T' || car[cont]=='t')
                        && car[cont + 5] == ' ') {
                    cont++;
                    q33();
                }
                else {
                    dispositivoFinal += car[cont];
                    cont++;
                    q32();
                }
            }
        }
    }
    
    public void q33(){
        System.out.println(" En q33 ");
        
        if(cont < car.length){
            if(car[cont] == 'P' || car[cont]=='p'){
                cont++;
                q34();
            }else
                qError(cont);
        }
    }
    
    public void q34(){
        System.out.println(" En q34 ");
        
        if(cont < car.length){
            if(car[cont] == 'O' || car[cont]=='o'){
                cont++;
                q35();
            }else
                qError(cont);
        }
    }
    
    public void q35(){
        System.out.println(" En q35 ");
        
        if(cont < car.length){
            if(car[cont] == 'R' || car[cont]=='r'){
                cont++;
                q36();
            }else
                qError(cont);
        }
    }
    
    public void q36(){
        System.out.println(" En q36 ");
        
        if(cont < car.length){
            if(car[cont] == 'T' || car[cont]=='t'){
                cont++;
                q37();
            }else
                qError(cont);
        }
    }
    
    public void q37(){
        System.out.println(" En q37 ");
        
        if(cont < car.length){
            if(car[cont] == ' '){
                cont++;
                q38();
            }else
                qError(cont);
        }
    }
    
    //Aquí se obtiene el puerto del dispositivo final
    public void q38(){
        System.out.println(" En q38 ");
        
        
            if(car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3'){
                port2 += car[cont];
                cont++;
                q28();
                aceptado = true;
            }else
                qError(-2);
        
    }
  
    public void qError(int indice){
       
       if((indice > 0) && (indice < car.length-1)){
            
            salida += c.substring(0, indice);
            salida += "<u>" + String.valueOf(c.charAt(indice)) + "</u>";
            salida += c.substring(indice+1) + "\n";
            
        }
        //Si el indice es 0
        else if(indice == 0){
            salida += "<u>" + String.valueOf(c.charAt(indice));
            salida += "</u>" + c.substring(indice+1);
            
            
        }
        // Si el indice está al final
        else if (indice == car.length-1){
            salida += c.substring(0, indice);
            salida += "<u>" + String.valueOf(c.charAt(indice)) + "</u>";
            
        }
       
       //Si el índice es -1 hay errores con el puerto del primer dispositivo
       else if (indice == -1){
            salida += "Verificar el campo PORT para dispositivo origen";
            
        }
       
       //Si el índice es -2 hay errores con el puerto del primer dispositivo
       else if (indice == -2){
            salida += "Verificar el campo PORT para dispositivo destino";
            
        }
       
       aceptado = false;
   }
    
    /**
     * @return the cont
     */
    public int getCont() {
        return cont;
    }

    /**
     * @param cont the cont to set
     */
    public void setCont(int cont) {
        this.cont = cont;
    }

    /**
     * @return the car
     */
    public char[] getCar() {
        return car;
    }

    /**
     * @param car the car to set
     */
    public void setCar(char[] car) {
        this.car = car;
    }

    /**
     * @return the c
     */
    public String getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(String c) {
        this.c = c;
    }

    /**
     * @return the aceptado
     */
    public boolean isAceptado() {
        return aceptado;
    }

    /**
     * @param aceptado the aceptado to set
     */
    public void setAceptado(boolean aceptado) {
        this.aceptado = aceptado;
    }

    /**
     * @return the salida
     */
    public String getSalida() {
        return salida;
    }

    /**
     * @param salida the salida to set
     */
    public void setSalida(String salida) {
        this.salida = salida;
    }

    /**
     * @return the dispositivoInicial
     */
    public String getDispositivoInicial() {
        return dispositivoInicial;
    }

    /**
     * @param dispositivoInicial the dispositivoInicial to set
     */
    public void setDispositivoInicial(String dispositivoInicial) {
        this.dispositivoInicial = dispositivoInicial;
    }

    /**
     * @return the dispositivoFinal
     */
    public String getDispositivoFinal() {
        return dispositivoFinal;
    }

    /**
     * @param dispositivoFinal the dispositivoFinal to set
     */
    public void setDispositivoFinal(String dispositivoFinal) {
        this.dispositivoFinal = dispositivoFinal;
    }

    /**
     * @return the port1
     */
    public String getPort1() {
        return port1;
    }

    /**
     * @param port1 the port1 to set
     */
    public void setPort1(String port1) {
        this.port1 = port1;
    }

    /**
     * @return the port2
     */
    public String getPort2() {
        return port2;
    }

    /**
     * @param port2 the port2 to set
     */
    public void setPort2(String port2) {
        this.port2 = port2;
    }
}
