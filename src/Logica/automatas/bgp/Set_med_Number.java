/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas.bgp;

import Logica.automatas.Automata;

/**
 * Automata que valida la sintaxis de la instruccion set-med que es utilizado
 * para definir el valor de la metrica del router bgp
 *
 * @author Efrain Hernandez Gonzalez
 */
public class Set_med_Number extends Automata {

    public Set_med_Number(String c) {
        super(c);
    }

    @Override
    protected void q0() {
        if (cont < car.length) {
            if (car[cont] != 's' && car[cont] != 'S') {
                qError();
            } else if (car[cont] == 's' || car[cont] == 'S') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 't' && car[cont] != 'T') {
                qError();
            } else if (car[cont] == 't' || car[cont] == 'T') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != '-') {
                qError();
            } else if (car[cont] == '-') {
                cont++;
                q4();
            }
        } else {
            qError();
        }
    }

    private void q4() {
        if (cont < car.length) {
            if (car[cont] != 'm' && car[cont] != 'M') {
                qError();
            } else if (car[cont] == 'm' || car[cont] == 'M') {
                cont++;
                q5();
            }
        } else {
            qError();
        }
    }

    private void q5() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q6();
            }
        } else {
            qError();
        }
    }

    private void q6() {
        if (cont < car.length) {
            if (car[cont] != 'd' && car[cont] != 'D') {
                qError();
            } else if (car[cont] == 'd' || car[cont] == 'D') {
                cont++;
                q7();
            }
        } else {
            qError();
        }
    }

    private void q7() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == ' ') {
                cont++;
                q7();
            } else if (car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                cont++;
                q8();
            }
        } else {
            qError();
        }
    }

    private void q8() {
        if (cont < car.length) {
            if (car[cont] != '0' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                cont++;
                q9();
            }
        } else {
            qError();
        }

    }

    private void q9() {
        if (cont < car.length) {
            if (car[cont] != '0' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                cont++;
                q10();
            }
        } else {
            qError();
        }
    }

    private void q10() {
        if (cont < car.length) {
            if (car[cont] != '0' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                q9();
                System.out.println(" por aqui ");
            }
        } else {
            System.out.println(" Expresión valida ");
            aceptado = true;
        }
    }
}
