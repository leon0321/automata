/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas.bgp;

import Logica.automatas.Automata;

/**
 * Automata que validad la palabra config
 *
 * @author Efrain Hernandez Gonzalez
 * @see Automata
 */
public class Config extends Automata {

    public Config(String c) {
        super(c);
    }

    @Override
    protected void q0() {
        if (cont < car.length) {
            if (car[cont] != 'c' && car[cont] != 'C') {
                qError();
            } else if (car[cont] == 'c' || car[cont] == 'C') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'o' && car[cont] != 'O') {
                qError();
            } else if (car[cont] == 'o' || car[cont] == 'O') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 'n' && car[cont] != 'N') {
                qError();
            } else if (car[cont] == 'n' || car[cont] == 'N') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != 'f' && car[cont] != 'F') {
                qError();
            } else if (car[cont] == 'f' || car[cont] == 'F') {
                cont++;
                q4();
            }
        } else {
            qError();
        }
    }

    private void q4() {
        if (cont < car.length) {
            if (car[cont] != 'i' && car[cont] != 'I') {
                qError();
            } else if (car[cont] == 'i' || car[cont] == 'I') {
                cont++;
                q5();
            }
        } else {
            qError();
        }
    }

    private void q5() {
        if (cont < car.length) {
            if (car[cont] != 'g' && car[cont] != 'G') {
                qError();
            } else if (car[cont] == 'g' || car[cont] == 'G') {
                cont++;
                q6();
            }
        } else {
            qError();
        }
    }

    private void q6() {
        System.out.println(" En estado q6 - aceptación ");
        System.out.println(" Expresión valida ");
        aceptado = true;
    }
}
