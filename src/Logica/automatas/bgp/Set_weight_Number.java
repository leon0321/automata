package Logica.automatas.bgp;

import Logica.automatas.Automata;

/**
 * Automata que valida la sintaxis del comando set-weight
 *
 * @author Efrain Hernandez Gonzalez
 * @see Automata
 */
public class Set_weight_Number extends Automata {

    public Set_weight_Number(String c) {
        super(c);
    }

    @Override
    protected void q0() {
        if (cont < car.length) {
            if (car[cont] != 's' && car[cont] != 'S') {
                qError();
            } else if (car[cont] == 's' || car[cont] == 'S') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 't' && car[cont] != 'T') {
                qError();
            } else if (car[cont] == 't' || car[cont] == 'T') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != '-') {
                qError();
            } else if (car[cont] == '-') {
                cont++;
                q4();
            }
        } else {
            qError();
        }
    }

    private void q4() {
        if (cont < car.length) {
            if (car[cont] != 'w' && car[cont] != 'W') {
                qError();
            } else if (car[cont] == 'w' || car[cont] == 'W') {
                cont++;
                q5();
            }
        } else {
            qError();
        }
    }

    private void q5() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q6();
            }
        } else {
            qError();
        }
    }

    private void q6() {
        if (cont < car.length) {
            if (car[cont] != 'i' && car[cont] != 'I') {
                qError();
            } else if (car[cont] == 'i' || car[cont] == 'I') {
                cont++;
                q7();
            }
        } else {
            qError();
        }
    }

    private void q7() {
        if (cont < car.length) {
            if (car[cont] != 'g' && car[cont] != 'G') {
                qError();
            } else if (car[cont] == 'g' || car[cont] == 'G') {
                cont++;
                q8();
            }
        } else {
            qError();
        }
    }

    private void q8() {
        if (cont < car.length) {
            if (car[cont] != 'h' && car[cont] != 'H') {
                qError();
            } else if (car[cont] == 'h' || car[cont] == 'H') {
                cont++;
                q9();
            }
        } else {
            qError();
        }
    }

    private void q9() {
        if (cont < car.length) {
            if (car[cont] != 't' && car[cont] != 'T') {
                qError();
            } else if (car[cont] == 't' || car[cont] == 'T') {
                cont++;
                q10();
            }
        } else {
            qError();
        }
    }

    private void q10() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == ' ') {
                cont++;
                q10();
            } else if (car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                cont++;
                q11();
            }
        } else {
            qError();
        }
    }

    private void q11() {
        if (cont < car.length) {
            if (car[cont] != '0' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                cont++;
                q12();
            }
        } else {
            qError();
        }

    }

    private void q12() {
        if (cont < car.length) {
            if (car[cont] != '0' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                cont++;
                q13();
            }
        } else {
            qError();
        }
    }

    private void q13() {
        if (cont < car.length) {
            if (car[cont] != '0' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                q12();
            }
        } else {
            System.out.println(" Expresión valida ");
            aceptado = true;
        }
    }
}
