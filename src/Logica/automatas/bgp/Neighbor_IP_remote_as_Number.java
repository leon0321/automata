/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas.bgp;

import Logica.automatas.Automata;

/**
 * Validad el comando neighbor remote-as que permite definir un vecino BGP
 *
 * @author Efrain Hernandez Gonzalez
 * @see Automata
 */
public class Neighbor_IP_remote_as_Number extends Automata {

    public Neighbor_IP_remote_as_Number(String c) {
        super(c);
    }

    @Override
    protected void q0() {
        if (cont < car.length) {
            if (car[cont] != 'n' && car[cont] != 'N') {
                qError();
            } else if (car[cont] == 'n' || car[cont] == 'N') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
                System.out.println("aqui es 1");
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 'i' && car[cont] != 'I') {
                qError();
            } else if (car[cont] == 'i' || car[cont] != 'I') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != 'g' && car[cont] != 'G') {
                qError();
                System.out.println("aqui es 3");
            } else if (car[cont] == 'g' || car[cont] == 'G') {
                cont++;
                q4();
            }
        } else {
            qError();
        }
    }

    private void q4() {
        if (cont < car.length) {
            if (car[cont] != 'h' && car[cont] != 'H') {
                qError();
                System.out.println("aqui es 4");
            } else if (car[cont] == 'h' || car[cont] == 'H') {
                cont++;
                q5();
            }
        } else {
            qError();
        }
    }

    private void q5() {
        if (cont < car.length) {
            if (car[cont] != 'b' && car[cont] != 'B') {
                qError();
                System.out.println("aqui es 5");
            } else if (car[cont] == 'b' || car[cont] == 'B') {
                cont++;
                q6();
            }
        } else {
            qError();
        }
    }

    private void q6() {
        if (cont < car.length) {
            if (car[cont] != 'o' && car[cont] != 'O') {
                qError();
                System.out.println("aqui esjhjjjh 6");
            } else if (car[cont] == 'o' || car[cont] == 'O') {
                cont++;
                q7();
            }
        } else {
            qError();
        }
    }

    private void q7() {
        if (cont < car.length) {
            if (car[cont] != 'r' && car[cont] != 'R') {
                qError();
                System.out.println("aqui es 7");
            } else if (car[cont] == 'r' || car[cont] == 'R') {
                cont++;
                q8();
            }
        } else {
            qError();
        }
    }

    private void q8() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] == '0') {
                qError();
                System.out.println("aqui es 8");
            } else if (car[cont] == ' ') {
                cont++;
                q8();
            } else if (car[cont] == '1') {
                q9();
            } else if (car[cont] == '2') {
                cont++;
                q10();
            } else if ((car[cont] >= '3') && (car[cont] <= '9')) {
                cont++;
                q11();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q9() {
        System.out.println(" En estado q9 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q11();
            } else if (car[cont] == '.') {
                cont++;
                q18();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q10() {

        System.out.println(" En estado q10 ");
        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '4')) {
                cont++;
                q12();
            } else if (car[cont] == '5') {
                cont++;
                q13();
            } else if ((car[cont] >= '6') && (car[cont] <= '9')) {
                cont++;
                q17();
            } else if (car[cont] == '.') {
                cont++;
                q18();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q11() {
        System.out.println(" En estado q11 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q14();
            } else if (car[cont] == '.') {
                cont++;
                q11();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q12() {
        System.out.println(" En estado q12 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q15();
            } else if (car[cont] == '.') {
                cont++;
                q18();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q13() {
        System.out.println(" En estado q13 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '5')) {
                cont++;
                q16();
            } else if (car[cont] == '.') {
                cont++;
                q18();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q14() {
        System.out.println(" En estado q14 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q18();
            }
        } else {
            qError();
        }
    }

    public void q15() {
        System.out.println(" En estado q15 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q18();
            }
        } else {
            qError();
        }
    }

    public void q16() {
        System.out.println(" En estado q16 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q18();
            }
        } else {
            qError();
        }
    }

    public void q17() {
        System.out.println(" En estado q17 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q18();
            }
        } else {
            qError();
        }
    }

    public void q18() {
        System.out.println(" En estado q18 ");

        if (cont < car.length) {
            if (car[cont] == '0') {
                cont++;
                q48();
            } else if (car[cont] == '1') {
                cont++;
                q19();
            } else if (car[cont] == '2') {
                cont++;
                q20();
            } else if ((car[cont] >= '3') && (car[cont] <= '9')) {
                cont++;
                q21();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q19() {
        System.out.println(" En estado q19 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q21();
            } else if (car[cont] == '.') {
                cont++;
                q28();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q20() {
        System.out.println(" En estado q20 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '4')) {
                cont++;
                q22();
            } else if (car[cont] == '5') {
                cont++;
                q23();
            } else if ((car[cont] >= '6') && (car[cont] <= '9')) {
                cont++;
                q27();
            } else if (car[cont] == '.') {
                cont++;
                q28();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q21() {
        System.out.println(" En estado q21 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q24();
            } else if (car[cont] == '.') {
                cont++;
                q28();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q22() {
        System.out.println(" En estado q22 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q25();
            } else if (car[cont] == '.') {
                cont++;
                q28();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q23() {
        System.out.println(" En estado q23 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '5')) {
                cont++;
                q26();
            } else if (car[cont] == '.') {
                cont++;
                q28();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q24() {
        System.out.println(" En estado q24 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q28();
            }
        } else {
            qError();
        }

    }

    public void q25() {
        System.out.println(" En estado q25 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q28();
            }
        } else {
            qError();
        }
    }

    public void q26() {
        System.out.println(" En estado q26 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q28();
            }
        } else {
            qError();
        }
    }

    public void q27() {
        System.out.println(" En estado q27 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q28();
            }
        } else {
            qError();
        }
    }

    public void q28() {
        System.out.println(" En estado q28 ");

        if (cont < car.length) {
            if (car[cont] == '0') {
                cont++;
                q49();
            } else if (car[cont] == '1') {
                cont++;
                q29();
            } else if (car[cont] == '2') {
                cont++;
                q30();
            } else if ((car[cont] >= '3') && (car[cont] <= '9')) {
                cont++;
                q31();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q29() {
        System.out.println(" En estado q29 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q31();
            } else if (car[cont] == '.') {
                cont++;
                q38();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q30() {
        System.out.println(" En estado q30 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '4')) {
                cont++;
                q32();
            } else if (car[cont] == '5') {
                cont++;
                q33();
            } else if ((car[cont] >= '6') && (car[cont] <= '9')) {
                cont++;
                q37();
            } else if (car[cont] == '.') {
                cont++;
                q38();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q31() {
        System.out.println(" En estado q31 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q34();
            } else if (car[cont] == '.') {
                cont++;
                q38();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q32() {
        System.out.println(" En estado q32 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q35();
            } else if (car[cont] == '.') {
                cont++;
                q38();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q33() {
        System.out.println(" En estado q34 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '5')) {
                cont++;
                q36();
            } else if (car[cont] == '.') {
                cont++;
                q38();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q34() {
        System.out.println(" En estado q34 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q38();
            }
        } else {
            qError();
        }

    }

    public void q35() {
        System.out.println(" En estado q35 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q38();
            }
        } else {
            qError();
        }
    }

    public void q36() {
        System.out.println(" En estado q36 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q38();
            }
        } else {
            qError();
        }
    }

    public void q37() {
        System.out.println(" En estado q37 ");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q38();
            }
        } else {
            qError();
        }
    }

    public void q38() {

        System.out.println(" En estado q38 ");

        if (cont < car.length) {
            if (car[cont] == '0') {
                qError();
            } else if (car[cont] == '1') {
                cont++;
                q39();
            } else if (car[cont] == '2') {
                cont++;
                q40();
            } else if ((car[cont] >= '3') && (car[cont] <= '9')) {
                cont++;
                q41();
            } else {
                qError();
            }
        } else {
            qError();
        }

    }

    public void q39() {
        System.out.println(" En estado   n q39 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q41();
            } else if (car[cont] == ' ') {
                q50();
            } else {
                qError();
            }
        } else {
            qError();
        }
    }

    public void q40() {

        System.out.println(" En estado q40 ");
        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '4')) {
                cont++;
                q42();
            } else if (car[cont] == '5') {
                cont++;
                q43();
            } else if ((car[cont] >= '6') && (car[cont] <= '9')) {
                cont++;
                q47();
            } else {
                qError();
            }
        } else if (car[cont] == ' ') {
            q50();
        } else {
            qError();
        }

    }

    public void q41() {
        System.out.println(" En estado q41 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q44();
            } else {
                qError();
            }
        } else if (car[cont] == ' ') {
            q50();
        } else {
            qError();
        }

    }

    public void q42() {
        System.out.println(" En estado q42 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '9')) {
                cont++;
                q45();
            } else {
                qError();
            }
        } else if (car[cont] == ' ') {
            q50();
        } else {
            qError();
        }

    }

    public void q43() {
        System.out.println(" En estado q43 ");

        if (cont < car.length) {
            if ((car[cont] >= '0') && (car[cont] <= '5')) {
                cont++;
                q46();
            } else {
                qError();
            }
        } else if (car[cont] == ' ') {
            q50();
        } else {
            qError();
        }
    }

    public void q44() {
        System.out.println(" En estado q44 ");

        if (car[cont] == ' ') {
            q50();
        } else {
            qError();
        }
    }

    public void q45() {
        System.out.println(" En estado q45 ");

        if (car[cont] == ' ') {
            q50();
        } else {
            qError();
        }
    }

    public void q46() {
        System.out.println(" En estado q46 ");

        if (car[cont] == ' ') {
            q50();
        } else {
            qError();
        }
    }

    public void q47() {
        System.out.println(" En estado q47 ");

        if (car[cont] == ' ') {
            q50();
        } else {
            qError();
        }
    }

    public void q48() {
        System.out.println(" En estado q48");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q28();
            }
        } else {
            qError();
        }

    }

    public void q49() {
        System.out.println(" En estado q49");

        if (cont < car.length) {
            if (car[cont] == '.') {
                cont++;
                q38();
            }
        } else {
            qError();
        }

    }

    public void q50() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] != 'r' && car[cont] != 'R') {
                qError();
                System.out.println("aqui es 50");
            } else if (car[cont] == ' ') {
                cont++;
                q50();
            } else if (car[cont] == 'r' || car[cont] == 'R') {
                cont++;
                q51();
            }
        } else {
            qError();
        }
    }

    private void q51() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q52();
            }
        } else {
            qError();
        }
    }

    private void q52() {
        if (cont < car.length) {
            if (car[cont] != 'm' && car[cont] != 'M') {
                qError();
            } else if (car[cont] == 'm' || car[cont] == 'M') {
                cont++;
                q53();
            }
        } else {
            qError();
        }
    }

    private void q53() {
        if (cont < car.length) {
            if (car[cont] != 'o' && car[cont] != 'O') {
                qError();
            } else if (car[cont] == 'o' || car[cont] == 'O') {
                cont++;
                q54();
            }
        } else {
            qError();
        }
    }

    private void q54() {
        if (cont < car.length) {
            if (car[cont] != 't' && car[cont] != 'T') {
                qError();
            } else if (car[cont] == 't' || car[cont] == 'T') {
                cont++;
                q55();
            }
        } else {
            qError();
        }
    }

    private void q55() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
                System.out.println("aqui es 55");
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q56();
            }
        } else {
            qError();
        }
    }

    private void q56() {
        if (cont < car.length) {
            if (car[cont] != '-') {
                qError();
                System.out.println("aqui es 56");
            } else if (car[cont] == '-') {
                cont++;
                q57();
            }
        } else {
            qError();
        }
    }

    private void q57() {
        if (cont < car.length) {
            if (car[cont] != 'a' && car[cont] != 'A') {
                qError();
                System.out.println("aqui es 57");
            } else if (car[cont] == 'a' || car[cont] == 'A') {
                cont++;
                q58();
            }
        } else {
            qError();
        }
    }

    private void q58() {
        if (cont < car.length) {
            if (car[cont] != 's' && car[cont] != 'S') {
                qError();
                System.out.println("aqui es 58");
            } else if (car[cont] == 's' || car[cont] == 'S') {
                cont++;
                q59();
            }
        } else {
            qError();
        }
    }

    private void q59() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == ' ') {
                cont++;
                q59();
            } else if (car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                cont++;
                q60();
            }
        } else {
            qError();
        }
    }

    private void q60() {
        if (cont < car.length) {
            if (car[cont] != '0' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                cont++;
                q61();
            }
        } else {
            qError();
        }

    }

    private void q61() {
        if (cont < car.length) {
            if (car[cont] != '0' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                cont++;
                q62();
            }
        } else {
            qError();
        }
    }

    private void q62() {
        if (cont < car.length) {
            if (car[cont] != '0' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                q61();
                System.out.println(" po aqui ");
            }
        } else {
            System.out.println(" Expresión valida ");
            aceptado = true;
        }
    }
}
