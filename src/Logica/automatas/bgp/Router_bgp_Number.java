/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas.bgp;

import Logica.automatas.Automata;

/**
 * Automata que valida sintasis de la instrucion router bgp la cual sirve para
 * definir el numero del AS de u router BGP
 *
 * @author Efrain Hernandez Gonzalez
 * @see Automata
 */
public class Router_bgp_Number extends Automata {

    public Router_bgp_Number(String c) {
        super(c);
    }

    @Override
    protected void q0() {
        if (cont < car.length) {
            if (car[cont] != 'r' && car[cont] != 'R') {
                qError();
                System.out.println("aqui es 0");
            } else if (car[cont] == 'r' || car[cont] == 'R') {
                cont++;
                q1();
            }
        } else {
            qError();
        }
    }

    private void q1() {
        if (cont < car.length) {
            if (car[cont] != 'o' && car[cont] != 'O') {
                qError();
                System.out.println("aqui es 1");
            } else if (car[cont] == 'o' || car[cont] == 'O') {
                cont++;
                q2();
            }
        } else {
            qError();
        }
    }

    private void q2() {
        if (cont < car.length) {
            if (car[cont] != 'u' && car[cont] != 'U') {
                qError();
                System.out.println("aqui es 2");
            } else if (car[cont] == 'u' || car[cont] == 'U') {
                cont++;
                q3();
            }
        } else {
            qError();
        }
    }

    private void q3() {
        if (cont < car.length) {
            if (car[cont] != 't' && car[cont] != 'T') {
                qError();
                System.out.println("aqui es 3");
            } else if (car[cont] == 't' || car[cont] == 'T') {
                cont++;
                q4();
            }
        } else {
            qError();
        }
    }

    private void q4() {
        if (cont < car.length) {
            if (car[cont] != 'e' && car[cont] != 'E') {
                qError();
                System.out.println("aqui es 4");
            } else if (car[cont] == 'e' || car[cont] == 'E') {
                cont++;
                q5();
            }
        } else {
            qError();
        }
    }

    private void q5() {
        if (cont < car.length) {
            if (car[cont] != 'r' && car[cont] != 'R') {
                qError();
                System.out.println("aqui es 5");
            } else if (car[cont] == 'r' || car[cont] == 'R') {
                cont++;
                q6();
            }
        } else {
            qError();
        }
    }

    private void q6() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] != 'b' && car[cont] != 'B') {
                qError();
                System.out.println("aqui es 6");
            } else if (car[cont] == ' ') {
                cont++;
                q6();
            } else if (car[cont] == 'b' || car[cont] == 'B') {
                cont++;
                q7();
            }
        } else {
            qError();
        }
    }

    private void q7() {
        if (cont < car.length) {
            if (car[cont] != 'g' && car[cont] != 'G') {
                qError();
                System.out.println("aqui es 7");
            } else if (car[cont] == 'g' || car[cont] == 'G') {
                cont++;
                q8();
            }
        } else {
            qError();
        }
    }

    private void q8() {
        if (cont < car.length) {
            if (car[cont] != 'p' && car[cont] != 'P') {
                qError();
                System.out.println("aqui es 8");
            } else if (car[cont] == 'p' || car[cont] == 'P') {
                cont++;
                q9();
            }
        } else {
            qError();
        }
    }

    private void q9() {
        if (cont < car.length) {
            if (car[cont] != ' ' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == ' ') {
                cont++;
                q9();
            } else if (car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                cont++;
                q10();
            }
        } else {
            qError();
        }
    }

    private void q10() {
        if (cont < car.length) {
            if (car[cont] != '0' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                cont++;
                q11();
            }
        } else {
            qError();
        }

    }

    private void q11() {
        if (cont < car.length) {
            if (car[cont] != '0' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                cont++;
                q12();
            }
        } else {
            qError();
        }
    }

    private void q12() {
        if (cont < car.length) {
            if (car[cont] != '0' && car[cont] != '1' && car[cont] != '2' && car[cont] != '3' && car[cont] != '4' && car[cont] != '5' && car[cont] != '6' && car[cont] != '7' && car[cont] != '8' && car[cont] != '9') {
                qError();
            } else if (car[cont] == '0' || car[cont] == '1' || car[cont] == '2' || car[cont] == '3' || car[cont] == '4' || car[cont] == '5' || car[cont] == '6' || car[cont] == '7' || car[cont] == '8' || car[cont] == '9') {
                q11();
                System.out.println(" por aqui ");
            }
        } else {
            System.out.println(" Expresión valida ");
            aceptado = true;
        }
    }
}