/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas;

/**
 * Clase padre de los autómatas del paquete bgp y rip1 en analogía con los
 * autómatas de los libros en esta clase los estados son métodos y las aristas
 * son condiciones que permiten pasar de un estado a otro. Esta clase hereda su
 * constructor, un método inicio, el estado de error y el estado q0 que es un
 * método abstracto
 *
 * @author Leonardo Ortega Hernandez
 */
public abstract class Automata {

    protected int cont;
    protected char[] car;
    protected String c;
    protected boolean aceptado = false;

    /**
     * Constructor de la clase que revive la palabra a validar por el autómata
     *
     * @param c es la cadena de debe ser validada por el autómata
     */
    public Automata(String c) {
        this.c = c;
        car = c.toCharArray();
    }

    /**
     * Metodo que inicia la validacion la palabra contenidad en el automata
     *
     * @return Retorna false si la palabra no es validad y true si es aceptada
     */
    public boolean inicio() {
        cont = 0;
        q0();
        return aceptado;
    }
/**
 * Representa el estado inicial del automata y tantos metodos como estados
 */
    protected abstract void q0();
/**
 * Representa el estado de Error cuando una palabra no es aceptada este metodo es llamado
 */
    protected void qError() {
        aceptado = false;
    }
}
