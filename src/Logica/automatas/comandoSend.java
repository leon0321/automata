/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas;

/**
 *
 * @author Jorge
 */
public class comandoSend {
        
    private int cont;
    private char[] car;
    private char[] aux;
    private String c;
    private boolean aceptado = false;
    private String ipInicial;
    private String ipFinal;
    private String cantidadPaquetes;
    private String salida;
    
    public comandoSend(String c){
        this.c = c;
        car = c.toCharArray();
        ipInicial = "";
        ipFinal = "";
        cantidadPaquetes = "";
        salida = "";
    }
    
    public boolean inicio(){
        cont = 0;
        q0();
        return aceptado;
    }
    
    public void q0(){
        //System.out.println(" En q0 ");
        
        if(cont < car.length){
            if(car[cont] == 'S' || car[cont]=='s'){
                cont++;
                q1();
            }else
                qError(cont);
        }
    }
    
    public void q1(){
        //System.out.println(" En q1 ");
        
        if(cont < car.length){
            if(car[cont] == 'E' || car[cont]=='e'){
                cont++;
                q2();
            }else
                qError(cont);
        }
    }
    
    public void q2(){
        //System.out.println(" En q2 ");
        
        if(cont < car.length){
            if(car[cont] == 'N' || car[cont]=='n'){
                cont++;
                q3();
            }else
                qError(cont);
        }
    }
    
    public void q3(){
        //System.out.println(" En q3 ");
        
        if(cont < car.length){
            if(car[cont] == 'D' || car[cont]=='d'){
                cont++;
                q4();
            }else
                qError(cont);
        }
    }
    
    public void q4(){
        //System.out.println(" En q4 ");
        
        if(cont < car.length){
            if(car[cont] == ' '){
                cont++;
                q5();
            }else
                qError(cont);
        }
    }
    
    public void q5(){
        //System.out.println(" En q5 ");
        
        if(cont < car.length){
            if(car[cont] == 'F' || car[cont]=='f'){
                cont++;
                q6();
            }else
                qError(cont);
        }
    }
    
    public void q6(){
        //System.out.println(" En q6 ");
        
        if(cont < car.length){
            if(car[cont] == 'R' || car[cont]=='r'){
                cont++;
                q7();
            }else
                qError(cont);
        }
    }
    
    public void q7(){
        //System.out.println(" En q7 ");
        
        if(cont < car.length){
            if(car[cont] == 'O' || car[cont]=='o'){
                cont++;
                q8();
            }else
                qError(cont);
        }
    }
    
    public void q8(){
        //System.out.println(" En q8 ");
        
        if(cont < car.length){
            if(car[cont] == 'M' || car[cont]=='m'){
                cont++;
                q9();
            }else
                qError(cont);
        }
    }
    
    public void q9(){
        //System.out.println(" En q9 ");
        
        if(cont < car.length){
            if(car[cont] == ' '){
                cont++;
                q10();
            }else
                qError(cont);
        }
    }
    
    public void q10(){
        String s = "";
        boolean siga = false;
        //System.out.println(" En q10 ");
        
        try{
            if(cont < car.length){

                //formo la cadena con la ip
                while (car[cont] != ' ') {
                    s += car[cont];
                    cont++;
                }

                System.out.println(s);

                //valido la ip con su autómata
                AutomataIP ai = new AutomataIP(s);
                siga = ai.inicio();

                //si la ip es válida cuando se encuentra el espacio sigo
                if(siga){
                    ipInicial = s;
                    if(car[cont] == ' '){
                        cont++;
                        q11();
                    }
                }else
                    qError(-2);

            }
        }catch(ArrayIndexOutOfBoundsException e){
            qError(-3);
        }
    }
    
    public void q11(){
        //System.out.println(" En q11 ");
        if(cont < car.length){
            if(car[cont] == 'T' || car[cont]=='t'){
                cont++;
                q12();
            }else
                qError(cont);
        }
    }
    
    public void q12(){
        //System.out.println(" En q12 ");
        if(cont < car.length){
            if(car[cont] == 'O' || car[cont]=='t'){
                cont++;
                q13();
            }else
                qError(cont);
        }
        
    }
    
    public void q13(){
        //System.out.println(" En q13 ");
        if(cont < car.length){
            if(car[cont] == ' '){
                cont++;
                q14();
            }else
                qError(cont);
        }
        
    }
    
    public void q14(){
        String s = "";
        boolean siga = false;
        //System.out.println(" En q14 ");
        try{
            if(cont < car.length){

                //formo la cadena con la ip
                while (car[cont] != ' ') {
                    s += car[cont];
                    cont++;
                }

                System.out.println(s);

                //valido la ip con su autómata
                AutomataIP ai = new AutomataIP(s);
                siga = ai.inicio();

                //si la ip es válida cuando se encuentra el espacio sigo
                if(siga){
                    ipFinal = s;
                    if(car[cont] == ' '){
                        cont++;
                        q15();
                    }else
                        qError(-2);
                }
            }
            
        }catch(ArrayIndexOutOfBoundsException e){
            qError(-3);
        }
    }
    
    //Aqui se debe validar el número
    public void q15(){
        String n = "";
        System.out.println("entro");
        System.out.println("contador: " +cont);
        
        while(cont < car.length){
            n += car[cont];
            cont++;
            System.out.println("Formando n: "+n);
            System.out.println("Contador vale: "+cont);
            if(cont == car.length){
                break;
            }
        }
        System.out.println("N quedó: "+n);
        
        aux = n.toCharArray();

        
        if(isNumeric(n)){
            cantidadPaquetes = n;
            System.out.println(" Comando válido");
            aceptado = true;
        }
        else
            qError(-1);
    }
    
    
    public void qError(int indice){
        System.out.println(" En error, expresión no válida ");
        
        if((indice > 0) && (indice < car.length-1)){
            
            salida += c.substring(0, indice);
            salida += "<u>" + String.valueOf(c.charAt(indice)) + "</u>";
            salida += c.substring(indice+1) + "\n";
            
        }
        //Si el indice es 0
        else if(indice == 0){
            salida += "<u>" + String.valueOf(c.charAt(indice));
            salida += "</u>" + c.substring(indice+1);
            
            
        }
        // Si el indice está al final
        else if (indice == car.length-1){
            salida += c.substring(0, indice);
            salida += "<u>" + String.valueOf(c.charAt(indice)) + "</u>";
            
        }
        //Si el índice es -1 la cantidad de paquetes no es numérica
        else if (indice == -1){
            salida += "La cantidad de paquetes debe ser un valor numérico";
        }
        //Si el indice es -4 el número de paquetes empieza en cero
        else if (indice == -4){
            salida += "La cantidad de paquetes no debe empezar por 0";
        }
        //Si el indice es -2 el error es de ip inválida
        else if (indice == -2){
            salida += "Comprobar direcciones IP";
        }
        
        //Si el indice es -6 no hay nùmero de paquetes
        else if(indice == -6){
            salida += "Por favor ingrese un número de paquetes";
        }
        
        aceptado = false;
    }
    
    //Método usado para verificar si una cadena es un número o no lo es
    private static boolean isNumeric(String cadena){
	try {
		Integer.parseInt(cadena);
		return true;
	} catch (NumberFormatException nfe){
		return false;
	}
    }

    /**
     * @return the ipInicial
     */
    public String getIpInicial() {
        return ipInicial;
    }

    /**
     * @return the ipFinal
     */
    public String getIpFinal() {
        return ipFinal;
    }

    /**
     * @return the cantidadPaquetes
     */
    public String getCantidadPaquetes() {
        return cantidadPaquetes;
    }

    /**
     * @return the salida
     */
    public String getSalida() {
        return salida;
    }

    /**
     * @param salida the salida to set
     */
    public void setSalida(String salida) {
        this.salida = salida;
    }
}
