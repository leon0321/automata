/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.automatas;

/**
 *
 * @author Jorge
 */

//Se retorna al final la salida del estado de error -si lo hubiere- y el nombre
//del router a crear -en caso de que el comando sea aceptado-.
public class CreateRouter {
    
    private int cont;
    private char[] car;
    private String c;
    private boolean aceptado = false;
    private String salida;
    private String nombre;
    
    public CreateRouter(String c){
        this.c = c;
        car = c.toCharArray();
        salida = "";
        nombre = "";
    }
    
   public boolean inicio(){
        setCont(0);
       q0();
       return isAceptado();
   }
   
   public void q0(){
       System.out.println(" En q0 ");
        
        if(getCont() < getCar().length){
            if(getCar()[getCont()] == 'C' || getCar()[getCont()]=='c'){
                setCont(getCont() + 1);
                q1();
            }else
                qError(getCont());
        }
   }
   
   public void q1(){
       System.out.println(" En q1 ");
        
        if(getCont() < getCar().length){
            if(getCar()[getCont()] == 'R' || getCar()[getCont()]=='r'){
                setCont(getCont() + 1);
                q2();
            }else
                qError(getCont());
        }
   }
   
   public void q2(){
       System.out.println(" En q2 ");
        
        if(getCont() < getCar().length){
            if(getCar()[getCont()] == 'E' || getCar()[getCont()]=='e'){
                setCont(getCont() + 1);
                q3();
            }else
                qError(getCont());
        }
   }
   
   public void q3(){
       System.out.println(" En q3 ");
        
        if(getCont() < getCar().length){
            if(getCar()[getCont()] == 'A'|| getCar()[getCont()]=='a'){
                setCont(getCont() + 1);
                q4();
            }else
                qError(getCont());
        }
   }
   
   public void q4(){
       System.out.println(" En q4 ");
        
        if(getCont() < getCar().length){
            if(getCar()[getCont()] == 'T' || getCar()[getCont()]=='t'){
                setCont(getCont() + 1);
                q5();
            }else
                qError(getCont());
        }
   }
   
   public void q5(){
       System.out.println(" En q5 ");
        
        if(getCont() < getCar().length){
            if(getCar()[getCont()] == 'E' || getCar()[getCont()]=='e'){
                setCont(getCont() + 1);
                q6();
            }else
                qError(getCont());
        }
   }
   
   public void q6(){
       System.out.println(" En q6 ");
        
        if(getCont() < getCar().length){
            if(getCar()[getCont()] == ' '){
                setCont(getCont() + 1);
                q7();
            }else
                qError(getCont());
        }
   }
   
   public void q7(){
       System.out.println(" En q7 ");
        
        if(getCont() < getCar().length){
            if(getCar()[getCont()] == 'R' || getCar()[getCont()]=='r'){
                setCont(getCont() + 1);
                q8();
            }else
                qError(getCont());
        }
   }
   
   public void q8(){
       System.out.println(" En q8 ");
        
        if(getCont() < getCar().length){
            if(getCar()[getCont()] == 'O' || getCar()[getCont()]=='o'){
                setCont(getCont() + 1);
                q9();
            }else
                qError(getCont());
        }
   }
   
   public void q9(){
       System.out.println(" En q9 ");
        
        if(getCont() < getCar().length){
            if(getCar()[getCont()] == 'U' || getCar()[getCont()]=='u'){
                setCont(getCont() + 1);
                q10();
            }else
                qError(getCont());
        }
   }
   
   public void q10(){
       System.out.println(" En q10 ");
        
        if(getCont() < getCar().length){
            if(getCar()[getCont()] == 'T' || getCar()[getCont()]=='t'){
                setCont(getCont() + 1);
                q11();
            }else
                qError(getCont());
        }
   }
   
   public void q11(){
       System.out.println(" En q11 ");
        
        if(getCont() < getCar().length){
            if(getCar()[getCont()] == 'E' || getCar()[getCont()]=='e'){
                setCont(getCont() + 1);
                q12();
            }else
                qError(getCont());
        }
   }
   
   public void q12(){
       System.out.println(" En q12 ");
        
        if(getCont() < getCar().length){
            if(getCar()[getCont()] == 'R' || getCar()[getCont()]=='r'){
                setCont(getCont() + 1);
                q13();
            }else
                qError(getCont());
        }
   }
   
   public void q13(){
       System.out.println(" En q13 ");
        
        if(getCont() < getCar().length){
            if(getCar()[getCont()] == ' '){
                setCont(getCont() + 1);
                q14();
            }else
                qError(getCont());
        }
   }
   
   //Aquí se forma el nombre, cualquier nombre es válido, excepto si es una
   //palabra reservada del sistema
   public void q14(){
       System.out.println(" En q14 ");
        
       //Aquí se forma el nombre
        if(cont < car.length){
            nombre += car[cont];  
            cont++;
            q14();
        }
        //Se procede a verificar el nombre
        else if(cont == car.length){
//            
//            validarNombre vn = new validarNombre();
//            
//            if(!vn.isReservada(nombre))
//                
//                   setAceptado(true);
//            else
//                
//                qError(-1);
            aceptado = true;
        }
   }
    
   public void qError(int indice){
       
       System.out.println(" En estado de error ");
       
       if((indice > 0) && (indice < getCar().length-1)){
            
            setSalida(getSalida() + getC().substring(0, indice));
            setSalida(getSalida() + "<u>" + String.valueOf(getC().charAt(indice)) + "</u>");
            setSalida(getSalida() + getC().substring(indice+1) + "\n");
            
        }
        //Si el indice es 0
        else if(indice == 0){
            setSalida(getSalida() + "<u>" + String.valueOf(getC().charAt(indice)));
            setSalida(getSalida() + "</u>" + getC().substring(indice+1));
            
            
        }
        // Si el indice está al final
        else if (indice == getCar().length-1){
            setSalida(getSalida() + getC().substring(0, indice));
            setSalida(getSalida() + "<u>" + String.valueOf(getC().charAt(indice)) + "</u>");
            
        }
       
        // Si el índice es -1, el nombre es igual a una palabra reservada
        else if(indice == -1){
            salida += "El nombre del dispositivo no debe ser una palabra"
                    + " reservada del sistema";
        }
       
        setAceptado(false);
   }

    /**
     * @return the cont
     */
    public int getCont() {
        return cont;
    }

    /**
     * @param cont the cont to set
     */
    public void setCont(int cont) {
        this.cont = cont;
    }

    /**
     * @return the car
     */
    public char[] getCar() {
        return car;
    }

    /**
     * @param car the car to set
     */
    public void setCar(char[] car) {
        this.car = car;
    }

    /**
     * @return the c
     */
    public String getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(String c) {
        this.c = c;
    }

    /**
     * @return the aceptado
     */
    public boolean isAceptado() {
        return aceptado;
    }

    /**
     * @param aceptado the aceptado to set
     */
    public void setAceptado(boolean aceptado) {
        this.aceptado = aceptado;
    }

    /**
     * @return the salida
     */
    public String getSalida() {
        return salida;
    }

    /**
     * @param salida the salida to set
     */
    public void setSalida(String salida) {
        this.salida = salida;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
