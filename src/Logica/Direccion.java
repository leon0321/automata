/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import java.io.Serializable;

/**
 *
 * @author Jorge
 */
public class Direccion implements Serializable {

    private String ip;
    private String mascara;

    public Direccion(String ip, String mascara) {
        this.ip = ip;
        this.mascara = mascara;
    }

    public Direccion() {
        this.ip = "";
        this.mascara = "";
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMascara() {
        return mascara;
    }

    public void setMascara(String mascara) {
        this.mascara = mascara;
    }
}
