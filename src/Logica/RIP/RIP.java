/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.RIP;

import Logica.*;
import Controlador.Controlador;
import Logica.ProtocoloBGP.Rute;
import Logica.automatas.RIP.Automata_network;
import Logica.automatas.RIP.clear_ip;
import Logica.automatas.RIP.show_ip_route;
import Logica.tools.Enlace;
import java.util.ArrayList;

/**
 *
 * @author Nathan
 */
public class RIP {

    private ArrayList<Dispositivo> routers; // Lista de todos los routers de la red
    private ArrayList<TablaRIP> tablas; // Tablas de enrutamiento del protocolo RIP
    private Router enrutador;//router en configuracion
    private int pos = 0;//posicion del router en el ArrayList

    public RIP(Router r) {
        routers = new ArrayList<>();
        tablas = new ArrayList<>();
        this.enrutador = r;
    }

    //Metodo verificador del comendo a ejecutar
    public String EjecutarComando(String comando) {
        String resultado = "";//mensaje en consola
        String[] palabras = comando.split(" ");//se divide el comando por palabras
        addConfiguracion();//se busca o añade el router
        switch (palabras[0]) {
            // clear ip route
            case "clear":

                // verifica si el comando tiene 4 palabras
                if(palabras.length!=4){
                    resultado = "<p>El comando esta incompleto</p>";
                    break;
                }
                // verifica que el comando este bien escrito con el automata clear_ip
                if (new clear_ip(palabras[0] + palabras[1] + palabras[2]+palabras[3]).inicio()) {
                    if (palabras.length == 4) {
                        resultado = "<p>La dirección " + palabras[3] + " no se encuentra en la tabla.</p>";
                        
                        for (int i = 0; i < tablas.get(pos).getDirecciones().size(); i++) {
                            //si se encuentra la ip en la tabla se elimina
                            if (enrutador.getTabla().getDirecciones().get(i).getIp().equalsIgnoreCase(palabras[3])) {
                                tablas.get(pos).getDirecciones().remove(i);
                                tablas.get(pos).getDestinos().remove(i);
                                tablas.get(pos).getIntermedios().remove(i);
                                enrutador.setTablaRIP(tablas.get(pos));
                                resultado = "<p>La dirección " + palabras[3] + " ha sido eliminada.</p>";
                                break;
                            }
                        }
                    }
                } else {
                    resultado = "<p>El comando no es valido</p>";
                }

                break;

            //comando network
            case "network":
                // verifica que el comando este bien escrito con el automata Automata_network
                if (new Automata_network(comando).inicio()) {
                    Direccion dir = new Direccion();
                    dir.setIp(palabras[1]);
                    //añade la direccion a la tabla del router
                    if(tablas.get(pos).addDireccion(dir, enrutador)){
                        enrutador.setTablaRIP(tablas.get(pos));
                        resultado = "<p>Se ha agregado la dirección " + tablas.get(pos).getDirecciones().get(tablas.get(pos).getDirecciones().size() - 1).getIp() + " a la Tabla RIP.</p><p>" + tablas.get(pos).getDirecciones().get(tablas.get(pos).getDirecciones().size() - 1).getMascara() + "</p>";
                    }else{
                        resultado = "Se debe configurar la ip de red";
                    }
                    break;
                } else {
                    resultado = "<p>La ip: " + palabras[1] + " no es valida</p>";
                    break;
                }

            // Comando show ip route
            case "show":

                // verifica que el comando este bien escrito con el automata Show_ip_route
                if (new show_ip_route(palabras[0] + palabras[1] + palabras[2]).inicio()) {
                    // Actualiza las tablas antes de sacar la lista
                    actualizar();

                    // Si hay al menos una dirección en la tabla
                    if (tablas.get(pos).getDestinos().size() > 0) {
                        resultado = "<table>"
                                + "<tr>"
                                + "<td>#</td><td>IP</td><td>Marscara</td><td>Propiedad</td><td>saltos</td>"
                                + "</tr>";
                        for (int i = 0; i < tablas.get(pos).getDestinos().size(); i++) {
                            resultado += "<tr>"
                                    + "<td>" + (i + 1) + "</td>"
                                    + "<td>" + tablas.get(pos).getDestinos().get(i).getIp() + "</td>"
                                    + "<td>" + tablas.get(pos).getDestinos().get(i).getMascara() + "</td>"
                                    + "<td>" + (tablas.get(pos).getIntermedios().get(i) == null ? "Propietario" : "No propietario") + "</td>"
                                    + "<td>" + tablas.get(pos).getSaltos().get(i) + "</td>"
                                    + "</tr>";
                        }
                        resultado += "</table>";
                    } else {
                        resultado = "<p>vacío</p>";
                    }
                }

                break;

            default:
                resultado = "<p>Error comando</p>";

        }
        return resultado;
    }

    //busca el router en la lista de routers y si no lo encuentra lo adiciona
    private void addConfiguracion() {
        if (routers.isEmpty()) {
            routers.add(enrutador);
            tablas.add(enrutador.getTabla());
        } else {
            boolean b = false;
            for (int i = 0; i < routers.size(); i++) {
                if (routers.get(i).getNombre().equals(enrutador.getNombre())) {
                    b = true;
                    pos = i;
                }
            }
            if (!b) {
                routers.add(enrutador);
                tablas.add(new TablaRIP());
                pos = routers.size() - 1;
            }
        }
    }

    //actualiza la tabla del router con respecto a sus vecinos
    public void actualizar() {
        Router nuevo;
        for (int i = 0; i < Controlador.allDispositivos.size(); i++) {
            if (Controlador.allDispositivos.get(i).getClass() == enrutador.getClass()) {
                if (!Controlador.allDispositivos.get(i).equals(enrutador)) {
                    nuevo = (Router) Controlador.allDispositivos.get(i);
                    for (int j = 0; j < nuevo.getVecinos().size(); j++) {
                        if (nuevo.getVecinos().get(j).equals(enrutador)) {
                            for (int x = 0; x < nuevo.getTabla().getDirecciones().size(); x++) {
                                enrutador.getTabla().addDireccion(nuevo.getTabla().getDirecciones().get(x), nuevo.getTabla().getSaltos().get(x), nuevo);
                            }
                        }
                    }
                }
            }
        }
    }

    /* metodo que escoge la ruta, recive las rutas posibles 'paths' y el dispositivo inicial 'inicial'
     * retorna la mejor ruta posible
     */
    public static ArrayList<DispositivoGrafico> enviar(ArrayList paths, Dispositivo inicial) {
        Rute ruta = new Rute();//almacena la ruta que se va a evaluar
        Router temporal = new Router();//almacena el router en la posicion actual
        ArrayList<Integer> pos = new ArrayList<>();//almacena las posiciones de las rutas que
                                                   //no estan configuradas con el protocolo
        int a = 0, b = 0;//verifican que el router este configurado tanto con el dispositivo
                        //anterior como con el siguiente
        for (int i = 0; i < paths.size(); i++) {
            ruta = (Rute) paths.get(i);
            for (int j = 0; j < ruta.size(); j++) {
                //comienza cada vez que se encuentra un router
                if (ruta.get(j).getClass() == Router.class) {
                    a = a + 2;//+2 indicando entrada y salida
                    String[] palabras = {""};
                    temporal = (Router) ruta.get(j);//dispositivo actual (router)
                    Dispositivo next = ruta.get(j + 1);//dispositivo siguiente
                    Dispositivo before = ruta.get(j - 1);//dispositivo anterior
                    for (int x = 0; x < Controlador.allDispositivos.size(); x++) {
                        //se busca el router en la lista y se copia con todos sus datos
                        if (Controlador.allDispositivos.get(x).getNombre().equals(temporal.getNombre())) {
                            temporal = (Router) Controlador.allDispositivos.get(x);
                        }
                    }
                    for (int x = 0; x < Controlador.lineas.size(); x++) {
                        //si el dispositivo actual y el siguiente estan conectados verifica las ip
                        if (Controlador.lineas.get(x).getInicial().equals(temporal) && Controlador.lineas.get(x).getFinall().equals(next)) {
                            palabras = Controlador.lineas.get(x).getpInicial().getDireccion().getIp().split("\\.");
                            String ip = "";
                            switch(returnMsk(palabras[0])){
                                case 1:
                                    ip = palabras[0] + ".0.0.0";
                                    break;
                                case 2:
                                    ip = palabras[0] +"."+ palabras[1]+".0.0";
                                    break;
                                case 3:
                                    ip = palabras[0] +"."+ palabras[1]+"."+palabras[2]+".0";
                                    break;
                            }
                            for (int y = 0; y < temporal.getTabla().getDirecciones().size(); y++) {
                                if (temporal.getTabla().getDirecciones().get(y).getIp().equals(ip)) {
                                    b++;
                                }
                            }
                        }

                    }
                    //si es el primer router de la ruta
                    if (a == 2) {
                        palabras = inicial.toString().split("\\.");
                        String compare = palabras[0] + "." + palabras[1] + "." + palabras[2];
                        switch(returnMsk(palabras[0])){
                            case 1:
                                compare = palabras[0];
                                break;
                            case 2:
                                compare = palabras[0] +"."+ palabras[1];
                                break;
                            case 3:
                                compare = palabras[0] +"."+ palabras[1]+"."+palabras[2];
                                break;
                        }
                        for (int y = 0; y < temporal.getTabla().getDirecciones().size(); y++) {
                            //compara con la ip del dispositivo inicial
                            if (temporal.getTabla().getDirecciones().get(y).getIp().contains(compare)) {
                                b++;
                            }
                        }
                    } else {//sino conpara con el dispositivo anterior 'before'
                        for (int x = 0; x < Controlador.lineas.size(); x++) {
                            if (Controlador.lineas.get(x).getFinall().equals(temporal) && Controlador.lineas.get(x).getInicial().equals(before)) {
                                palabras = Controlador.lineas.get(x).getpFinal().getDireccion().getIp().split("\\.");
                                String compare = palabras[0] + "." + palabras[1] + "." + palabras[2];
                                switch(returnMsk(palabras[0])){
                                    case 1:
                                        compare = palabras[0];
                                        break;
                                    case 2:
                                        compare = palabras[0] +"."+ palabras[1];
                                        break;
                                    case 3:
                                        compare = palabras[0] +"."+ palabras[1]+"."+palabras[2];
                                        break;
                                }
                                for (int y = 0; y < temporal.getTabla().getDirecciones().size(); y++) {
                                    if (temporal.getTabla().getDirecciones().get(y).getIp().contains(compare)) {
                                        b++;
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //se añaden las pociciones donde los dispositivos no estan correctamente configurados
            if (a != b) {
                pos.add(i);
            }
            a = 0;
            b = 0;
        }
        //se eliminan las rutas sin configurar
        for (int i = pos.size() - 1; i >= 0; i--) {
            int j = pos.get(i);
            paths.remove(j);
        }
        //se busca la ruta mas corta entre las que quedan
        for (int i = 0; i < paths.size(); i++) {
            ruta = (Rute) paths.get(i);
            if (i == 0) {
                a = ruta.size();
                b = i;
            } else {
                if (a > ruta.size()) {
                    a = ruta.size();
                    b = i;
                }
            }
        }
        ruta = (Rute) paths.get(b);
        return Enlace.caminoGrafico(ruta);
    }
    //returna la cantidad de octetos de red
    public static int returnMsk(String octeto){
        int msk = Integer.parseInt(octeto);
        if(msk<128){
            return 1;
        }else if(msk>=128 && msk<192){
            return 2;
        }else{
            return 3;
        }
    }
}
