/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.RIP;

import Controlador.Controlador;
import Logica.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Nathan
 */
public class TablaRIP implements Serializable {

    private ArrayList<Direccion> direcciones;
    private ArrayList<Direccion> destinos;
    private ArrayList<Dispositivo> intermedios;
    private ArrayList<Integer> saltos;

    public TablaRIP() {
        direcciones = new ArrayList<>();
        destinos = new ArrayList<>();
        intermedios = new ArrayList<>();
        saltos = new ArrayList<>();
    }
    /*Metodo para añadir una direccion
     * recibe la direccion y el dispositivo
     * devuelve true si la configuracion es exitosa
     */

    public boolean addDireccion(Direccion d, Dispositivo dis) {
        String[] palabras = d.getIp().split("\\.");//separa la ip por octetos
        Dispositivo d2 = verificaIp(dis, d.getIp());//devuelve el disposito con el cual se realiza la conexion
        boolean b = false, ver = false;
        int pos = 0;
        if (d2 != null) {
            ver = true;
            //si el dispositivo no es un router
            if (!d2.getClass().equals(Router.class)) {
                //se añade la direccion segun su mascara
                switch (returnMsk(palabras[0])) {
                    case 1:
                        if (palabras[3].equals("0") && palabras[2].equals("0") && palabras[1].equals("0")) {
                            for (int i = 0; i < direcciones.size(); i++) {
                                if (direcciones.get(i).getIp().equals(d.getIp())) {
                                    b = true;
                                    pos = i;
                                }
                            }
                            if (!b) {
                                d.setMascara(asignarMascara(d.getIp()));
                                direcciones.add(d);
                                destinos.add(d);
                                saltos.add(0);
                                intermedios.add(null);
                            } else {
                                saltos.set(pos, 0);
                                intermedios.set(pos, null);
                            }
                        } else {
                            ver = false;
                        }
                        break;
                    case 2:
                        if (palabras[3].equals("0") && palabras[2].equals("0")) {
                            for (int i = 0; i < direcciones.size(); i++) {
                                if (direcciones.get(i).getIp().equals(d.getIp())) {
                                    b = true;
                                    pos = i;
                                }
                            }
                            if (!b) {
                                d.setMascara(asignarMascara(d.getIp()));
                                direcciones.add(d);
                                destinos.add(d);
                                saltos.add(0);
                                intermedios.add(null);
                            } else {
                                saltos.set(pos, 0);
                                intermedios.set(pos, null);
                            }
                        } else {
                            ver = false;
                        }
                        break;
                    case 3:
                        if (palabras[3].equals("0")) {
                            for (int i = 0; i < direcciones.size(); i++) {
                                if (direcciones.get(i).getIp().equals(d.getIp())) {
                                    b = true;
                                    pos = i;
                                }
                            }
                            if (!b) {
                                d.setMascara(asignarMascara(d.getIp()));
                                direcciones.add(d);
                                destinos.add(d);
                                saltos.add(0);
                                intermedios.add(null);
                            } else {
                                saltos.set(pos, 0);
                                intermedios.set(pos, null);
                            }
                        } else {
                            ver = false;
                        }
                        break;
                }
              //si es un router y el puerto pertenece al router agrega la ip sin verificar la mascara
            } else if (verRoute(d.getIp(), dis)) {
                for (int i = 0; i < direcciones.size(); i++) {
                    if (direcciones.get(i).getIp().equals(d.getIp())) {
                        b = true;
                        pos = i;
                    }
                }
                if (!b) {
                    d.setMascara(asignarMascara(d.getIp()));
                    direcciones.add(d);
                    destinos.add(d);
                    saltos.add(0);
                    intermedios.add(null);
                } else {
                    saltos.set(pos, 0);
                    intermedios.set(pos, null);
                }
            } else {
                ver = false;
            }
        }
        return ver;
    }

    //metodo para añadir las direcciones de las tablas vecinas
    public void addDireccion(Direccion d, int s, Dispositivo dis) {
        boolean b = false;
        for (int i = 0; i < direcciones.size(); i++) {
            if (direcciones.get(i).getIp().equals(d.getIp())) {
                b = true;
            }
        }
        if (!b) {
            d.setMascara(asignarMascara(d.getIp()));
            direcciones.add(d);
            destinos.add(d);
            saltos.add(s + 1);
            intermedios.add(dis);
        }
    }

    //metodo para eliminar una direccion de la tabla
    public void eliminarDireccion(String dir) {
        for (int i = 0; i < direcciones.size(); i++) {
            if (direcciones.get(i).getIp().equals(dir)) {
                direcciones.remove(i);
            }
        }
    }

    //metodo para asignar la mascara correspondiente a la ip
    public String asignarMascara(String ip) {
        String[] palabras = ip.split("\\.");
        String mascara = "";
        int primerOcteto = Integer.parseInt(palabras[0]);

        if (primerOcteto < 128) {
            mascara = "255.0.0.0";
        } else if (primerOcteto >= 128 && primerOcteto < 192) {
            mascara = "255.255.0.0";
        } else {
            mascara = "255.255.255.0";
        }
        return mascara;
    }

    /*metodo que verifica la ip con respecto a los puertos del dispositivo
     * retorna el dispositivo siguiente
     */
    public Dispositivo verificaIp(Dispositivo d, String ip) {
        Dispositivo temp = null;
        String[] palabras1 = ip.split("\\.");
        String compare = palabras1[0] + "." + palabras1[1] + "." + palabras1[2];
        switch (returnMsk(palabras1[0])) {
            case 1:
                compare = palabras1[0] + ".0.0.0";
                break;
            case 2:
                compare = palabras1[0] + "." + palabras1[1] + ".0.0";
                break;
            case 3:
                compare = palabras1[0] + "." + palabras1[1] + "." + palabras1[2] + ".0";
                break;
        }

        for (int i = 0; i < d.getPuertos().size(); i++) {
            //si tiene un puerto en uso
            if (d.getPuertos().get(i).getDireccion().getIp() != null && !d.getPuertos().get(i).getDireccion().getIp().equals("")) {
                String[] palabras2 = d.getPuertos().get(i).getDireccion().getIp().split("\\.");
                String compare2 = palabras2[0] + "." + palabras2[1] + "." + palabras2[2];
                switch (returnMsk(palabras2[0])) {
                    case 1:
                        compare2 = palabras2[0] + ".0.0.0";
                        break;
                    case 2:
                        compare2 = palabras2[0] + "." + palabras2[1] + ".0.0";
                        break;
                    case 3:
                        compare2 = palabras2[0] + "." + palabras2[1] + "." + palabras2[2] + ".0";
                        break;
                }
                //compara la ip del puerto con la ip de red
                if (compare.equals(compare2)) {
                    for (int j = 0; j < Controlador.lineas.size(); j++) {
                        if (!Controlador.lineas.get(j).getpFinal().getDireccion().getIp().equals("")) {
                            String[] palabras3 = Controlador.lineas.get(j).getpFinal().getDireccion().getIp().split("\\.");
                            String compare3 = palabras3[0] + "." + palabras3[1] + "." + palabras3[2];
                            switch (returnMsk(palabras3[0])) {
                                case 1:
                                    compare3 = palabras3[0] + ".0.0.0";
                                    break;
                                case 2:
                                    compare3 = palabras3[0] + "." + palabras3[1] + ".0.0";
                                    break;
                                case 3:
                                    compare3 = palabras3[0] + "." + palabras3[1] + "." + palabras3[2] + ".0";
                                    break;
                            }
                            //si la ip de red de la linea es igual a la ip de red ingresada
                            if (compare3.equals(compare)) {
                                for (int k = 0; k < Controlador.allDispositivos.size(); k++) {
                                    for (int l = 0; l < Controlador.allDispositivos.get(k).getPuertos().size(); l++) {
                                        //busca el dispositivo con la misma ip ingresada
                                        if (Controlador.allDispositivos.get(k).getPuertos().get(l).getDireccion().getIp().equals(Controlador.lineas.get(j).getpInicial().getDireccion().getIp()) && !Controlador.allDispositivos.get(k).equals(d)) {
                                            temp = Controlador.allDispositivos.get(k);
                                            if (temp.getClass() == Router.class && Controlador.lineas.get(j).getpFinal().getDireccion().getIp().equals(ip)) {
                                                return temp;
                                            } else if (temp.getClass().equals(Router.class)) {
                                                //no retorna si el dispositivo es un router
                                            } else {
                                                return temp;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //si la ip inicial de la linea esta configurada
                        if (!Controlador.lineas.get(j).getpInicial().getDireccion().getIp().equals("")) {
                            String[] palabras4 = Controlador.lineas.get(j).getpInicial().getDireccion().getIp().split("\\.");
                            String compare4 = palabras4[0] + "." + palabras4[1] + "." + palabras4[2];
                            switch (returnMsk(palabras4[0])) {
                                case 1:
                                    compare4 = palabras4[0] + ".0.0.0";
                                    break;
                                case 2:
                                    compare4 = palabras4[0] + "." + palabras4[1] + ".0.0";
                                    break;
                                case 3:
                                    compare4 = palabras4[0] + "." + palabras4[1] + "." + palabras4[2] + ".0";
                                    break;
                            }
                            //retorna el dispositivo si las ips son iguales
                            if (compare4.equals(compare)) {
                                for (int k = 0; k < Controlador.allDispositivos.size(); k++) {
                                    for (int l = 0; l < Controlador.allDispositivos.get(k).getPuertos().size(); l++) {
                                        if (Controlador.allDispositivos.get(k).getPuertos().get(l).getDireccion().getIp().equals(Controlador.lineas.get(j).getpFinal().getDireccion().getIp()) && !Controlador.allDispositivos.get(k).equals(d)) {
                                            temp = Controlador.allDispositivos.get(k);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return temp;
    }

    //verifica la que la ip se encuentre entre los puertos de router
    public boolean verRoute(String ip, Dispositivo d) {
        boolean band = false;
        for (int i = 0; i < d.getPuertos().size(); i++) {
            if (d.getPuertos().get(i).getDireccion().getIp().equals(ip)) {
                band = true;
            }
        }

        return band;
    }

    /*metodo para retornar la mascara
     * retorna 1 para clase A
     *         2 para clase B
     *         3 para clase C
     */
    public int returnMsk(String octeto) {
        int msk = Integer.parseInt(octeto);
        if (msk < 128) {
            return 1;
        } else if (msk >= 128 && msk < 192) {
            return 2;
        } else {
            return 3;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="getters">
    public ArrayList<Direccion> getDirecciones() {
        return direcciones;
    }

    public ArrayList<Direccion> getDestinos() {
        return destinos;
    }

    public ArrayList<Dispositivo> getIntermedios() {
        return intermedios;
    }

    public ArrayList<Integer> getSaltos() {
        return saltos;
    }
    // </editor-fold>
}
