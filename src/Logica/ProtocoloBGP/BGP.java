/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.ProtocoloBGP;

import Controlador.Controlador;
import Logica.Constantes.Constantes;
import Logica.Dispositivo;
import Logica.DispositivoGrafico;
import Logica.Nodo;
import Logica.Router;
import Logica.tools.Enlace;
import Logica.tools.Ping;
import java.util.ArrayList;
import java.util.Random;
import java.util.StringTokenizer;

/**
 * <p align="justify">
 * Clase responsable de los procesos de enrutamiento y validación las
 * comunicaciones establecidas a través del protocolo BGP, esta clase es en si
 * misma la logica del protocolo BGP
 * </p>
 * <p align="justify">
 * Soporta los siguientes procesos:
 * <li>Multiprotocolo<ul>Un router pude usar en simultaneo dos protocolo de
 * enrutameinro en estapar este contexto, un protocolo inter o iBGP para el
 * erutamiento dentr del mismo AS y el enrutamiento eBGP para la comunicacion
 * entre distintos AS</ul></li>
 * <li>Algoritmo de selecion de rutas</li>
 * </p>
 *
 * @author Leonardo Ortega Hernandez
 */
public class BGP {

    public static int it = 0;
    public static int np = 0;

    /**
     * <p align="justify">Determina entre un par de rutas cual es la mejor según
     * el criterio de selección BGP (I-BGP | E-BGP)</p>
     * <p align="justify"><b>Algoritmo de selecion</b>
     * <ol>
     * <li>Si el NEXT_HOP (siguiente salto) es inaccesible no se considera la
     * ruta.</li>
     * <li> Mayor WEIGHT: Se elige la ruta con el valor más grande de este
     * atributo. Este criterio es específico para los routers Cisco y se aplica
     * localmente en cada router.
     * </li><li> Mayor LOCAL_PREF (anunciado por I-BGP): Se elige la ruta con el
     * valor más grande de este atributo. Esto se aplica a todos los routers del
     * AS.
     * </li><li>En el caso de que se tenga el mismo valor de LOCAL_PREF, se
     * elige una ruta originada por el propio router (configurada mediante
     * comandos redistribute, aggregate o network) antes que una aprendida a
     * través de un vecino.
     * </li><li>Más corto AS_PATH: Se elige la ruta con el mínimo número de ASNs
     * en este atributo.
     * </li><li>Menor ORIGIN: Se elige la ruta según el modo en que se aprendió
     * (IGP < EGP < INCOMPLETE). </li><li>Menor MED: En el caso de que se tenga
     * el mismo origen para la ruta, en el caso de las rutas que provienen de un
     * mismo AS se elige la ruta con el mínimo valor de este atributo (se puede
     * configurar también para comparar este atributo en rutas de diferentes
     * AS).
     * </li><li>Se elige una ruta aprendida por E-BGP antes que una aprendida
     * por I-BGP.
     * </li><li>Menor IGP METRIC al NEXT-HOP: Se elige la ruta con el NEXT-HOP
     * más próximo, es decir, aquélla para la cual es necesario pasar por el
     * vecino más próximo localmente (vecino no BGP, sino IGP). Este vecino más
     * próximo vendrá indicado por la métrica IGP, de manera que sea la salida
     * más próxima del AS.
     * </li><li>Ruta hacia el router BGP con el Router-ID (dirección IP) más
     * pequeño.</li>
     * </ol>
     * </p>
     *
     * @see Rute
     * @param rutea ruta a comparar
     * @param ruteb ruta a comparar
     * @return retorna la ruta que es mejor entre las dos recibidas
     */
    public static Rute selectsPaths(Rute rutea, Rute ruteb) {
        
        Rute rute1 = (Rute) filtrarBGP(rutea);
        Rute rute2 = (Rute) filtrarBGP(ruteb);
        Rute rute;
        System.err.println("ra: " + rute1 + "\nrb: " + rute2);
        String s = rute1 + " ?? " + rute2 + " = ";
        Router RTA = null;
        Router RTB = null;
        int sizemax = (rute1.size() > rute2.size()) ? rute2.size() : rute1.size();
        System.err.println(sizemax);
        System.err.println("----------------------");
        rute1.setOrigin();
        System.err.println("----------------------");
        rute2.setOrigin();
        System.err.println(rute1.getOrigin() + "::" + rute2.getOrigin());
        if (rute1.getOrigin() != rute2.getOrigin()) {//si la ruta es interna elige esta como mejor ruta
            System.err.println("origin -> ra:" + rute1.getOrigin() + "  rb:" + rute2.getOrigin());
            rute = (rute1.getOrigin() > rute2.getOrigin()) ? rutea : ruteb;
            s += rute;
            System.err.println(s);
            return rute;
        }
        for (int i = 0; i < sizemax; i++) {//hace un seguimiento de los pesos de ambas rutas
            RTA = (Router) rute1.get(i);
            RTB = (Router) rute2.get(i);
            if (RTA.getConfigBGP().getWeight() != RTB.getConfigBGP().getWeight()) {//si una de las rutas tiene mayor peso que la otra en un tramo se elige esa
                rute = (RTA.getConfigBGP().getWeight() > RTB.getConfigBGP().getWeight()) ? rutea : ruteb;
                s += rute;
                System.err.println("weight:\n" + s);
                return rute;
            }
        }
        for (int i = 0; i < sizemax; i++) {
            RTA = (Router) rute1.get(i);
            RTB = (Router) rute2.get(i);
            //si las rutas son internas se decide por la que mayor preferencia local tenga
            if ((RTA.getConfigBGP().getLocal_pref() != RTB.getConfigBGP().getLocal_pref()) && (RTA.getConfigBGP().getAs_number() == RTB.getConfigBGP().getAs_number())) {
                System.err.println("local_prefr");
                rute = (RTA.getConfigBGP().getLocal_pref() > RTB.getConfigBGP().getLocal_pref()) ? rutea : ruteb;
                s += rute;
                System.err.println(s);
                return rute;
            }
        }
        //si una de las rutas tiene menor AS-path se elige esta
        if (rute1.getAs_path().size() != rute2.getAs_path().size()) {
            System.err.println("cant_as");
            rute = (rute1.getAs_path().size() > rute2.getAs_path().size()) ? ruteb : rutea;
            s += rute;
            System.err.println(s);
            return rute;
        }
        for (int i = 0; i < sizemax; i++) {
            RTA = (Router) rute1.get(i);
            RTB = (Router) rute2.get(i);
            //si una de las rutas tiene mayor valor de métrica se elige esta como la mejor
            if (RTA.getConfigBGP().getMed() != RTB.getConfigBGP().getMed()) {
                System.err.println("med");
                rute = (RTA.getConfigBGP().getMed() > RTB.getConfigBGP().getMed()) ? rutea : ruteb;
                s += rute;
                System.err.println(s);
                return rute;
            }
        }
        //si una de las rutas tiene menor numero de saltos que la otra se elije esta
        if (rute1.size() != rute2.size()) {
            System.err.println("saltos redes");
            rute = (rute1.size() > rute2.size()) ? ruteb : rutea;
            s += rute;
            System.err.println(s);
            return rute;
        }
        if (rutea.size() != ruteb.size()) {
            System.err.println("ruta mas corta");
            rute = (rutea.size() > ruteb.size()) ? ruteb : rutea;
            s += rute;
            System.err.println(s);
            return rute;
        }
        //en este for se elije la ruta que tenga menor numero de ip network
        for (int i = 0; i < sizemax; i++) {
            RTA = (Router) rute1.get(i);
            RTB = (Router) rute2.get(i);
            String ip0 = RTA.getConfigBGP().getIpnetwork().getIp();
            String ip1 = RTB.getConfigBGP().getIpnetwork().getIp();
            if ((!"".equalsIgnoreCase(ip0) && !"".equalsIgnoreCase(ip1))) {
                StringTokenizer t1 = new StringTokenizer(ip0, ".");
                StringTokenizer t2 = new StringTokenizer(ip1, ".");
                for (int j = 0; j < 4; j++) {
                    int a = Integer.parseInt(t1.nextToken());
                    int b = Integer.parseInt(t2.nextToken());;
                    if (a != b) {
                        rute = (a > b) ? ruteb : rutea;
                        s += rute;
                        System.err.println("network: " + ip0 + " - " + ip1 + "\n" + s);
                        return rute;
                    }
                }
            }
        }
        Random r = new Random();
        rute = ((r.nextInt() % 2) == 0) ? rutea : ruteb;
        s += rute;
        System.err.println("Random; \n" + s);
        return rute;
    }

    /**
     * Metodo recursivo que encuentra rutas correctamente configuradas entre un
     * dispocitivo y otro
     *
     * @param a Dispositivo inicial
     * @param b Dispositivo final
     * @param path ruta inicial que únicamente debe contener el dispositivo a
     * @param paths un array vació que será llenado con las rutas encontradas
     */
    public static void createPaths(Dispositivo a, Dispositivo b, Rute path, ArrayList paths) {
        it++;
        //si el dispocitivo actual es igual al dispocitivo destino termono con una ruta
        if (a == b) {
            np++;
            boolean t = false;
            if (Enlace.isNice(path)) {
                t = true;
                paths.add(path);
            }
            return;
        }
        int con = 0;
        Rute path1 = new Rute(path);

        for (int i = 0; i < a.getVecinos().size(); i++) {
            Dispositivo r = a.getVecinos().get(i);
            if (con == 0 && !path.contains(r)) {
                path.add(r);
                createPaths(r, b, path, paths);
                con++;
            } else if (!path1.contains(r)) {
                Rute path2 = new Rute(path1);
                path2.add(r);
                createPaths(r, b, path2, paths);
            }
        }

    }

    /**
     * Este método encuentra caminos entre dos dispositivos que estan
     * configurados correctamente, los objetos dentro del arraylist devuelto son
     * de tipo Ruta (es una clase que extiende de Arraylist) y contiene los
     * dispositivos que conforma en camino
     *
     * @param a dispositivo inicial en la ruta
     * @param b dispocitivo final
     * @return un arraylist que contiene todos los caminos encontrados
     */
    public static ArrayList createPaths(Dispositivo a, Dispositivo b) {
        Rute path = new Rute();
        path.add(a);
        ArrayList paths = new ArrayList<>();
        createPaths(a, b, path, paths);
        return paths;
    }

    /**
     * haya caminos correcta mente configurados entre un dispositivo inicial y
     * una lista de otros dispositivos
     *
     * @param d0 dispositivo inicial
     * @param list lista de dispositivos
     * @return devuelve un lista de rutas de los caminos encontrados
     */
    public static ArrayList<Rute> createPaths(Dispositivo d0, ArrayList<Dispositivo> list) {
        ArrayList paths = new ArrayList<>();
        for (int j = 0; j < list.size(); j++) {
            Dispositivo d1 = list.get(j);
            if (d0 != d1) {
                Rute routers = new Rute();
                routers.add(d0);
                ArrayList paths1 = new ArrayList<>();
                createPaths(d0, d1, routers, paths1);
                paths.addAll(paths1);
            }
        }
        return paths;
    }

    /**
     * <p>Elige el mejor camino entre un un par de nodos utilizando los criterio
     * BGP</p><p>En caso de que la ruta involucre mas de un router de borde la
     * ruta sera modificada según el protocolo interno de cada router de
     * borde</p>
     *
     * @param a Dispocitivo inicial
     * @param b Dispocitivo final
     * @param cantidadPaquetes Cantidad de paquetes a enviar
     * @return un lista de dispositivos visuales que son usados por la Interface
     * grafica
     */
    public static ArrayList<DispositivoGrafico> best(Dispositivo a, Dispositivo b, int cantidadPaquetes) {
        ArrayList paths = new ArrayList();
        ArrayList pathsIGP = new ArrayList();
        Rute path = new Rute();
        path.add(a);
        Controlador controlador = new Controlador();
        createPaths(a, b, path, paths);
        if (paths.isEmpty()) {
            Controlador.salida = Constantes.ERROR_NODOS_DESCONFIGURADOS;
            return null;
        }
        if (paths.size() == 1) {
            Controlador.salida = "Todo bien<br>Ruta: " + paths.get(0);
            return Enlace.caminoGrafico((Rute) paths.get(0));
        }
        Rute bestpath = selectsPaths(paths);
        bestpath = getRutaMultiprotocolo(bestpath);
        if (bestpath == null) {
            return null;
        }
        Controlador.salida = "Todo bien<br>Ruta:" + bestpath + Controlador.salida;
        return Enlace.caminoGrafico(bestpath);
    }

    /**
     * Sobrecarga del método selectsPaths para facilitar su uso
     *
     * @param paths con los rutas encontradas entre un nodo y otro
     * @return Una ruta que es la mejor según BGP
     */
    public static Rute selectsPaths(ArrayList paths) {
        Rute bestpath = (Rute) paths.get(0);
        for (int i = 0; i < paths.size() - 1; i++) {
            Rute r2 = (Rute) paths.get(1 + i);
            bestpath = selectsPaths(bestpath, r2);
        }
        return bestpath;
    }

    /**
     * Selecciona de una Ruta los dispositivos que son router
     *
     * @param rute una ruta
     * @return devuelve una lista con los router encontrados
     */
    public static Rute filtrar(ArrayList<Dispositivo> rute) {
        Rute list = new Rute();
        for (int i = 0; i < rute.size(); i++) {
            if (rute.get(i).getClass() == Router.class) {
                list.add(rute.get(i));
            }
        }
        return list;
    }

    /**
     * De una lista de dispositivos selecciona los que sean router bgp activos
     *
     * @param rute lista de dispositivos
     * @return retorna una ruta que solo contiene router bgp
     */
    public static Rute filtrarBGP(ArrayList<Dispositivo> rute) {
        Rute list = new Rute();
        for (int i = 0; i < rute.size(); i++) {
            if (rute.get(i).getClass() == Router.class) {
                Router r = (Router) rute.get(i);
                if (r.getConfigBGP().isEnabled()) {
                    list.add(r);
                }
            }
        }
        return list;
    }

    /**
     * Selecciona de una Ruta los dispositivos que son Nodos
     *
     * @param rute una ruta con dispositivos
     * @return devuelve una lista con los nodos encontrados
     */
    public static Rute filtrarNodos(ArrayList<Dispositivo> rute) {
        Rute list = new Rute();
        for (int i = 0; i < rute.size(); i++) {
            if (rute.get(i).getClass() == Nodo.class) {
                list.add(rute.get(i));
            }
        }
        return list;
    }

    /**
     * <p>Implementa el proceso de multiprotocolo dentro del proceso BGP según
     * sea el protocolo de los router BGP</p><p> Este método hace que dentro de
     * un AS se pueda manejar un protocolo de enrutamiento distinto al de otro
     * AS</p>
     *
     * @param rute rura elegida por BGP sin tener encuentra los otros protocolos
     * @return retorna una ruta optimizada por los otros protocolos
     */
    public static Rute getRutaMultiprotocolo(Rute rute) {
        String salidad = "";
        int Protocolo = Constantes.OSPF;
        rute.setOrigin();
        Nodo a = (Nodo) rute.get(0);
        Nodo b = (Nodo) rute.get(rute.size() - 1);
        Ping ping = new Ping(a, b, 3);
        Rute rute_bgp = rute.get();
        if (rute.getOrigin() == Rute.IGP) {
            if (rute_bgp.size() == 1) {
                Router router = (Router) rute_bgp.get(0);
                Rute r1 = toRute(ping.enviar(router.getConfigBGP().getiBGP()));
                r1.setOrigin();
                if (r1.getAs_path().size() == 1) {
                    if (r1.getAs_path().get(0) == rute.getAs_path().get(0)) {
                        return r1;
                    }
                } else if (r1.getAs_path().isEmpty()) {
                    return r1;
                }
            } else if (rute_bgp.isEmpty()) {
                Rute r1 = toRute(ping.enviar(Protocolo));
                if (r1.get().isEmpty()) {
                    return r1;
                }
            } else if (rute_bgp.size() > 1) {
                Rute rutea = rutei(a, b, (Router) rute_bgp.get(0), "AtoB");
                salidad += Controlador.salida;
                Rute ruteb = rutei(b, a, (Router) rute_bgp.get(rute_bgp.size() - 1), "BtoA");
                salidad += Controlador.salida;
                if (rutea == null || ruteb == null) {
                    Controlador.salida = salidad;
                    return null;
                }
                Rute intermedia = rutee(rute, rute_bgp);
                Rute total = new Rute();
                System.err.println("Rute a: " + rutea);
                System.err.println("Rute I: " + intermedia);
                System.err.println("Rute b: " + ruteb);
                total.addAll(rutea);
                total.addAll(intermedia);
                total.addAll(ruteb);
                return total;
            }
        } else {
            Rute rutea = rutei(a, b, (Router) rute_bgp.get(0), "AtoB");
            salidad += Controlador.salida;
            Rute ruteb = rutei(b, a, (Router) rute_bgp.get(rute_bgp.size() - 1), "BtoA");
            salidad += Controlador.salida;
            if (rutea == null || ruteb == null) {
                Controlador.salida = salidad;
                return null;
            }
            System.err.println("hola ruta externa2: " + rute_bgp);
            Rute intermedia = rutee(rute, rute_bgp);
            Rute total = new Rute();
            System.err.println("Rute a: " + rutea);
            System.err.println("Rute I: " + intermedia);
            System.err.println("Rute b: " + ruteb);
            total.addAll(rutea);
            total.addAll(intermedia);
            total.addAll(ruteb);
            return total;
        }

        return rute;
    }

    /**
     * Toma una lista de dispositivos gráficos y la convierte en una Rute
     *
     * @param gráficos lista de dispositivos gráficos
     * @return retorna una instancia de Rute
     * @see Rute
     * @see DispositivoGrafico
     */
    public static Rute toRute(ArrayList<DispositivoGrafico> graficos) {
        Rute rute = new Rute();
        for (int i = 0; i < graficos.size(); i++) {
            rute.add(graficos.get(i).getDispositivo());
        }
        return rute;
    }

    /**
     * <p>Genera una ruta según el protocolo ibgp del router, desde un nodo
     * hasta donde este el primer router de borde</p><p>Debido a que a partir de
     * hay se debe ejecutar otro protocolo ya sea el de comunicación BGP o el
     * protocolo que posea el AS destino</p>
     *
     * @param a nodo inicial
     * @param b nodo final
     * @param r una ruta que comunica a con b
     * @param sentido indica en qué sentido se usa el protocolo interno con un
     * valor String<li>si el sentido es desde a a b sentido = "AtoB"</li><li>si
     * el sentido es desde b a a sentido = "BtoA"</li>
     * @return Retorna una instancia de Rute desde el nodo a hasta el primer
     * router de borde o null
     * @see Nodo
     * @see Rute
     */
    public static Rute rutei(Nodo a, Nodo b, Router r, String sentido) {
        Rute rute = new Rute();
        Ping ping = new Ping(a, b, 3);
        ArrayList rutaigp = ping.enviar(r.getConfigBGP().getiBGP());
        if (rutaigp == null) {
            return null;
        }
        Rute r1 = toRute(rutaigp);
        if (r1.get().isEmpty()) {
            return null;
        }
        for (int i = 0; i < r1.size(); i++) {
            if ("AtoB".equalsIgnoreCase(sentido)) {
                rute.add(r1.get(i));
            } else if ("BtoA".equalsIgnoreCase(sentido)) {
                rute.add(0, r1.get(i));
            }
            if (r1.get(i).getClass() == Router.class) {
                Router d = (Router) r1.get(i);
                if (d.getConfigBGP().isEnabled()) {
                    if (d == r) {
                        return rute;
                    }
                    return null;
                }
            }
        }
        return null;
    }

    /**
     * Corta la parte de una ruta donde empieza el primer router bgp hasta el
     * ultimo router bgp
     *
     * @param rute ruta completa
     * @param rutebgp ruta que solo contiene los router de borde
     * @return Retorna una rute entre el primer y el ultimo router de borde de
     * una ruta completa
     */
    public static Rute rutee(Rute rute, Rute rutebgp) {
        Rute rutamedia = new Rute(rute);
        boolean eliminar = true;
        int i = 0;
        for (; rutamedia.size() > i;) {
            System.err.println("index: " + i + "  " + rutamedia);
            if (rutebgp.get(0) == rutamedia.get(i)) {
                rutamedia.remove(i);
                eliminar = false;
            }
            if (rutebgp.get(rutebgp.size() - 1) == rutamedia.get(i)) {
                rutamedia.remove(i);
                eliminar = true;
            }
            if (eliminar) {
                rutamedia.remove(i);
            } else {
                i++;
            }
        }
        return rutamedia;
    }
}
