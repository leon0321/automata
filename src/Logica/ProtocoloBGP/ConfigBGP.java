package Logica.ProtocoloBGP;

import Logica.Constantes.Constantes;
import Logica.Direccion;
import Logica.Router;
import java.io.Serializable;

/**
 * Clase modelo que contiene los valores de la configuracion del router BGP
 * tiene los siguientes atributos
 * <li>private Router <b>router</b>: Router que posse la configuracion BGP</li>
 * <li>private Direccion <b>ipnetwork</b>: Direcion ip del router BGP</li>
 * <li>private int <b>as_number</b>: Numero del AS del router BGP</li>
 * <li>private int <b>local_pref</b>: Preferencia local</li>
 * <li>private int <b>med</b>: Metrica del la configuracion BGP</li>
 * <li>private int <b>weight</b>: Peso de la condiguracion bgp del router</li>
 * <li>private boolean <b>enabled</b>: Indca si la configuracion esta activa o
 * no</li>
 * <li>private int <b>iBGP</b>: Posee el indicar del protocolo interno del
 * router</li>
 *
 * @author Leonardo Ortega Hernandez
 */
public class ConfigBGP implements Serializable {

    private Router router;
    private Direccion ipnetwork;//network ip
    private int as_number = 100;//router bgp n
    private int local_pref = 100;//set-local-prefe n
    private int med = 100;//set-med n
    private int weight = 100;//set-weight n
    private boolean enabled = false;//Configuracion AS abilitada o desabilitada
    private int iBGP = Constantes.OSPF;//Protocolo interno del AS

    /**
     * Construye una configuracion por defecto de un router
     *
     * @param router router que sera configurado
     */
    public ConfigBGP(Router router) {
        this.router = router;
        ipnetwork = new Direccion();
    }

    /**
     * Retorna la dirección ip bgp del router
     *
     * @return Retorna una instancia de Dirección
     * @see Dirección
     */
    public Direccion getIpnetwork() {
        return ipnetwork;
    }

    /**
     * Modifica la dirección ip bgp del router
     *
     * @param ipnetwork Recibe una instancia de Direccion
     * @see Dirección
     */
    public void setIpnetwork(Direccion ipnetwork) {
        this.ipnetwork = ipnetwork;
    }

    /**
     * Devuelve el numero AS del router
     *
     * @return
     */
    public int getAs_number() {
        return as_number;
    }

    /**
     * Modifica el numero AS del router
     *
     * @param as_number nuevo valor entero
     */
    public void setAs_number(int as_number) {
        this.as_number = as_number;
    }

    /**
     * Devuelve el valor de la preferencia local del router bgp
     *
     * @return valor entero local prefe
     */
    public int getLocal_pref() {
        return local_pref;
    }

    /**
     * Modifica el valor de la preferencia local
     *
     * @param local_pref nuevo valor
     */
    public void setLocal_pref(int local_pref) {
        this.local_pref = local_pref;
    }

    /**
     * Devuelve el valor de la metrica
     *
     * @return valor de la metrica
     */
    public int getMed() {
        return med;
    }

    /**
     * Modifica valor de la metrica del router en la configuracion bgp
     *
     * @param med nuevo valor de metrica
     */
    public void setMed(int med) {
        this.med = med;
    }

    /**
     * Devuelve el peso del router bgp
     *
     * @return valor de atributo weight
     */
    public int getWeight() {
        return weight;
    }

    /**
     * Modifica el valor del atributo weight o peso de la configuracion bgp
     *
     * @param weight nuevo valor del peso
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    /**
     * Indica si la configuración bgp esta activada o desactivada
     *
     * @return Retorna false si no esta activa la configuración bgp en caso
     * contrario retorna true
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Activa o desactiva la configuración bgp en el router
     *
     * @param enabled valor boleano false para desactivar y true para activar la
     * configuración
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Devuelve el identificardor del protocolo que esta usando el router bgp
     * como iBGP. El valor de indicador esta definido en la clase Constantes
     *
     * @return Retorna el protocolo actula interno del router
     * @see Constantes
     */
    public int getiBGP() {
        return iBGP;
    }

    /**
     * Modifica el protocolo interno que usa el router
     *
     * @param iBGP nuevo valor del protocolo a usar
     */
    public void setiBGP(int iBGP) {
        this.iBGP = iBGP;
    }
}