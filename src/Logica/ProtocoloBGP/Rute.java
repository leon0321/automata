/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.ProtocoloBGP;

import Logica.Dispositivo;
import Logica.Router;
import java.util.ArrayList;

/**
 * Objeto que contine una ruta de dispositivos
 *
 * @author Leonardo Ortega Hernandez
 */
public class Rute extends ArrayList<Dispositivo> {

    /**
     * Representa que la ruta ha sido aprendida desde fuera del AS
     */
    public static final int IGP = 3;
    /**
     * Representa que la ruta ha sido aprendida desde dentro del AS
     */
    public static final int EGP = 2;
    /**
     * Representa que la ruta ha siso aprendida desde un origen desconocido del
     * AS
     */
    public static final int INCOMPLETE = 1;
    /**
     * Posee el valor de origen de la ruta actual este valor puede ser ( IGP |
     * EGP | INCOMPLETE )
     */
    private int origin = IGP;
    /**
     * Es una lista de los numeros AS por los que atraviesa la ruta
     */
    private ArrayList<Integer> as_path = new ArrayList<>();//Contiene el AS-Path de la ruta

    /**
     * Constructor que toma una lista de dispositivos para crear una ruta
     *
     * @param rute ruta base para crear una nueva instacia de ruta
     */
    public Rute(Rute rute) {
        this.addAll(rute);
    }

    /**
     * Costructor que crea un ruta vacia
     */
    public Rute() {
        super();
    }

    /**
     * Devuelve el as-path debe ser utilizado despues de usar el metodo
     * setOrigin()
     *
     * @return Retorna una array de integer que es AS-Path
     */
    public ArrayList<Integer> getAs_path() {
        return as_path;
    }

    /**
     * Devueve el valor del origen de la ruta
     *
     * @return Retorna un valor int que es el valor de origen de la ruta
     */
    public int getOrigin() {
        return origin;
    }

    /**
     * Calcula el origen de la ruta y modifica el atributo origin
     */
    public void setOrigin() {
        ArrayList<Dispositivo> list = BGP.filtrar(this);
        if (list.isEmpty()) {
            return;
        }
        Router r0 = (Router) list.get(0);
        as_path.add(r0.getConfigBGP().getAs_number());
        for (int i = 0; i < list.size(); i++) {
            Router r1 = (Router) list.get(i);
            if (!as_path.contains(r1.getConfigBGP().getAs_number())) {
                as_path.add(r1.getConfigBGP().getAs_number());
            }
            if (r1.getConfigBGP().getAs_number() != r0.getConfigBGP().getAs_number()) {
                origin = EGP;
            }
        }
    }

    /**
     * Filtra la ruta y solo devela un array que contiene los router de borde de
     * la red
     *
     * @return Retorna una array con los router de borde
     */
    public Rute get() {
        Rute rute = new Rute();
        int cont = 0;
        for (int i = 0; i < this.size(); i++) {
            if (this.get(i).getClass() == Router.class) {
                Router router = (Router) this.get(i);
                if (router.getConfigBGP().isEnabled()) {
                    rute.add(router);
                    cont++;
                }
            }
        }
        return rute;
    }
}
