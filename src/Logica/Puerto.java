/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Controlador.Controlador;
import Logica.Constantes.Constantes;
import Logica.automatas.AutomataIP;
import Logica.automatas.AutomataMascara;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Esta clase abstrae las carracteristicas del un puerdo de un dispocitivo en la
 * red
 *
 * @author Jorge
 */
public class Puerto implements Serializable {

    private String nombre;
    private Direccion direccion;
    private boolean disponible;

    public Puerto() {
    }

    public Puerto(String nombre) {
        direccion = new Direccion();
        disponible = true;
        this.nombre = nombre;
    }

    public Puerto(Direccion direccion, boolean disponible, String nombre) {
        this.direccion = direccion;
        this.disponible = disponible;
        this.nombre = nombre;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Método para configurar un Puerto
     *
     * @param puerto
     * @return String con el resultado de la operacion dependiendo de los
     * errores <b>DISPOSITIVO_CONFIGURADO_CORRECTAMENTE</b>
     * <b>ERROR_IP_INVALIDA</b> <b>ERROR_MASCARA_INVALIDA</b>
     * <b>ERROR_NODO_IP_EXISTE</b>
     */
    public String configurar(Puerto puerto) {
        String salida = "";
        //Se valida con los autómatas si el parámetro es correcto ipOctetos y el
        //parámetro MAC es correcto.
        AutomataIP ai = new AutomataIP(puerto.getDireccion().getIp());
        AutomataMascara am = new AutomataMascara(puerto.getDireccion().getMascara());

        boolean ip = true;
        boolean mascara = true;
        if (!puerto.getDireccion().getIp().equals("") || !puerto.getDireccion().getMascara().equals("")) {
            ip = ai.inicio();
            mascara = am.inicio();
        }


        //Luego de validar si los parámetros son válidos se debe
        //validar que las direcciones ipOctetos y mac no existan con 
        //anterioridad

        boolean repetido = validarIpRepetida(puerto, this);

        //Si repetido es falso no hay repeticiones por lo tanto se asignan
        //los campos
        if (!repetido) {
            if (ip && mascara) {
                this.getDireccion().setIp(puerto.getDireccion().getIp());
                this.getDireccion().setMascara(puerto.getDireccion().getMascara());
                return Constantes.PUERTO_CONFIGURADO_CORRECTAMENTE;
            } else {
                if (!ip) {
                    //this.getDireccion().setIp(null);
                    return Constantes.ERROR_IP_INVALIDA;
                } else if (!mascara) {
                    //this.getDireccion().setMascara(null);
                    return Constantes.ERROR_MASCARA_INVALIDA;
                }

            }
        } else {
            return Constantes.ERROR_IP_EXISTE;
        }

        return salida;
    }

    //<editor-fold defaultstate="collapsed" desc="validarIpRepetida">
    /**
     * Mètodo para validar si una ipOctetos esta repetida
     *
     * @param puertoNuevo
     * @param puertoActual
     * @return boolean con el resultado de la busqueda. True si la ipOctetos se
     * encuentra usada
     */
    private boolean validarIpRepetida(Puerto puertoNuevo, Puerto puertoActual) {

        boolean retorno = false;
        String ip = puertoNuevo.getDireccion().getIp();

        for (int i = 0; i < Controlador.allDispositivos.size(); i++) {
            for (Puerto puerto : Controlador.allDispositivos.get(i).getPuertos()) {
                if (ip.equals(puerto.getDireccion().getIp()) && puertoActual != puerto && !ip.equals("")) {
                    retorno = true;
                    break;
                }
            }

        }
        return retorno;
    }
    //</editor-fold>

    public String calcularRed() {
        return calcularRed(getDireccion().getIp(), getDireccion().getMascara());
    }

    public String calcularRed(String ip, String mascara) {
        String s = "";
        ArrayList<String> ipOctetos = new ArrayList<>();
        ArrayList<String> maskOctetos = new ArrayList<>();

        StringTokenizer ipTokens = new StringTokenizer(ip, ".");
        while (ipTokens.hasMoreTokens()) {
            ipOctetos.add(ipTokens.nextToken());
        }

        StringTokenizer maskTokens = new StringTokenizer(mascara, ".");
        while (maskTokens.hasMoreTokens()) {
            maskOctetos.add(maskTokens.nextToken());
        }

        for (int i = 0; i < ipOctetos.size(); i++) {
            s += compararOctetos(ipOctetos.get(i), maskOctetos.get(i));
            if (i < ipOctetos.size() - 1) {
                s += ".";
            }
        }

        //System.out.println(s);
        return s;
    }

    public boolean ipUsable() {
        return ipUsable(getDireccion().getIp(), getDireccion().getMascara());
    }

    public boolean ipUsable(String ip, String mascara) {
        boolean usable = true;
        String red = calcularRed(ip, mascara);
        System.out.println("red: " + red);
        String broadcast = calcularBroadcast(red, mascara);
        System.out.println("broadcast:" + broadcast);

        if (ip.equals(red) || ip.equals(broadcast)) {
            usable = false;
        }

        return usable;
    }

    private String calcularBroadcast(String red, String mascara) {
        String s = "";
        ArrayList<String> redOctetos = new ArrayList<>();
        ArrayList<String> maskOctetos = new ArrayList<>();

        StringTokenizer ipTokens = new StringTokenizer(red, ".");
        while (ipTokens.hasMoreTokens()) {
            redOctetos.add(ipTokens.nextToken());
        }

        StringTokenizer maskTokens = new StringTokenizer(mascara, ".");
        while (maskTokens.hasMoreTokens()) {
            maskOctetos.add(maskTokens.nextToken());
        }

        for (int i = 0; i < redOctetos.size(); i++) {
            s += compararBroadcast(redOctetos.get(i), maskOctetos.get(i));
            if (i < redOctetos.size() - 1) {
                s += ".";
            }
        }

        //System.out.println(s);
        return s;
    }

    /**
     * Funcion para comparar y obtener los octetos del broadcast
     *
     * @param red
     * @param mask
     * @return
     */
    private String compararBroadcast(String red, String mask) {
        String broadOcteto = "";

        //pasamos los octetos a binario
        String redBinario = Integer.toBinaryString(Integer.parseInt(red));
        String maskBinario = Integer.toBinaryString(Integer.parseInt(mask));

        //completamos los octetos
        redBinario = completarOctetos(redBinario);
        maskBinario = completarOctetos(maskBinario);

        for (int i = 0; i < redBinario.length(); i++) {
            if (maskBinario.charAt(i) == '1') {
                broadOcteto += redBinario.charAt(i);
            } else {
                broadOcteto += "1";
            }
        }

        //System.out.println(redOcteto);

        int entero = 0;
        for (int i = 0; i < broadOcteto.length(); i++) {
            if (broadOcteto.charAt(i) == '1') {
                entero += Math.pow(2, broadOcteto.length() - 1 - i);
            }
        }
        //System.out.println(entero);

        broadOcteto = entero + "";
        return broadOcteto;
    }

    /**
     * funcion para comparar los octetos y calcular la red
     *
     * @param ip
     * @param mask
     * @return
     */
    private String compararOctetos(String ip, String mask) {
        String redOcteto = "";

        //pasamos los octetos a binario
        String ipBinario = Integer.toBinaryString(Integer.parseInt(ip));
        String maskBinario = Integer.toBinaryString(Integer.parseInt(mask));

        //completamos los octetos
        ipBinario = completarOctetos(ipBinario);
        maskBinario = completarOctetos(maskBinario);

        for (int i = 0; i < ipBinario.length(); i++) {
            if (ipBinario.charAt(i) == '1' && maskBinario.charAt(i) == '1') {
                redOcteto += "1";
            } else {
                redOcteto += "0";
            }
        }

        //System.out.println(redOcteto);

        int entero = 0;
        for (int i = 0; i < redOcteto.length(); i++) {
            if (redOcteto.charAt(i) == '1') {
                entero += Math.pow(2, redOcteto.length() - 1 - i);
            }
        }
        //System.out.println(entero);

        redOcteto = entero + "";
        return redOcteto;
    }

    /**
     * funcion para completar los octetos hasta 8
     *
     * @param octeto
     * @return
     */
    private String completarOctetos(String octeto) {
        while (octeto.length() < 8) {
            octeto = "0" + octeto;
        }

        return octeto;
    }

    public String calcularMascara(String ip) {
        System.out.println("llamo a calcular mascara");
        String mascara = "";
        StringTokenizer token = new StringTokenizer(ip, ".");

        String octetoBinario = Integer.toBinaryString(Integer.parseInt(token.nextToken()));

        octetoBinario = completarOctetos(octetoBinario);

        if (octetoBinario.charAt(0) == '0') {
            mascara = "255.0.0.0";
        } else if (octetoBinario.charAt(0) == '1' && octetoBinario.charAt(1) == '0') {
            mascara = "255.255.0.0";
        } else if (octetoBinario.charAt(0) == '1' && octetoBinario.charAt(1) == '1' && octetoBinario.charAt(2) == '0') {
            mascara = "255.255.255.0";
        } else {
            mascara = "";
            System.out.println("no se cambio la mascara");
        }

        return mascara;
    }
}
