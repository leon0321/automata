/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.ProtocoloOSPF;

import Logica.Dispositivo;
import Logica.Puerto;

/**
 * Clase auxiliar para la implementación del protocolo OSPF
 *
 * @author Andrés Betin
 */
public class NodoOSPF {

    Dispositivo dispositivo;
    Puerto puerto;
    Dispositivo dispositivoAnterior;
    Puerto puertoAnterior;
    int costo;
    int iteracion;

    public NodoOSPF(Dispositivo dispositivo, Puerto puerto, Dispositivo dispositivoAnterior, Puerto puertoAnterior, int costo, int iteracion) {
        this.dispositivo = dispositivo;
        this.puerto = puerto;
        this.dispositivoAnterior = dispositivoAnterior;
        this.puertoAnterior = puertoAnterior;
        this.costo = costo;
        this.iteracion = iteracion;
    }

    //<editor-fold defaultstate="collapsed" desc="Gets y Sets">
    public int getCosto() {
        return costo;
    }

    public void setCosto(int costo) {
        this.costo = costo;
    }

    public Dispositivo getDispositivo() {
        return dispositivo;
    }

    public void setDispositivo(Dispositivo dispositivo) {
        this.dispositivo = dispositivo;
    }

    public Dispositivo getDispositivoAnterior() {
        return dispositivoAnterior;
    }

    public void setDispositivoAnterior(Dispositivo dispositivoAnterior) {
        this.dispositivoAnterior = dispositivoAnterior;
    }

    public int getIteracion() {
        return iteracion;
    }

    public void setIteracion(int iteracion) {
        this.iteracion = iteracion;
    }

    public Puerto getPuerto() {
        return puerto;
    }

    public void setPuerto(Puerto puerto) {
        this.puerto = puerto;
    }

    public Puerto getPuertoAnterior() {
        return puertoAnterior;
    }

    public void setPuertoAnterior(Puerto puertoAnterior) {
        this.puertoAnterior = puertoAnterior;
    }
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString">
    @Override
    public String toString() {
        return getDispositivo().getNombre();
    }
    //</editor-fold>
}
