/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.ProtocoloOSPF;

import Controlador.Controlador;
import Logica.*;
import Logica.Constantes.Constantes;
import Logica.Probabilidad.Probabilidad;
import java.util.ArrayList;

/**
 *
 * @author Andres Betin
 */
public class OSPF {

    private ArrayList<NodoOSPF> t;
    private ArrayList<NodoOSPF> etiquetasAlmacenadas;
    private ArrayList<DispositivoGrafico> camino;
    private int contadorIteracion = 0;
    private ArrayList<Dispositivo> todos;
    private Probabilidad probabilidad;
    private boolean mismared;
    private Dispositivo inicial;
    private Dispositivo finall;

    public OSPF(ArrayList<Dispositivo> all) {
        t = new ArrayList<>();
        etiquetasAlmacenadas = new ArrayList<>();
        camino = new ArrayList<>();
        todos = all;
        probabilidad = new Probabilidad();
    }

    //<editor-fold defaultstate="collapsed" desc="Gets y Sets">
    public ArrayList<DispositivoGrafico> getCamino() {
        return camino;
    }

    public void setCamino(ArrayList<DispositivoGrafico> camino) {
        this.camino = camino;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Método enrutamiento">
    /**
     * Método que comunica dos dispositivos por el camino <i>visible</i> mas
     * corto
     *
     * @param start
     * @param last
     * @param mismaRed determina si los dos dispositivos se encuentran en la
     * misma red
     * @return Salida con el camino seguido en caso de exito, o un error si no
     * se encuentran caminos.
     */
    private String enrutamiento(Dispositivo start, Dispositivo last, boolean mismaRed) {
        String salida;
        this.mismared = mismaRed;
        this.inicial = start;
        this.finall = last;

        tablaEnlaces(start);

        //Esto es para determinar si el dispositivo se encuentra al alcance del dispositivo start
        boolean alcanzable = true;
        for (NodoOSPF nodoOSPF : etiquetasAlmacenadas) {
            if (nodoOSPF.getDispositivo() == last && nodoOSPF.getCosto() == -1) {
                alcanzable = false;
            }
        }

        ArrayList<Dispositivo> caminoCorto = new ArrayList<>();
        if (!alcanzable) {
            salida = Constantes.ERROR_NODOS_INALCANZABLES;
        } else {
            NodoOSPF secuencia = null;
            for (NodoOSPF nodoOSPF : t) {
                if (nodoOSPF.getDispositivo() == last) {
                    secuencia = nodoOSPF;
                    break;
                }
            }
            ArrayList<DispositivoGrafico> caminoIntermedio = new ArrayList<>();
            while (secuencia.getDispositivo() != start) {
                caminoCorto.add(secuencia.getDispositivo());
                for (DispositivoGrafico dispositivoGrafico : Controlador.graficos) {
                    if (dispositivoGrafico.getDispositivo() == secuencia.getDispositivo()) {
                        caminoIntermedio.add(dispositivoGrafico);
                    }
                }
                for (NodoOSPF nodoOSPF : t) {
                    if (nodoOSPF.getDispositivo() == secuencia.dispositivoAnterior) {
                        secuencia = nodoOSPF;
                        break;
                    }
                }
            }

            for (DispositivoGrafico dispositivoGrafico : Controlador.graficos) {
                if (dispositivoGrafico.getDispositivo() == start) {
                    caminoIntermedio.add(dispositivoGrafico);
                }
            }

            for (int i = caminoIntermedio.size() - 1; i >= 0; i--) {
                camino.add(caminoIntermedio.get(i));
            }

            caminoCorto.add(start);


            salida = "";
            for (int i = caminoCorto.size() - 1; i >= 0; i--) {
                salida += caminoCorto.get(i).getNombre();
                if (i != 0) {
                    salida += "->";
                }
            }
        }
        return salida;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Método tablaEnlaces">
    /**
     * Metodo para crear la tabla de enlaces.
     *
     * @param inicial
     * @param allDispositivos
     */
    private void tablaEnlaces(Dispositivo inicial) {
        t = new ArrayList<>();
        etiquetasAlmacenadas = new ArrayList<>();
        contadorIteracion = 0;


        NodoOSPF nodoOSPFInicial = new NodoOSPF(inicial, new Puerto("puerto"), null, null, 0, contadorIteracion++);

        for (Dispositivo dispositivo : todos) {
            if (dispositivo == inicial) {
                etiquetasAlmacenadas.add(nodoOSPFInicial);
            } else {
                etiquetasAlmacenadas.add(new NodoOSPF(dispositivo, new Puerto("puerto"), null, new Puerto("puertoAnterior"), -1, -1));
            }
        }

        t.add(nodoOSPFInicial);

        algoritmoDijsktra(nodoOSPFInicial);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Método algoritmoDijsktra">
    /**
     * Algoritmo de Dijkstra aplicado al nodo incial Organiza la tabla de
     * enlaces de el nodo inicial. Además, en este método se encuentra la 
     * lógica de vector de distancias, ya que, si bien VD utiliza el algoritmo 
     * de Bellman-Ford -que también encuentra la ruta más corta en un grado 
     * ponderado-, el algoritmo de Dijkstra es más eficiente que el de 
     * Bellman-Ford
     * @param nodoOSPF
     */
    private void algoritmoDijsktra(NodoOSPF nodoOSPF) {
        etiquetasAlmacenadas.remove(nodoOSPF);
        //        System.out.println("Etiquetas: " + etiquetasAlmacenadas);
        //        System.out.println(nodoOSPF.getDispositivo().getVecinos().toString());

        for (Dispositivo dispositivo : nodoOSPF.getDispositivo().getVecinos()) {
            boolean estaT = false;
            if (mismared) {
                if (dispositivo.getClass() == Router.class && dispositivo != finall && dispositivo != inicial) {
                    System.out.println("entro en el if de misma red.");
                    continue;
                }
            }
            for (NodoOSPF nodoT : t) {
                if (dispositivo == nodoT.getDispositivo()) {
                    estaT = true;
                    break;
                }
            }
            if (estaT) {
                continue;
            }

            //sección para obtener el peso de la linea
            Puerto port = new Puerto("puerto");
            Puerto portAnterior = new Puerto("puertoAnterior");
            int pesolinea = 0;
            for (Linea linea : Controlador.lineas) {
                if ((linea.getInicial() == dispositivo && linea.getFinall() == nodoOSPF.getDispositivo())) {
                    pesolinea = linea.getPeso() + nodoOSPF.costo;
                    port = linea.getpInicial();
                    portAnterior = linea.getpFinal();
                    break;
                }
                if (linea.getInicial() == nodoOSPF.getDispositivo() && linea.getFinall() == dispositivo) {
                    pesolinea = linea.getPeso() + nodoOSPF.costo;
                    port = linea.getpFinal();
                    portAnterior = linea.getpInicial();
                    break;
                }
            }

            //sección para determinar la referenciación de un nodo
            NodoOSPF nodo = new NodoOSPF(dispositivo, port, nodoOSPF.getDispositivo(), portAnterior, pesolinea, contadorIteracion);

            for (NodoOSPF nodoEtiqueta : etiquetasAlmacenadas) {
                // && sonVisibles(nodoOSPF.getDispositivo(), portAnterior, dispositivo, port)
                if (dispositivo == nodoEtiqueta.getDispositivo() && (nodoEtiqueta.costo > nodo.costo || nodoEtiqueta.costo == -1) && sonVisibles(nodoOSPF.getDispositivo(), portAnterior, dispositivo, port)) {
                    nodoEtiqueta.setCosto(nodo.costo);
                    nodoEtiqueta.setDispositivoAnterior(nodoOSPF.getDispositivo());
                    nodoEtiqueta.setPuerto(nodo.getPuerto());
                    nodoEtiqueta.setPuertoAnterior(nodo.getPuertoAnterior());
                    nodoEtiqueta.setIteracion(contadorIteracion);
                    break;
                }
            }

        }

        //encontrar la siguiente etiqueta menor
        if (etiquetasAlmacenadas.size() > 0) {
            boolean alcanzable = false;
            for (NodoOSPF nodoOSPF1 : etiquetasAlmacenadas) {
                if (nodoOSPF1.costo != -1) {
                    alcanzable = true;
                }
            }
            if (alcanzable) {
                NodoOSPF mayor = new NodoOSPF(null, null, null, null, 0, 0);
                for (NodoOSPF nodo : etiquetasAlmacenadas) {
                    if (nodo.getCosto() > mayor.getCosto()) {
                        mayor = nodo;
                    }
                }
                NodoOSPF menor = mayor;
                for (NodoOSPF nodo : etiquetasAlmacenadas) {
                    if (nodo.getCosto() < menor.getCosto() && nodo.getCosto() != -1) {
                        menor = nodo;
                    }
                }
                //                System.out.println("menor:" + menor.getDispositivo().getNombre());
                //                System.out.println("Costo menor:" + menor.getCosto());
                t.add(menor);


                //para salir de esto hay que determinar si todos los elementos que se encuentran etiquetados
                //no deben ser alcanzables
                algoritmoDijsktra(menor);
                contadorIteracion++;
            }

        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Método envio">
    /**
     * Metodo para realizar un evío de un host a otro.
     *
     * @param start Dispositivo
     * @param last Dispositivo
     * @param cantidadPaquetes int
     * @param dispositivos ArrayList<Dispositivo>
     * @return String Resultado del comando
     */
    public String envio(Nodo start, Nodo last, int cantidadPaquetes) {
        System.out.println("Inicial");
        System.out.println(start.getPuertos().get(0).getDireccion().getIp());
        System.out.println(start.getPuertos().get(0).getDireccion().getMascara());
        System.out.println(start.getPuertos().get(0).calcularRed());
        String redStart = start.getPuertos().get(0).calcularRed();
        System.out.println("Final");
        System.out.println(last.getPuertos().get(0).getDireccion().getIp());
        System.out.println(last.getPuertos().get(0).getDireccion().getMascara());
        System.out.println(last.getPuertos().get(0).calcularRed());
        String redLast = last.getPuertos().get(0).calcularRed();

        String salida = "";
        if (redStart.equals(redLast)) {
            salida = enrutamientoMismaRed(start, last);
        } else {
            salida = enrutamientoOtraRed(start, last);
        }

        System.out.println("salida: " + salida);

        String salidaFinal = "";

        if (salida.equals(Constantes.ERROR_NODOS_INALCANZABLES)) {
            salidaFinal = salida;
        } else if (salida.equals("desc")) {
            salidaFinal = Constantes.ERROR_NODOS_DESCONFIGURADOS;
        } else if (salida.equals(Constantes.ERROR_NO_PUERTA_ENLACE_INICIAL) || salida.equals(Constantes.ERROR_NO_PUERTA_ENLACE_FINAL)) {
            salidaFinal = salida;
        } else {
            salidaFinal = ("Ruta Seguida:\n");
            salidaFinal += (salida + "\n");
            String paquetesRecibidos = probabilidad.envioPaquetes(cantidadPaquetes);
            salidaFinal += (paquetesRecibidos);
        }


        return salidaFinal;
    }
    //</editor-fold>

    private String enrutamientoMismaRed(Nodo start, Nodo last) {
        System.out.println("se debe calcular la misma red");
        String salida = enrutamiento(start, last, true);
        System.out.println(salida);
        return salida;
    }

    private String enrutamientoOtraRed(Nodo start, Nodo last) {
        System.out.println("Se va a buscar la otra red");
        //Se debe enviar el paquete a la puerta de enlace. 
        Router enlaceStart = buscarPuertaEnlace(start);
        if (enlaceStart == null) {
            System.out.println(Constantes.ERROR_NO_PUERTA_ENLACE_INICIAL);
            return (Constantes.ERROR_NO_PUERTA_ENLACE_INICIAL);
        }
        Router enlaceLast = buscarPuertaEnlace(last);
        if (enlaceLast == null) {
            System.out.println(Constantes.ERROR_NO_PUERTA_ENLACE_FINAL);
            return (Constantes.ERROR_NO_PUERTA_ENLACE_FINAL);
        }

        String salida1;
        String salida2;
        String salida3;
        salida1 = enrutamiento(start, enlaceStart, true);
        System.out.println("salida 1" + salida1);
        salida2 = enrutamiento(enlaceStart, enlaceLast, false);
        System.out.println("salida2" + salida2);
        salida3 = enrutamiento(enlaceLast, last, true);
        System.out.println("salida3" + salida3);

        if (salida1.equals(Constantes.ERROR_NODOS_INALCANZABLES) || salida2.equals(Constantes.ERROR_NODOS_INALCANZABLES) || salida3.equals(Constantes.ERROR_NODOS_INALCANZABLES)) {
            return Constantes.ERROR_NODOS_INALCANZABLES;
        }
        return salida1 + "->" + salida2 + "->" + salida3;
    }

    //<editor-fold defaultstate="collapsed" desc="buscarPuertaEnlace">
    /**
     * Método para obtener la puerta de enlace de un nodo
     *
     * @param nodo
     * @return Router que tiene la puerta de enlace para el nodo
     */
    private Router buscarPuertaEnlace(Nodo nodo) {
        Router routerPuertaEnlace = null;
        for (Dispositivo dispositivo : todos) {
            if (dispositivo.getClass() == Router.class) {
                for (Puerto puerto : ((Router) dispositivo).getPuertos()) {
                    if (puerto.getDireccion().getIp().equals(nodo.getPuertaEnlace())) {
                        routerPuertaEnlace = (Router) dispositivo;
                    }
                }
            }
        }

        return routerPuertaEnlace;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="sonVisibles">
    /**
     * método para determinar si dos dispositivos son visibles entre si.
     *
     * @param dispositivoInicial Dispositivo
     * @param inicial Puerto
     * @param dispositivoFinal Dispositivo
     * @param finall Puerto
     * @return boolean con el resultado de la operación
     */
    private boolean sonVisibles(Dispositivo dispositivoInicial, Puerto inicial, Dispositivo dispositivoFinal, Puerto finall) {
        //        System.out.println(dispositivoInicial.getNombre());
        //        System.out.println(inicial.getNombre());
        //        System.out.println(dispositivoFinal.getNombre());
        //        System.out.println(finall.getNombre());

        boolean visible = true;
        if (dispositivoInicial.getClass() == Router.class || dispositivoInicial.getClass() == Nodo.class) {
            if (dispositivoFinal.getClass() == Router.class || dispositivoFinal.getClass() == Nodo.class) {
                if (inicial.getDireccion().getIp().equals("") || finall.getDireccion().getIp().equals("") || (!inicial.calcularRed().equals(finall.calcularRed()))) {

                    System.out.println(dispositivoInicial.getNombre());
                    System.out.println(dispositivoFinal.getNombre());
                    System.out.println("configurable - configurable");
                    visible = false;
                }
            }
            if (dispositivoFinal.getClass().equals(Switch.class)) {
                if (inicial.getDireccion().getIp().equals("")) {
                    System.out.println(dispositivoInicial.getNombre());
                    System.out.println(dispositivoFinal.getNombre());
                    System.out.println("configurable - no configurable");
                    visible = false;
                }
            }
        } else if (dispositivoInicial.getClass() == Switch.class) {
            if (dispositivoFinal.getClass() == Router.class || dispositivoFinal.getClass() == Nodo.class) {
                if (finall.getDireccion().getIp().equals("")) {
                    System.out.println(dispositivoInicial.getNombre());
                    System.out.println(dispositivoFinal.getNombre());
                    System.out.println("no configurable - configurable");
                    visible = false;
                }
            }
        } else {
            System.out.println("Error, es otro tipo de dispositivo");
        }

        return visible;
    }
    //</editor-fold>
}
