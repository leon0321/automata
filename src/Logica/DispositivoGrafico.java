/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Controlador.Controlador;
import Vista.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

/**
 *
 * @author Andres Betin
 */
public class DispositivoGrafico implements MouseMotionListener, MouseListener {

    private DispositivoGrafico actual = this;
    private Dispositivo dispositivo;
    //Imagen del dispositivo
    private JLabel imagen;
    //Frame que se utiliza para acceder a los elementos de la ventana
    private VentanaPrincipal frame;
    //Referencia al panel de trabajo
    JPanel panelTrabajo;
    /*
     * Se utiliza para el evento de la creación de linea entre dos nodos. Activa
     * la linea para su relación con otro nodo
     */
    public static boolean lineaActiva = false;

    public DispositivoGrafico(Dispositivo dispositivo, JLabel imagen, VentanaPrincipal frame, JPanel panelTrabajo) {
        this.dispositivo = dispositivo;
        this.imagen = imagen;
        this.imagen.addMouseListener(this);
        this.imagen.addMouseMotionListener(this);
        this.imagen.setToolTipText(dispositivo.toolTip());
        this.frame = frame;
        this.panelTrabajo = panelTrabajo;
    }

    //<editor-fold defaultstate="collapsed" desc="Get y Sets">
    public Dispositivo getDispositivo() {
        return dispositivo;
    }

    public void setDispositivo(Dispositivo dispositivo) {
        this.dispositivo = dispositivo;
    }

    public VentanaPrincipal getFrame() {
        return frame;
    }

    public void setFrame(VentanaPrincipal frame) {
        this.frame = frame;
    }

    public JLabel getImagen() {
        return imagen;
    }

    public void setImagen(JLabel imagen) {
        this.imagen = imagen;
    }

    public JPanel getPanelTrabajo() {
        return panelTrabajo;
    }

    public void setPanelTrabajo(JPanel panelTrabajo) {
        this.panelTrabajo = panelTrabajo;
    }
    //</editor-fold>

    public void pintarLineas() {
        //System.out.println("entro");
        for (Linea lineaMover : Controlador.lineas) {
            if (lineaMover.getInicial() == dispositivo) {
                lineaMover.setX1(imagen.getX() + imagen.getWidth() / 2);
                lineaMover.setY1(imagen.getY() + imagen.getHeight() / 2);
                getPanelTrabajo().repaint();
            } else if (lineaMover.getFinall() == dispositivo) {
                lineaMover.setX2(imagen.getX() + imagen.getWidth() / 2);
                lineaMover.setY2(imagen.getY() + imagen.getHeight() / 2);
                getPanelTrabajo().repaint();
            }
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Eventos del Mouse">
    @Override
    public void mouseDragged(MouseEvent me) {
        //System.out.println(" arrastrar");
        int pX = imagen.getX() + me.getX() - imagen.getWidth() / 2;
        int pY = imagen.getY() + me.getY() - imagen.getHeight() / 2;
        int rangoI = 0;
        int rangoD = 0;

        if (imagen.getWidth() > 48) {
            rangoI = -50;
            rangoD = panelTrabajo.getWidth() - 120;
        } else {
            rangoI = 10;
            rangoD = panelTrabajo.getWidth() - 50;
        }
        if (pX < rangoI) {
            return;
        } else if (pX > rangoD) {
            return;
        } else if (pY < 5) {
            return;
        } else if (pY > panelTrabajo.getHeight() - 70) {
            return;
        } else {
            imagen.setLocation(imagen.getX() + me.getX() - imagen.getWidth() / 2, imagen.getY() + me.getY() - imagen.getHeight() / 2);
            if (!Controlador.lineas.isEmpty()) {
                pintarLineas();
            }
        }

    }

    @Override
    public void mouseMoved(MouseEvent me) {
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        //evento del click derecho
        if (me.isMetaDown()) {
            JMenuItem configurar = new JMenuItem("Configuración");
            JMenuItem eliminar = new JMenuItem("Eliminar");
            JMenuItem terminal = new JMenuItem("Open Shell");
            JMenuItem propiedades = new JMenuItem("Propiedades");
            JPopupMenu pMenu = new JPopupMenu();

            if (!dispositivo.getVecinos().isEmpty()) {

                JMenuItem relaciones = new JMenuItem("Relaciones");
                relaciones.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        EliminarRelaciones relaciones = new EliminarRelaciones(getFrame(), true, actual);
                    }
                });
                pMenu.add(relaciones);
            }
            eliminar.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    Controlador controlador = new Controlador();
                    controlador.mostrarEnConsola(controlador.eliminarDispositivo(actual));
                }
            });
             terminal.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                   Terminal term = new Terminal(getFrame(), true,dispositivo);
                   term.setVisible(true);
                   term.setLocationRelativeTo(VentanaPrincipal.principal);
                }
            });
            configurar.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    if (dispositivo.getClass() == Nodo.class) {
                        ConfiguracionNodo configuracion = new ConfiguracionNodo(getFrame(), true, (Nodo) dispositivo);
                    } else if (dispositivo.getClass() == Router.class) {
                        ConfiguracionRouter configuracionRouter = new ConfiguracionRouter(getFrame(), true, (Router) dispositivo);
                    } else {
                        JOptionPane.showMessageDialog(null, "El dispositivo aun no se ha habilitado para su configuracion");
                    }
                }
            });
            propiedades.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent ae) {
                    Controlador controlador = new Controlador();
                    controlador.mostrarEnConsola(controlador.propiedadesDispositivo(actual));
                    Propiedades propiedades = new Propiedades(getFrame(), true, dispositivo);
                }
            });

            pMenu.add(eliminar);
            if (dispositivo.getClass() != Switch.class) {
                pMenu.add(terminal);
                pMenu.add(configurar);                
            }
            pMenu.add(propiedades);
            int contadorNodos = 0;
            for (Dispositivo dispositivo1 : Controlador.allDispositivos) {
                if (dispositivo1.getClass() == Nodo.class) {
                    contadorNodos++;
                }
            }
            if (contadorNodos >= 2 && dispositivo.getClass() == Nodo.class) {
                JMenuItem enviar = new JMenuItem("Enviar");
                enviar.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent ae) {

                        Comunicacion relaciones = new Comunicacion(getFrame(), false, dispositivo, Controlador.allDispositivos, panelTrabajo);

                    }
                });
                pMenu.add(enviar);
            }
            pMenu.show(me.getComponent(), me.getX(), me.getY());

        } else {
            if (Controlador.CREAR_LINEA) {

                popupPuertos(me);

//                if (lineaActiva) {
//                    if (this != Controlador.lineaInicial) {
//                        if (Controlador.existLinea(Controlador.lineaInicial.getDispositivo(), dispositivo)) {
//                            lineaActiva = false;
//                        } else {
//                            Controlador controlador = new Controlador();
//                            popupPuertos(me);
//                            controlador.crearRelacion(Controlador.lineaInicial, this);//crea la linea (relacion)
//                            Controlador.CREAR_LINEA = false;
//                        }
//                    } else {
//                        lineaActiva = false;
//                    }
//                } else {
//                    Controlador.lineaInicial = this;
//                    lineaActiva = true;
//                    popupPuertos(me);
//                }
            }
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Métodos del mouse que no se estan usando">
    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        //        mouseDragged(me);
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="mostrarNombre">
    public void mostrarNombre() {

        String nombre = getDispositivo().getNombre();

        int w = nombre.length() * 15; //

        if (w < 48) {
            getImagen().setSize(48, 65);
        } else {
            getImagen().setSize(w, 65);
        }
        getImagen().setText(nombre);
        getImagen().repaint();
        pintarLineas();
        getPanelTrabajo().repaint();
    }
    //</editor-fold>

    public void modificarDisponibilidadPuerto(Dispositivo dispositivo, int i) {
        dispositivo.getPuertos().get(i).setDisponible(false);

    }

    public void popupPuertos(MouseEvent me) {

        JMenuItem puerto = null;
        JPopupMenu p = new JPopupMenu();
        boolean unoDisponible = false;
        for (int i = 0; i < getDispositivo().getPuertos().size(); i++) {
            if (getDispositivo().getPuertos().get(i).isDisponible()) {
                unoDisponible = true;
                puerto = new JMenuItem("Puerto " + (i));
                puerto.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int indicePuerto = Integer.parseInt("" + e.getActionCommand().charAt(e.getActionCommand().length() - 1));

                        if (Controlador.lineaIntermedia.getInicial() != null && Controlador.lineaIntermedia.getInicial() != actual.getDispositivo()) {
                            Controlador.indicePuertoFinal = indicePuerto;
                            Controlador.lineaIntermedia.setFinall(actual.getDispositivo());
                            Controlador.lineaIntermedia.setpFinal(actual.getDispositivo().getPuertos().get(indicePuerto));
                            Controlador.lineaIntermedia.setpInicial(Controlador.lineaIntermedia.getInicial().getPuertos().get(Controlador.indicePuertoInicial));
                            for (DispositivoGrafico dg : Controlador.graficos) {
                                if (dg.getDispositivo() == Controlador.lineaIntermedia.getInicial()) {
                                    Controlador.crearRelacion(dg, actual);
                                    break;
                                }
                            }
                            System.out.println("Dispositivo Final");
                        } else {
                            System.out.println("Dispositivo Inicial");

                            Controlador.lineaIntermedia.setInicial(actual.getDispositivo());
                            Controlador.indicePuertoInicial = indicePuerto;
                            Controlador.lineaIntermedia.setX1(actual.getImagen().getX() + actual.getImagen().getWidth() / 2);
                            Controlador.lineaIntermedia.setY1(actual.getImagen().getY() + actual.getImagen().getHeight() / 2);
                            Controlador.CREAR_LINEA_GRAFICA = true;
                        }
                    }
                });
                p.add(puerto);
            }
            if (!unoDisponible && i == getDispositivo().getPuertos().size() - 1) {
                JOptionPane.showMessageDialog(null, "No hay puertos disponibles");
                Controlador.CREAR_LINEA = false;
                Controlador.CREAR_LINEA_GRAFICA = false;
            }
        }
        p.show(me.getComponent(), me.getX(), me.getY());

    }

    @Override
    public String toString() {
        return dispositivo.toString(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
