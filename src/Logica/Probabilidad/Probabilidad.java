/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.Probabilidad;

import Logica.Probabilidad.generadores.CongruencialMixto;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *hjvgfjvjgfhjghjgjgjghjghhjghjghjk
 * @author Andres David
 */
public class Probabilidad {

    //Elementos para la generacion de numeros aleatorios
    private final int AGENERADOR = 16823;
    private final int CGENERADOR = 21;
    private final int MGENERADOR = 32749;
    private final int CANT_GENERADOR = 32749;
    //arraylist donde se encuentran almacenados los numeros aleatorios generados
    public static ArrayList<Double> reales;
    //iterador de las posiciones de los numeros aleatorios
    public static int posicionReales = 0;
    //Generador de Numeros
    private CongruencialMixto congruencialMixto;

    public Probabilidad() {
        //Se generan los numeros
        congruencialMixto = new CongruencialMixto();
        congruencialMixto.setA(AGENERADOR);
        congruencialMixto.setC(CGENERADOR);
        congruencialMixto.setM(MGENERADOR);
        congruencialMixto.setCantidad(CANT_GENERADOR);
        congruencialMixto.setXo(new GregorianCalendar().get(Calendar.SECOND) % congruencialMixto.getM());
        congruencialMixto.generarNumeros();
        if (reales == null || posicionReales == reales.size() - 1) {
            reales = congruencialMixto.getReales();
        }

    }

    //TODO Cambiar opcion de probabilidad.
    //<editor-fold defaultstate="collapsed" desc="Método envioPaquetes">
    /**
     * Metodo para calcular la cantidad de paquetes recibidos a partir de una
     * cantidad dada por medio de la probabilidad
     *
     * @param cantidad
     * @return int, cantidad de paquetes recibidos
     */
    public String envioPaquetes(int cantidad) {
        String salida = "Inicializacion";
        double aleatorio = reales.get(posicionReales++);

        double probabilidad = new Double(1) / new Double(cantidad + 1);
        System.out.println("Probabilidad: " + probabilidad);

        double start = 0;
        double end = probabilidad;

        for (int i = 0; i < cantidad + 1; i++) {
            //            System.out.println(i);
            //            System.out.println("aleatorio=" + aleatorio);
            //            System.out.println("Start: " + start);
            //            System.out.println("end: " + end);
            if (aleatorio >= start && aleatorio < end) {
                salida = "<br>El numero de paquetes recibidos fue: " + i + " de " + cantidad + " enviados<br>";
                break;
            }
            start = end;
            end += probabilidad;
        }

        return salida;
    }
    //</editor-fold>
}
