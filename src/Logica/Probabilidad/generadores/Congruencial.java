/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.Probabilidad.generadores;

import java.util.ArrayList;

/**
 *
 * @author jhonny
 */
public class Congruencial {

    private int xo;
    private int a;
    private int m;
    private int cantidad;
    private ArrayList<Integer> enteros;
    private ArrayList<Double> reales;

    public Congruencial() {

        enteros = new ArrayList<Integer>();
        reales = new ArrayList<Double>();
        this.xo = 0;
        this.a = 0;

        this.m = 0;
    }

    public ArrayList<Integer> getEnteros() {
        return enteros;
    }

    public void setEnteros(ArrayList<Integer> enteros) {
        this.enteros = enteros;
    }

    public ArrayList<Double> getReales() {
        return reales;
    }

    public void setReales(ArrayList<Double> reales) {
        this.reales = reales;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getXo() {
        return xo;
    }

    public void setXo(int xo) {
        this.xo = xo;
    }
}