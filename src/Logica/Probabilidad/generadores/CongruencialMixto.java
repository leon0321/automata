/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.Probabilidad.generadores;

import javax.swing.JOptionPane;

/**
 *
 * @author
 */
public class CongruencialMixto extends Congruencial {

    private int c;

    public CongruencialMixto() {
        this.c = 0;
    }

    public CongruencialMixto(int a, int x0, int c, int m, int cant){
        super.setA(a);
        super.setXo(x0);
        super.setM(m);
        super.setCantidad(cant);
        this.c = c;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public void generarNumeros(){

        int anterior = getXo();
        int numero = 0;
        double real = 0;
//        Salida += getXo();
        for (int i = 0; i < getCantidad(); i++) {
            numero = (anterior * getA() + getC()) % getM();
            real = (float) numero / getM();

//            SalidaReal += "\"" + numero + "\"" + ";" + "\"" + real + "\"" + "\r\n";
            getEnteros().add(numero);
            getReales().add(real);

            anterior = numero;
        }
    }

    public boolean  validarXO() {
        boolean result = false;

        if (getXo() > 0) {
            System.out.print("");
            result = true;
        } else {
            JOptionPane.showMessageDialog(null, "Xo tiene que ser mayor que 0");
        }
        return result;
    }

    public boolean  validarA() {
        boolean result = false;
        if (getA() > 0 && getA() % 2 != 0 && getA() % 3 != 0 && getA() % 5 != 0) {
            System.out.print("");
            result = true;
        } else {
            if (getA() <= 0) {
                JOptionPane.showMessageDialog(null, "a tiene que ser mayor que 0");
            } else if (getA() % 2 == 0) {
                JOptionPane.showMessageDialog(null, "a tiene que ser impar");
            } else if (getA() % 3 == 0) {
                JOptionPane.showMessageDialog(null, "a no se puede dividir entre 3");
            } else if (getA() % 5 == 0) {
                JOptionPane.showMessageDialog(null, "a no se puede dividir entre 5");
            } else if (getA() % 3 == 0) {
                JOptionPane.showMessageDialog(null, "a no se puede dividir entre 3");
            } else {
                JOptionPane.showMessageDialog(null,  "a no se puede utilizar");
            }
        }
        return result;
    }

    public boolean  validarC() {
        boolean result = false;
        if (c > 0 && c % 8 == 5) {
            System.out.print("");
            result = true;
        } else {
            if (c <= 0) {
                JOptionPane.showMessageDialog(null, "c tiene que ser mayor que 0");
            } else if (c % 8 != 5) {
                JOptionPane.showMessageDialog(null, "al dividir c entre 8 el modulo debe ser 5");
            } else {
                JOptionPane.showMessageDialog(null, "c no se puede utilizar");
            }
        }

        return result;
    }

    public boolean  validarM() {
        boolean result = false;
        if (getM() > getXo() && getM() > getA() && getM() > c && isPrimo(getM()) && getM() < Math.pow(2, 32)) {
            System.out.print("");
            result = true;
        } else {
            if (getM() <= getXo()) {
                JOptionPane.showMessageDialog(null, "m tiene que ser mayor que xo");
            } else if (getM() <= getA()) {
                JOptionPane.showMessageDialog(null, "m tiene que ser mayor que a");
            } else if (getM() <= getC()) {
                JOptionPane.showMessageDialog(null, "m tiene que ser mayor que c");
            } else if (!isPrimo(getM())) {
                JOptionPane.showMessageDialog(null, "m tiene que ser primo");
            } else if (!(getM() < Math.pow(2, 32))) {
                JOptionPane.showMessageDialog(null, "m tiene que ser menor que 2 a la 32");
            } else {
                JOptionPane.showMessageDialog(null, "m no se puede utilizar");
            }
        }
        return result;
    }

    public boolean  validarNumRep() {
        boolean result = false;
        if (getCantidad() > 0) {
            System.out.print("");
            result = true;
        } else {
            JOptionPane.showMessageDialog(null, "tiene que ser mayor que 0");
        }
        return result;
    }

    public boolean isPrimo(int numero) {
        boolean primo = true;

        for (int i = 1; i <= numero / 2; i++) {
            if (numero % i == 0 && i != 1) {
                primo = false;
                System.out.println("entro en el if y el i es: " + i);
                break;
            }
        }

        return primo;
    }
}