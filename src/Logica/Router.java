package Logica;

import Controlador.Controlador;
import Logica.ProtocoloBGP.ConfigBGP;
import Logica.ProtocoloRipV2.TablaRipV2;
import Logica.RIP.TablaRIP;
import java.util.ArrayList;

/**
 * <p align="justify">Esta clase es una Abstracción del <b>Router</b>,
 * dispositivo que proporciona conectividad a nivel de red o nivel tres en el
 * modelo OSI.</p><p align="justify" >Su función principal consiste en enviar o
 * encaminar paquetes de datos de una red a otra, es decir, interconectar
 * subredes, entendiendo por subred un conjunto de máquinas IP que se pueden
 * comunicar sin la intervención de un router (mediante bridges), y que por
 * tanto tienen prefijos de red distintos.</p><p align="justify" >Esta clase no
 * abstrae todas las características de un router real, solo las necesarias para
 * el contexto y funcionamiento de esta aplicación</p><p align="justify">Las
 * características que se consideran en esta aplicación son:<li>Cuatro puertos
 * para conexión con otros dispositivos</l>
 * <li>Tabla de enrutamiento para cada protocolo</l>
 * <li>Atributos que se encargan de la manejo de protocolos distintos</l>
 * <li>Soporta los protocolos: <ul>BGP, OSPF, RIP Versión 1, RIP Versión 2 y
 * Flooding</u></l></p>
 *
 * @author ACER && Leonardo Ortega Hernandez
 */
public class Router extends Dispositivo {

    private static int contRouters = 0;
    private ConfigBGP configBGP;
    private TablaRIP tabla = new TablaRIP();
    private TablaRipV2 tablarip2;
    private ArrayList<String> ipconet;
    private ArrayList<String> maskconet;
    private ArrayList<String> iprip;
    private ArrayList<String> maskrip;

    public Router(int x, int y) {
        super();
        super.setNombre("Router" + contRouters++);
        super.setX(x);
        super.setY(y);
        ArrayList<Puerto> puertos = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            Puerto puerto = new Puerto("Puerto" + i);
            puertos.add(puerto);
        }
        super.setPuertos(puertos);
        configBGP = new ConfigBGP(this);
        tablarip2 = new TablaRipV2();
        ipconet = new ArrayList<>();
        maskconet = new ArrayList<>();
        iprip = new ArrayList<>();
        maskrip = new ArrayList<>();
    }

    public Router() {
        super();
        ArrayList<Puerto> puertos = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            Puerto puerto = new Puerto("Puerto" + i);
            puertos.add(puerto);
        }
        super.setPuertos(puertos);
        configBGP = new ConfigBGP(this);
        tablarip2 = new TablaRipV2();
    }

    @Override
    public String Propiedades() {

        String a = "";

        a = "<b>Nombre de dispositivo:</b> " + getNombre();
        for (int i = 0; i < getPuertos().size(); i++) {
            a += "<br><b>Puerto " + i + ": </b>";
            for (Linea linea : Controlador.lineas) {
                if (linea.getpFinal() == getPuertos().get(i)) {
                    a += linea.getInicial().getNombre();
                    break;
                }
                if (linea.getpInicial() == getPuertos().get(i)) {
                    a += linea.getFinall().getNombre();
                    break;
                }
            }
            a += "<br><b>Gateway: </b>" + getPuertos().get(i).getDireccion().getIp();
            a += "<br><b>Máscara de red: </b> " + getPuertos().get(i).getDireccion().getMascara();
            a += "<br>";
        }

        return a;
    }

    @Override
    public String toolTip() {
        String a = "";

        a = "<html><b>Nombre de dispositivo: </b> " + getNombre();
        for (int i = 0; i < getPuertos().size(); i++) {
            a += "<br><b>Puerto " + i + ": </b>";
            for (Linea linea : Controlador.lineas) {
                if (linea.getpFinal() == getPuertos().get(i)) {
                    a += linea.getInicial().getNombre();
                    break;
                }
                if (linea.getpInicial() == getPuertos().get(i)) {
                    a += linea.getFinall().getNombre();
                    break;
                }
            }
            a += "<br><b>Gateway: </b>" + getPuertos().get(i).getDireccion().getIp();
            a += "<br><b>Máscara de red: </b>" + getPuertos().get(i).getDireccion().getMascara() + "<br>";

        }
        a += "</html>";

        return a;
    }

    @Override
    public String toString() {
        return this.getNombre();
    }

    public ConfigBGP getConfigBGP() {
        return configBGP;
    }

    public void setTablaRIP(TablaRIP rip) {
        tabla = rip;
    }

    public TablaRIP getTabla() {
        return tabla;
    }

    public ArrayList<String> getIpconet() {
        return ipconet;
    }

    public ArrayList<String> getMaskconet() {
        return maskconet;
    }

    public ArrayList<String> getIprip() {
        return iprip;
    }

    public ArrayList<String> getMaskrip() {
        return maskrip;
    }

    public void setTablarip2(ArrayList<String> ipconet, ArrayList<String> iprip, ArrayList<String> maskconet, ArrayList<String> maskrip) {
        this.ipconet = ipconet;
        this.iprip = iprip;
        this.maskconet = maskconet;
        this.maskrip = maskrip;
    }
}
