/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.tools;

import Controlador.Controlador;
import Logica.Dispositivo;
import Logica.DispositivoGrafico;
import Logica.Linea;
import Logica.Nodo;
import Logica.ProtocoloBGP.Rute;
import Logica.Puerto;
import Logica.Router;
import Logica.Switch;
import java.util.ArrayList;

/**
 * Esta clase se encarga de la validacion de los enlaces entre los puertos entre
 * los dipocitivoscontiene metodos que determinan si un enlace entre puertos
 * esta configurado correctamente, entre otros
 *
 * @author Leonardo ortega hernandez <leon9894@gmail.com>
 */
public class Enlace {

    private static Puerto pa;
    private static Puerto pb;
    private static ArrayList<Puerto> puertos = new ArrayList<>();
    private static ArrayList<Dispositivo> dispositivos = new ArrayList<>();

    /**
     * determina si el enlace entre dos dispocitivos esta configurado
     * correctamente
     *
     * @param a dispocitivo inicial
     * @param b dispocitivo final
     * @return Retorna false si la configuracion esta mala o no existe y true si
     * estan configurados adecuadamente
     */
    public static boolean isNice(Dispositivo a, Dispositivo b) {

        Linea l = find(a, b);
        if (a.getClass() == Switch.class && b.getClass() == Switch.class) {//si son dos switchs retorna true
            return true;
        }
        if (l.getInicial().getClass() == Switch.class && !(l.getFinall().getClass() == Switch.class)) {
            if (!puertos.contains(l.getpFinal())) {
                System.err.println(l.getpFinal().getDireccion().getIp() + "--" + l.getFinall());
                puertos.add(l.getpFinal());
                dispositivos.add(l.getFinall());
            }
            return isNice(l.getFinall(), l);
        }
        if (!(l.getInicial().getClass() == Switch.class) && (l.getFinall().getClass() == Switch.class)) {
            if (!puertos.contains(l.getpInicial())) {
                System.err.println(l.getpInicial().getDireccion().getIp() + "--" + l.getInicial());
                puertos.add(l.getpInicial());
                dispositivos.add(l.getInicial());
            }
            return isNice(l.getInicial(), l);
        }
        if (!(a.getClass() == Switch.class) && !(b.getClass() == Switch.class)) {
            if (!puertos.contains(l.getpInicial())) {
                puertos.add(l.getpInicial());
                dispositivos.add(l.getInicial());
            }
            if (!puertos.contains(l.getpFinal())) {
                puertos.add(l.getpFinal());
                dispositivos.add(l.getFinall());
            }
            return (isNice(l.getInicial(), l) && isNice(l.getFinall(), l));
        }
        return false;
    }

    /**
     * determina si hay una configuracion entre el puerto de la linea y el nodo
     * en ese mismo extremo de la linea
     *
     * @param a dispocitivo a verificar
     * @param l linea a conprovar si esta configurada con el dispocitivo
     * @return Retorna false si no exite configuracion y true en caso contrario
     */
    public static boolean isNice(Dispositivo a, Linea l) {
        if (a == l.getInicial() && !l.getpInicial().getDireccion().getIp().equalsIgnoreCase("")) {
            return true;
        }
        if (a == l.getFinall() && !l.getpFinal().getDireccion().getIp().equalsIgnoreCase("")) {
            return true;
        }
        return false;
    }

    /**
     * Encuentra una linea entre dos dispocitivos
     *
     * @param a dispocitivo inicial
     * @param b dispocitivo final
     * @return Retorna la linea que encontro entre los dos dipocitivos si no
     * exite dicha linea retorna null
     */
    public static Linea find(Dispositivo a, Dispositivo b) {
        for (int i = 0; i < Controlador.lineas.size(); i++) {
            Linea linea = Controlador.lineas.get(i);
            if (linea.getInicial() == a && linea.getFinall() == b) {
                return linea;
            }
            if (linea.getInicial() == b && linea.getFinall() == a) {
                return linea;
            }
        }
        return null;
    }

    /**
     * Verfica el estado de todos los enlaces entre los dipositivos que
     * conforman un camino
     *
     * @param rute ruata que sera verificada
     * @return Retorna false si alguno de los enlaces no esta configurado
     * correctamente en alguna parte del camino y true en caso contrario
     */
    public static boolean isNice(Rute rute) {
        puertos = new ArrayList<>();
        dispositivos = new ArrayList<>();
        for (int i = 0; i < rute.size() - 1; i++) {
            Dispositivo a = rute.get(i);
            Dispositivo b = rute.get(i + 1);
            if (!isNice(a, b)) {
                return false;
            }
        }
        boolean nice = isNice(puertos);
        return nice;
    }

    /**
     * De un arraylist de puerto de un camino verrifica el estado de la
     * configuracion. es decir que las tramas de red concuerden en la
     * configuracion de los puertos
     *
     * @param puertos Arraylis con los puertos que conforma un camino(No incluye
     * los puertos de los Swichts)
     * @return Retorna true si todos los puerto esta bien configurados y false
     * si alguno de ellos no lo esta
     */
    public static boolean isNice(ArrayList<Puerto> puertos1) {
        System.err.println("Is Nice------------------------------------------");
        for (int i = 0; i < puertos1.size(); i++) {
            System.err.println("ip: " + puertos1.get(i).getDireccion().getIp() + "\tD: " + dispositivos.get(i).getNombre());
        }

        System.err.println("1------------------------------------------------");
        for (int i = 0; i < puertos1.size() - 1; i += 2) {
            Dispositivo d1 = dispositivos.get(i);
            Dispositivo d2 = dispositivos.get(i + 1);
            Puerto a = puertos.get(i);
            Puerto b = puertos.get(i + 1);

            if (d1.getClass() == Nodo.class && d2.getClass() == Nodo.class) {
                String ip0 = a.calcularRed();
                String ip1 = b.calcularRed();
                System.err.println(ip0 + " : " + ip1 + "    ");
                if (!ip0.equalsIgnoreCase(ip1)) {
                    System.err.print(ip0 + " : " + ip1);
                    return false;
                }
            }

            System.err.println("2------------------------------------------------");
            if ((d1.getClass() == Nodo.class && d2.getClass() == Router.class)) {
                Nodo dp1 = (Nodo) d1;
                System.err.println(dp1.getPuertaEnlace());
                System.err.println(b.getDireccion().getIp());
                boolean sw = dp1.getPuertaEnlace().equalsIgnoreCase(b.getDireccion().getIp());
                if (!sw) {
                    return false;
                }
            }
            System.err.println("3------------------------------------------------");
            if ((d1.getClass() == Router.class && d2.getClass() == Nodo.class)) {
                Nodo dp2 = (Nodo) d2;
                boolean sw = dp2.getPuertaEnlace().equalsIgnoreCase(a.getDireccion().getIp());
                System.err.println(dp2.getPuertaEnlace());
                System.err.println(a.getDireccion().getIp());
                if (!sw) {
                    return false;
                }
            }
            System.err.println("4------------------------------------------------");
            String ip0 = a.calcularRed();
            String ip1 = b.calcularRed();
            System.err.println(ip0 + " : " + ip1 + "    ");
            if (!ip0.equalsIgnoreCase(ip1)) {
                System.err.print(ip0 + " : " + ip1);
                return false;
            }
        }
        return true;
    }

    /**
     * Toma una ruta y genera una lista de dispositivos graficos
     *
     * @param rute ruta a convertir en lista de dispositivos graficos
     * @return Retorna una lista de dispositivos graficos
     * @see Rute camino a base para genera el camino grafico
     * @see DispositivoGrafico
     */
    public static ArrayList<DispositivoGrafico> caminoGrafico(Rute rute) {
        ArrayList<DispositivoGrafico> dgs = new ArrayList<>();
        for (int i = 0; i < rute.size(); i++) {
            for (int j = 0; j < Controlador.graficos.size(); j++) {
                Dispositivo dg = Controlador.graficos.get(j).getDispositivo();
                Dispositivo d = rute.get(i);
                if (dg == d && !dgs.contains(Controlador.graficos.get(j))) {
                    dgs.add(Controlador.graficos.get(j));
                }
            }
        }
        return dgs;
    }

    /**
     * Toma una lista de Rutes y genera una lista de listas de caminos graficos
     *
     * @param list lista de rutas
     * @return Retorna una lista de listas de dispocitivos
     */
    public static ArrayList caminosGraficos(ArrayList list) {
        ArrayList dispo = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            Rute rute = (Rute) list.get(i);
            dispo.add(Enlace.caminoGrafico(rute));
        }
        return dispo;
    }
}
