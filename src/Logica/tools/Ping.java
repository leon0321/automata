/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.tools;

import Controlador.Controlador;
import Logica.Constantes.Constantes;
import Logica.Dispositivo;
import Logica.DispositivoGrafico;
import Logica.Nodo;
import Logica.ProtocoloBGP.BGP;
import Logica.ProtocoloOSPF.OSPF;
import Logica.ProtocoloRipV2.RipV2;
import Logica.RIP.RIP;
import Logica.Router;
import Logica.flooding.Flooding;
import java.util.ArrayList;

/**
 * Clase que se encarga de la ejecución del comando ping que envía paquetes para
 * la comprobación de una conexión entre dos dispositivos
 *
 * @author Leonardo ortega hernandez <leon9894@gmail.com>
 */
public class Ping {

    Nodo origen;//nodo desde el cual se ejecuta el ping
    Nodo destino;//nodo al cual se envia el ping
    String ipDestino;//dirrecion de el nodo destino
    int cantidadPaqutes = 0;//cantidad de paquetes a enviar

    /**
     * Este constructor se utiliza para generar un ping con la dirección de ip
     * del nodo destino y el nodo inicial
     *
     * @param origen nodo desde el que se ejecuta el ping
     * @param ip destino Dirección IP del nodo final
     * @param paquetes Cantidad de paquetes a enviar en el ping
     */
    public Ping(Nodo origen, String ipDestino, int paquetes) {
        this.origen = origen;
        this.ipDestino = ipDestino;
        this.destino = getDestino(ipDestino);
        this.cantidadPaqutes = paquetes;
    }

    /**
     * Este constructor se utiliza para generar un ping con el nodo destino y el
     * nodo inicial
     *
     * @param origen nodo desde el que se ejecuta el ping
     * @param destino nodo destino del ping
     * @param paquetes Cantidad de paquetes a enviar en el ping
     */
    public Ping(Nodo origen, Nodo destino, int paquetes) {
        this.origen = origen;
        this.destino = destino;
        this.cantidadPaqutes = paquetes;
    }

    /**
     * Inicia la ejecución del proceso de comunicación del comando ping
     *
     * @param protocolo identificador del protocolo sobre el cual se ejuta el
     * comando ping
     * @return Retorna un lista de dispositivos gráficos que conforman el camino
     * y si el camino no existe retorna null
     */
    public ArrayList enviar(int protocolo) {
        if (destino == null) {
            Controlador.salida = "Nodo destino es inalcanzable";
            return null;
        }
        ArrayList best = null;
        if (protocolo == Constantes.OSPF) {
            best = OSPF(origen, destino, cantidadPaqutes);
        } else if (protocolo == Constantes.BGP) {
            best = BGP(origen, destino, cantidadPaqutes);
        } else if (protocolo == Constantes.RIP1) {
            best = RIPv1(origen, destino, cantidadPaqutes);
        } else if (protocolo == Constantes.RIP2) {
            best = RipV2(origen, destino, cantidadPaqutes);
        } else if (protocolo == Constantes.FLOODING) {
            best = Flooding(origen, destino, cantidadPaqutes);
        } else if (protocolo == Constantes.VD) {
            Controlador.salida = "Protocolo Vector Distancia en construccion";
        }
        return best;
    }

    /**
     * Con base en una ip busca el nodo con dicha ip entre todos los
     * dispositivos nodos de la red
     *
     * @param ip dirección ip del nodo que se busca
     * @return Retorna el nodo encontrado, si no se encontró un nodo con la
     * dirrecion ip retorna null
     */
    public static Nodo getDestino(String ip) {
        for (int i = 0; i < Controlador.allDispositivos.size(); i++) {
            if (Controlador.allDispositivos.get(i).getClass() == Nodo.class) {
                Nodo destino = (Nodo) Controlador.allDispositivos.get(i);
                if (destino.getPuertos().get(0).getDireccion().getIp().equalsIgnoreCase(ip)) {
                    return destino;
                }
            }
        }
        return null;
    }

    /**
     * Ejecuta el envío de paquetes con el Protocolo OSPF
     *
     * @param origen nodo desde el cual se origina el envío de paquetes
     * @param destino nodo destino para el envió de paquetes
     * @param cantidadPaquetes cantidad de paquetes a enviar
     * @return Retorna un lista de dispositivos que conforma la ruta hallada
     * entre el nodo destino y el origen, si no existe dicha ruta retorna null
     */
    public static ArrayList<DispositivoGrafico> OSPF(Dispositivo origen, Dispositivo destino, int cantidadPaquetes) {
        Controlador controlador = new Controlador();
        OSPF com = new OSPF(Controlador.allDispositivos);
        String salida = com.envio((Nodo) origen, (Nodo) destino, cantidadPaquetes);
        Controlador.salida = salida;
        if (salida.equals(Constantes.ERROR_NODOS_INALCANZABLES)) {
            controlador.mostrarEnConsola(salida);
        } else if (salida.equals(Constantes.ERROR_NO_PUERTA_ENLACE_INICIAL) || salida.equals(Constantes.ERROR_NO_PUERTA_ENLACE_FINAL)) {
            controlador.mostrarEnConsola(salida);
        } else if (!salida.equals(Constantes.ERROR_NODOS_DESCONFIGURADOS)) {
            return (com.getCamino());
        }
        return null;
    }

    /**
     * Ejecuta el envío de paquetes con el Protocolo BGP
     *
     * @param origen nodo desde el cual se origina el envío de paquetes
     * @param destino nodo destino para el envió de paquetes
     * @param cantidadPaquetes cantidad de paquetes a enviar
     * @return Retorna un lista de dispositivos que conforma la ruta hallada
     * entre el nodo destino y el origen, si no existe dicha ruta retorna null
     */
    public static ArrayList<DispositivoGrafico> BGP(Dispositivo origen, Dispositivo destino, int cantidadPaquetes) {
        return BGP.best(origen, destino, cantidadPaquetes);
    }

    /**
     * Ejecuta el envío de paquetes con el Protocolo RIP version 1
     *
     * @param origen nodo desde el cual se origina el envío de paquetes
     * @param destino nodo destino para el envió de paquetes
     * @param cantidadPaquetes cantidad de paquetes a enviar
     * @return Retorna un lista de dispositivos que conforma la ruta hallada
     * entre el nodo destino y el origen, si no existe dicha ruta retorna null
     */
    public static ArrayList<DispositivoGrafico> RIPv1(Dispositivo origen, Dispositivo destino, int cantidadPaquetes) {
        ArrayList<DispositivoGrafico> best = null;
        try {
            best = RIP.enviar(BGP.createPaths(origen, destino), origen);
        } catch (java.lang.IndexOutOfBoundsException e) {
            Controlador.salida = Constantes.ERROR_RIP;
        }
        return best;
    }

    /**
     * Ejecuta el envío de paquetes con el Protocolo RIP version 2
     *
     * @param origen nodo desde el cual se origina el envío de paquetes
     * @param destino nodo destino para el envió de paquetes
     * @param cantidadPaquetes cantidad de paquetes a enviar
     * @return Retorna un lista de dispositivos que conforma la ruta hallada
     * entre el nodo destino y el origen, si no existe dicha ruta retorna null
     */
    public static ArrayList<DispositivoGrafico> RipV2(Dispositivo origen, Dispositivo destino, int cantidadPaquetes) {
            return RipV2.best(origen, destino, cantidadPaquetes);
    }

    /**
     * Ejecuta el envío de paquetes con el Protocolo Flooding
     *
     * @param origen nodo desde el cual se origina el envío de paquetes
     * @param destino nodo destino para el envió de paquetes
     * @param cantidadPaquetes cantidad de paquetes a enviar
     * @return Retorna un lista de dispositivos que conforma la ruta hallada
     * entre el nodo destino y el origen, si no existe dicha ruta retorna null
     */
    public ArrayList Flooding(Dispositivo origen, Dispositivo destino, int cantidadPaquetes) {
        System.out.println("hola estoy en flooding -----------------------------------------------------");
        ArrayList rutas = Flooding.enviarVecinos(origen, destino, Controlador.allDispositivos);
        Controlador c = new Controlador();
        if (rutas.isEmpty()) {
            Controlador.salida = "No existen nigun camino configurado para llegar al destino";
            return null;
        }
        String salidad = "Todo bien<br>";
        for (int i = 0; i < rutas.size(); i++) {
            salidad += rutas.get(i) + "<br>";
        }
        Controlador.salida = salidad;
        return Enlace.caminosGraficos(rutas);
    }
}
