/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Controlador.Controlador;
import Logica.Constantes.Constantes;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Jorge
 */
public abstract class Dispositivo implements Serializable {

    //puertos del dispositivo
    private ArrayList<Puerto> puertos;
    //nombre del dispositivo
    private String nombre;
    //Lista de Dispositivos vecinos 
    private ArrayList<Dispositivo> vecinos;
    //Posicion del dispositivo
    int y;
    int x;
    
    public Dispositivo() {
        vecinos = new ArrayList<>();
        puertos = new ArrayList<>();

//        for (Puerto puerto : puertos) {
//            puerto = new Puerto();
//        }
    }

    //<editor-fold defaultstate="collapsed" desc="Gets y Sets">
    public int getX() {
        return x;
    }
    
    public void setX(int x) {
        this.x = x;
    }
    
    public int getY() {
        return y;
    }
    
    public void setY(int y) {
        this.y = y;
    }
    
    public ArrayList<Dispositivo> getVecinos() {
        return vecinos;
    }
    
    public void setVecinos(ArrayList<Dispositivo> vecinos) {
        this.vecinos = vecinos;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public ArrayList<Puerto> getPuertos() {
        return puertos;
    }
    
    public void setPuertos(ArrayList<Puerto> puertos) {
        this.puertos = puertos;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Metodos Abstractos">
    /**
     * Metodo para obtener las propiedades de un dispositivo
     *
     * @return String con las propiedades del dispositivo
     */
    public abstract String Propiedades();

    /**
     * Metodo para establecer el tooltip de un dispositivo
     *
     * @return String tooltip
     */
    public abstract String toolTip();
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="validarNombreRepetido">
    /**
     * Mètodo para validar si existe el nombre de un dispositivo
     *
     * @param dispositivoNuevo
     * @param dispositivoActual
     * @return true o false en caso de que el nombre ya exista
     */
    protected boolean validarNombreRepetido(Dispositivo dispositivoNuevo, Dispositivo dispositivoActual) {
        boolean retorno = false;
        String nombreDispositivo = dispositivoNuevo.getNombre();
        
        for (Dispositivo dispositivo1 : Controlador.allDispositivos) {
            if (nombreDispositivo.equals(dispositivo1.getNombre()) && dispositivo1 != dispositivoActual) {
                retorno = true;
                break;
            }
        }
        return retorno;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="configurar">
    /**
     * Configurar Dispositivo
     *
     * @param dispositivo
     * @return
     */
    public String configurar(Dispositivo dispositivo) {
        
        boolean repetido = this.validarNombreRepetido(dispositivo, this);
        if (!repetido) {
            if (this.getClass() == Nodo.class) {
                ((Nodo) this).setPuertaEnlace(((Nodo) dispositivo).getPuertaEnlace());
            }
            this.setNombre(dispositivo.getNombre());
            for (int i = 0; i < this.getPuertos().size(); i++) {
                System.out.println("Puerto " + i);
                String resultado = this.getPuertos().get(i).configurar(dispositivo.getPuertos().get(i));
                if (!resultado.equals(Constantes.PUERTO_CONFIGURADO_CORRECTAMENTE)) {
                    return "Error puerto " + i + ": " + resultado;
                }
            }
            for (DispositivoGrafico dispositivoGrafico : Controlador.graficos) {
                if (dispositivoGrafico.getDispositivo() == this) {
                    System.out.println("encontrado");
                    dispositivoGrafico.mostrarNombre();
                    dispositivoGrafico.getImagen().setToolTipText(this.toolTip());
                    break;
                }
            }
            Controlador.actualizarTooltips();
            
            return Constantes.DISPOSITIVO_CONFIGURADO_CORRECTAMENTE;
            
        } else {
            return Constantes.ERROR_DISPOSITIVO_NOMBRE_EXISTE;
        }
    }
    //</editor-fold>
}
