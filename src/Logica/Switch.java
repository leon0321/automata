/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Controlador.Controlador;
import java.util.ArrayList;

/**
 *
 * @author ACER
 */
public class Switch extends Dispositivo {

    private static int contSwitch = 0;

    public Switch(int x, int y) {
        super();
        super.setNombre("Switch" + contSwitch++);
        super.setX(x);
        super.setY(y);
        ArrayList<Puerto> puertos = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            Puerto puerto = new Puerto("Puerto"+i);
            puertos.add(puerto);
        }
        super.setPuertos(puertos);
    }

    public Switch() {
        super();
        ArrayList<Puerto> puertos = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            Puerto puerto = new Puerto("Puerto"+i);
            puertos.add(puerto);
        }
        super.setPuertos(puertos);
    }

    @Override
    public String Propiedades() {

        String a = "";

        a = "<b>Nombre de dispositivo:</b> " + getNombre();
        for (int i = 0; i < getPuertos().size(); i++) {
            a += "<br><b>Puerto " + i + ": </b>";
            for (Linea linea : Controlador.lineas) {
                if (linea.getpFinal() == getPuertos().get(i)) {
                    a += linea.getInicial().getNombre();
                    break;
                }
                if (linea.getpInicial() == getPuertos().get(i)) {
                    a += linea.getFinall().getNombre();
                    break;
                }
            }
        }

        return a;
    }

    @Override
    public String toolTip() {

        String a = "";

        a = "<html><b>Nombre de dispositivo:</b> " + getNombre();
        for (int i = 0; i < getPuertos().size(); i++) {
            a += "<br><b>Puerto " + i + ": </b>";
            for (Linea linea : Controlador.lineas) {
                if (linea.getpFinal() == getPuertos().get(i)) {
                    a += linea.getInicial().getNombre();
                    break;
                }
                if (linea.getpInicial() == getPuertos().get(i)) {
                    a += linea.getFinall().getNombre();
                    break;
                }
            }

        }
        a += "</html>";
        return a;
    }

    @Override
    public String toString() {
        return this.getNombre();
    }
}
