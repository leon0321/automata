package Logica;

import java.io.Serializable;

/**
 * @author jhonny
 */
public class Linea implements Serializable{

    private Dispositivo inicial;
    private Dispositivo finall;
    private Puerto pInicial;
    private Puerto pFinal;
    private int peso;
    private int x1;
    private int y1;
    private int x2;
    private int y2;
    boolean pintar;

    public Linea(Dispositivo inicial, Dispositivo finall) {
        this.inicial = inicial;
        this.finall = finall;
        peso = 0;
        pintar = false;
    }

    public Linea(Dispositivo inicial, Dispositivo finall, int peso) {
        this.inicial = inicial;
        this.finall = finall;
        this.peso = peso;
    }

    public Linea() {
    }

    public Puerto getpFinal() {
        return pFinal;
    }

    public void setpFinal(Puerto pFinal) {
        this.pFinal = pFinal;
    }

    public Puerto getpInicial() {
        return pInicial;
    }

    public void setpInicial(Puerto pInicial) {
        this.pInicial = pInicial;
    }
    public boolean isPintar() {
        return pintar;
    }

    public void setPintar(boolean pintar) {
        this.pintar = pintar;
    }
    

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }
    

    public Dispositivo getFinall() {
        return finall;
    }

    public void setFinall(Dispositivo finall) {
        this.finall = finall;
    }

    public Dispositivo getInicial() {
        return inicial;
    }

    public void setInicial(Dispositivo inicial) {
        this.inicial = inicial;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    @Override
    public String toString() {
        return "(" + getInicial() + "," + getFinall() + ")";
    }
    
}
