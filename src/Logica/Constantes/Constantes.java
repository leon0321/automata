/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.Constantes;

/**
 *
 * @author Andres Betin && Leonardo Ortega Hernandez
 */
public class Constantes {

    public static final int CREAR_NODO = 1;
    public static final int CREAR_ROUTER = 2;
    public static final int CREAR_SWICH = 3;
    //Comandos disponibles
    public static final String COMANDO_C_NODE_1 = "CREATE NODE";
    public static final String COMANDO_C_NODE_2 = "create node";
    public static final String COMANDO_C_ROUTER_1 = "CREATE ROUTER";
    public static final String COMANDO_C_ROUTER_2 = "create router";
    public static final String COMANDO_C_SWITCH_1 = "CREATE SWITCH";
    public static final String COMANDO_C_SWITCH_2 = "create switch";
    public static final String COMANDO_DELETE_1 = "DELETE";
    public static final String COMANDO_DELETE_2 = "delete";
    public static final String COMANDO_CREATE_RELATION_1 = "CREATE RELATION";
    public static final String COMANDO_CREATE_RELATION_2 = "create relation";
    public static final String COMANDO_DELETE_RELATION_1 = "DELETE RELATION";
    public static final String COMANDO_DELETE_RELATION_2 = "delete relation";
    public static final String COMANDO_PROPERTIES_1 = "PROPERTIES";
    public static final String COMANDO_PROPERTIES_2 = "properties";
    public static final String COMANDO_SEND_1 = "SEND";
    public static final String COMANDO_SEND_2 = "send";
    public static final String COMANDO_EXIT_1 = "EXIT";
    public static final String COMANDO_EXIT_2 = "exit";
    //Mensaje de error de comandos.
    public static final String ERROR_COMANDO = " no se reconoce como un comando ejecutable";
    public static final String ERROR_CONSOLA = " no se reconoce como un comando ejecutable";
    public static final String ERROR_CONFIGURACION = "El dispositivo no corresponde con la clase especificada";
    //Configuracion Correcta
    public static final String DISPOSITIVO_CONFIGURADO_CORRECTAMENTE = "Dispositivo Configurado Correctamente";
    public static final String PUERTO_CONFIGURADO_CORRECTAMENTE = "Puerto Configurado Correctamente";
    //Error dispositivo nombre existe
    public static final String ERROR_DISPOSITIVO_NOMBRE_EXISTE = "El nombre ingresado ya existe";
    //Errores de puertos
    public static final String ERROR_IP_EXISTE = "La ip ingresada ya existe";
    public static final String ERROR_IP_INVALIDA = "IP inválida";
    public static final String ERROR_MASCARA_INVALIDA = "Máscara inválida";
    //Error General configuración Dispositivos
    public static final String ERROR_GENERAL_CONFIGURACION_DISPOSITIVO = "Existen Problemas al configurar el dispositivo";
    public static final String ERROR_CAMPOS_VACIOS = "Existen campos vacíos";
    //Errores envio
    public static final String ERROR_NODOS_DESCONFIGURADOS = "Por lo menos uno de los nodos de la red se encuentra desconfigurado";
    public static final String ERROR_NODOS_INALCANZABLES = "No existe un camino configurado entre los nodos";
    //Errores de puerta de enlace
    public static final String ERROR_NO_PUERTA_ENLACE_INICIAL = "No hay puerta de enlace configurada para el dispositivo inicial";
    public static final String ERROR_NO_PUERTA_ENLACE_FINAL = "No hay puerta de enlace configurada para el dispositivo final";
    //Error de simulacio
    public static final String ERROR_SIMULANDO = "Actualmente se esta simulando la comunicación. No se aceptan comandos";
    //RIP
    public static final String COMANDO_RIP = "router rip";
    public static final String ERROR_RIP = "Ejecute la configuracion rip de los puertos de todos los routers";
    //protocolos
    /**
     * Es el indicador del protocolo OSPF su valor numerico es el numero 0
     */
    public static final int OSPF = 0;
    /**
     * Es el indicador del protocolo BGP su valor numerico es el numero 1
     */
    public static final int BGP = 1;
    /**
     * Es el indicador del protocolo RIP version 1 su valor numerico es el
     * numero 2
     */
    public static final int RIP1 = 2;
    /**
     * Es el indicador del protocolo RIP version 2 su valor numerico es el
     * numero 3
     */
    public static final int RIP2 = 3;
    /**
     * Es el indicador del protocolo FLOODING su valor numerico es el numero 4
     */
    public static final int FLOODING = 4;
    /**
     * Es el indicador del protocolo Vector Distancia su valor numerico es el
     * numero 5
     */
    public static final int VD = 5;
}
