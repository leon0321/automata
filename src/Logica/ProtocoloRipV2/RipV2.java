/**
 * esta clase contiene el metodo de enrutamiento del protocolo rip en su version
 * 2
 */
package Logica.ProtocoloRipV2;

import Controlador.Controlador;
import Logica.Constantes.Constantes;
import Logica.Dispositivo;
import Logica.DispositivoGrafico;
import Logica.Nodo;
import static Logica.ProtocoloBGP.BGP.createPaths;
import static Logica.ProtocoloBGP.BGP.filtrar;
import Logica.ProtocoloBGP.Rute;
import Logica.ProtocoloOSPF.OSPF;
import Logica.Router;
import Logica.tools.Enlace;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author TavoRojas
 */
public class RipV2 implements Serializable {

    /**
     * selecciona cuál es la ruta mas corta entre un par de rutas ingresadas
     * esta selección se hace debido a que en el protocolo rip se enviará el
     * paquete por el camino con el menor número de saltos posibles.
     *
     * @param rutea primera ruta
     * @param ruteb segunda ruta
     */
    public static Rute seleccionRuta(Rute rutea, Rute ruteb) {
        if (rutea.size() <= ruteb.size()) {
            return rutea;
        } else {
            return ruteb;
        }

    }

    /**
     * Elige el mejor camino entre un un par de nodos utilizando los criterio
     * RIP V2 Nota: esta clase ha sido trasladada desde del protocolo BGP,
     * modificandole parte de la estructura, la seleccion de la mejor ruta a
     * tomar y algunos cambios en las validaciones existentes.
     *
     * @param a Dispocitivo inicial
     * @param b Dispocitivo final
     * @param cantidadPaquetes Cantidad de paquetes a enviar
     * @return un lista de dispocitivos visuales que son unsaddos por la
     * interface grafica //
     */
    public static ArrayList<DispositivoGrafico> best(Dispositivo a, Dispositivo b, int cantidadPaquetes) {
        ArrayList paths = new ArrayList();
        ArrayList pathsIGP = new ArrayList();
        Rute path = new Rute();
        path.add(a);
        int contRouter = 0;

        /**
         * se crea una instancia de la clase ospf para hacer algunas
         * validaciones de los nodos entre los cuales se quiere establecer la
         * comunicacion
         */
        OSPF com = new OSPF(Controlador.allDispositivos);
        String salida = com.envio((Nodo) a, (Nodo) b, cantidadPaquetes);
        Controlador.salida = salida;
        
        // se utiliza este metodo de las clase BGP para creacion de caminos entre nodos
        createPaths(a, b, path, paths);
        if (paths.isEmpty()) {
            Controlador.salida = Constantes.ERROR_NODOS_DESCONFIGURADOS;
            return null;
        }
        /**
         * cuando se genera solo un camino
         */
        if (paths.size() == 1) {

            /*Se cuenta cuantos Routers tiene la ruta para verificar 
             que el num de saltos es menor a 16*/
            for (int i = 0; i < paths.size(); i++) {
                if (paths.get(i).getClass() == Router.class) {
                    contRouter++;
                }
            }
            
            //se verifica que el numero de saltos entre nodos sea menor a 16
            if (contRouter >= 16) {
                Controlador.salida = "Nodos Inalcanzables por numero de Saltos mayor a 15";
                return null;
            } else {
                if (salida.equals(Constantes.ERROR_NODOS_INALCANZABLES)) {
                    //controlador.mostrarEnConsola(salida, "red");
                    return null;
                } else if (salida.equals(Constantes.ERROR_NO_PUERTA_ENLACE_INICIAL) || salida.equals(Constantes.ERROR_NO_PUERTA_ENLACE_FINAL)) {
                    //controlador.mostrarEnConsola(salida, "red");
                    return null;
                } else if (salida.equals(Constantes.ERROR_NODOS_DESCONFIGURADOS)) {
                    //controlador.mostrarEnConsola(salida, "red");
                    return null;
                } else {
                    Controlador.salida = "Todo bien<br>Ruta: " + paths.get(0);
                    return Enlace.caminoGrafico((Rute) paths.get(0));
                }
            }
        }

        /**
         * cuando se generan mas de un camino
         */
        for (int i = 0; i < paths.size(); i++) {
            Rute p = filtrar((Rute) paths.get(i));
            if (p.isEmpty()) {
                pathsIGP.add(paths.get(i));
                paths.remove(i);
            }
        }
        if (pathsIGP.isEmpty()) {
            Rute bestpath = (Rute) paths.get(0);
            for (int i = 0; i < paths.size() - 1; i++) {
                Rute r2 = (Rute) paths.get(1 + i);
                bestpath = seleccionRuta(bestpath, r2);
            }

            /*Se cuenta cuantos Routers tiene la ruta mas corta para verificar 
             que el num de saltos es menor a 16*/
            for (int i = 0; i < bestpath.size(); i++) {
                if (bestpath.get(i).getClass() == Router.class) {
                    contRouter++;
                }
            }

            if (contRouter >= 16) {
                Controlador.salida = "Nodos Inalcanzables por numero de Saltos mayor a 15";
                return null;
            } else {
                if (salida.equals(Constantes.ERROR_NODOS_INALCANZABLES)) {
                    //controlador.mostrarEnConsola(salida, "red");
                    return null;
                } else if (salida.equals(Constantes.ERROR_NO_PUERTA_ENLACE_INICIAL) || salida.equals(Constantes.ERROR_NO_PUERTA_ENLACE_FINAL)) {
                    //controlador.mostrarEnConsola(salida, "red");
                    return null;
                } else if (salida.equals(Constantes.ERROR_NODOS_DESCONFIGURADOS)) {
                    //controlador.mostrarEnConsola(salida, "red");
                    return null;
                } else {
                    //controlador.mostrarEnConsola("Todo bien<br>Ruta" + bestpath, "green");
                    return Enlace.caminoGrafico(bestpath);
                }
            }
        }
        Controlador.salida = Constantes.ERROR_NODOS_DESCONFIGURADOS;
        return null;
    }
}
