/**
 *
 * En esta clase podremos encontrar basicamente dos metodos que son:
 * imprimirTabla y CrearTabla En cada método puedes encontrar descripcion de lo
 * que hace cada uno de ellos
 *
 */
package Logica.ProtocoloRipV2;

import Logica.Dispositivo;
import Logica.Puerto;
import Logica.Router;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author TavoRojas
 */
public class TablaRipV2 implements Serializable {

    public TablaRipV2() {
    }

    /**
     *
     * Método utilizado para mostrar la tabla de enrutamiento de los routers por
     * medio de la terminal (shell)
     *
     * @param r roruter desde donde se encuentra abierta la terminal (shell)
     */
    public String imprimirTabla(Router r) {
        String ip_conectadas; //ip conectadas directamente al router
        String ip_rip; //ip conectadas por rip al router
        String ip; //cadena a retornar o imprimir en la ventana del shell
        ArrayList<String> ips; //array de ip conectadas directamente al router
        ArrayList<String> ips2; //array de ip no conectadas directamente al router
        ArrayList<String> mascaraconectada; //array de las mascaras correspondientes a las ip conectadas
        ArrayList<String> mascararip; //array de las mascaras correspondientes a las ip no conectadas
        String aux1; //auxiliar para llenar array de ip conectadas directamente
        String aux2; //auxiliar para llenar array de ip conectadas por rip
        Puerto p = new Puerto(); //instancia de puerto para ver sus respectivas ip

        int auxentero;

        ips = new ArrayList<>();
        ips2 = new ArrayList<>();
        mascaraconectada = new ArrayList<>();
        mascararip = new ArrayList<>();

        //llena arraylist de conectadas directamente
        for (int i = 0; i < r.getIpconet().size(); i++) {
            aux1 = p.calcularRed(r.getIpconet().get(i), r.getMaskconet().get(i));
            ips.add((String) aux1);
            mascaraconectada.add((String) r.getMaskconet().get(i));
        }

        //llena arraylist de conectadas por rip
        for (int i = 0; i < r.getIprip().size(); i++) {
            aux2 = p.calcularRed(r.getIprip().get(i), r.getMaskrip().get(i));
            ips2.add((String) aux2);
            mascararip.add((String) r.getMaskrip().get(i));
        }

        if (ips.size() > ips2.size()) {
            auxentero = ips.size() - ips2.size();
            for (int f = 0; f < auxentero; f++) {
                ips2.add(" ");
                mascararip.add(" ");
            }

        }
        if (ips.size() < ips2.size()) {
            auxentero = ips2.size() - ips.size();
            for (int f = 0; f < auxentero; f++) {
                ips.add(" ");
                mascaraconectada.add(" ");
            }

        }

        //remover ip conectadas por rip que estan conectadas directamente
        if (!ips.isEmpty() && !ips2.isEmpty()) {
            //si conectada mayor que desconectada
            if (ips.size() > ips2.size()) {
                for (int i = 0; i < ips2.size(); i++) {
                    for (int j = 0; j < ips.size(); j++) {
                        if (ips2.get(i).equalsIgnoreCase(ips.get(j))) {
                            ips2.remove(ips.get(j));
                            mascararip.remove(mascaraconectada.get(j));
                        }
                    }
                }
            } else {
                //si desconectadas mayor que conectadas
                for (int i = 0; i < ips.size(); i++) {
                    for (int j = 0; j < ips2.size(); j++) {
                        if (ips.get(i).equalsIgnoreCase(ips2.get(j))) {
                            ips2.remove(ips2.get(j));
                            mascararip.remove(mascaraconectada.get(j));
                        }
                    }
                }
            }
        }

        if (!ips2.isEmpty()) {
            for (int i = 0; i < ips2.size() - 1; i++) {
                for (int j = i + 1; j < ips2.size(); j++) {
                    if (ips2.get(i).equals(ips2.get(j))) {
                        ips2.remove(ips2.get(j));
                        mascararip.remove(mascararip.get(j));
                        ips2.add(" ");
                        mascararip.add(" ");

                    }
                }
            }

        }
//imprime conectadas directamente
        ip_conectadas = "";
        for (int i = 0; i < ips.size(); i++) {

            ip_conectadas = "<tr>" + ip_conectadas + "</tr><td>" + ips.get(i) + "</td><td>" + mascaraconectada.get(i) + "</td>";
        }
//imprime conectadas por rip
        ip_rip = "";
        for (int i = 0; i < ips2.size(); i++) {

            ip_rip = "<tr>" + ip_rip + "</tr><td>" + ips2.get(i) + "</td><td>" + mascararip.get(i) + "</td>";

        }
//lo que se va a mostrar en la terminal (shell)
        ip = "<table color=white>"
                + "<tr>"
                + "<td class=\"greenwhite\" >Ip Conectadas</td><td class=\"greenwhite\">Mascara</td></tr>"
                + ip_conectadas
                + "<tr>"
                + "<td class=\"greenwhite\" >Ip por RIP</td><td class=\"greenwhite\">Mascara</td></tr>"
                + ip_rip
                + "</table>";
        return ip;
    }

    /**
     * El método Crar tabla, basicamente lo que hace es crear arraylists que
     * contengan las direcciones ip que se conectan a cada uno de los routers
     * presentes en la red que se ha creado, al igual que las redes que conocerá
     * a través de sus vecinos
     */
    public void CrearTabla() {
        ArrayList<Router> router; //totalidad de routers presentes en la red
        ArrayList<Dispositivo> disp; //almacenaremos aqui todos los dispositivos de la red
        System.out.println("Creando Tabla Rip 2");
        ArrayList<String> ipconect; //ip conectadas directamente en cada uno de los routers
        ArrayList<String> iprip; //ip que son alcanzables por los routers de la redy que estos no tienen conectadas fisicamente 
        ArrayList<String> maskconect;//mascaras correspondientes a cada una de las ip conectadas directamente en cada uno de los routers
        ArrayList<String> maskrip;//mascaras correspondientes a cada una de las ip que son alcanzables por los routers de la redy que estos no tienen conectadas fisicamente 

        disp = Controlador.Controlador.allDispositivos;
        router = new ArrayList<>();

        //se llena el arraylist con los routers que se encuentran en la red
        for (int i = 0; i < disp.size(); i++) {
            if (disp.get(i).getClass() == Router.class) {
                router.add((Router) disp.get(i));
            }
        }
        //se llenan los arraylists de ip y mascaras (conectadas y no conectadas directamente)
        //para cada uno de los routers presentes en la red
        for (int i = 0; i < router.size(); i++) {

            ipconect = new ArrayList<>();
            maskconect = new ArrayList<>();
            iprip = new ArrayList<>();
            maskrip = new ArrayList<>();

            //llenar ip conectadas y mascaras por router
            for (int j = 0; j < 4; j++) {
                if (!router.get(i).getPuertos().get(j).isDisponible()) {
                    ipconect.add(router.get(i).getPuertos().get(j).getDireccion().getIp());
                    maskconect.add(router.get(i).getPuertos().get(j).getDireccion().getMascara());
                }
            }
            //llenado de ip y mascaras que no estan conectadas a los puertos del router en cuestion presentes en la red
            for (int p = 0; p < router.size(); p++) {
                if (!router.get(p).getNombre().equals(router.get(i).getNombre())) {
                    for (int q = 0; q < 4; q++) {
                        if (!router.get(p).getPuertos().get(q).isDisponible()) {
                            iprip.add(router.get(p).getPuertos().get(q).getDireccion().getIp());
                            maskrip.add(router.get(p).getPuertos().get(q).getDireccion().getMascara());
                        }
                    }
                }
            }

            //se va actualizando la tabla en el router
            router.get(i).setTablarip2(ipconect, iprip, maskconect, maskrip);
        }
        System.out.println("Fin Creando Tabla Rip 2");
    }
}
