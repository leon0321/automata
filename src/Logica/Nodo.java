package Logica;

import java.util.ArrayList;

/**
 *
 * @author ACER
 */
public class Nodo extends Dispositivo {

    private static int contNodos = 0;
    private String puertaEnlace;

    public Nodo(int x, int y) {

        super();
        super.setNombre("Nodo" + contNodos++);
        super.setX(x);
        super.setY(y);
        ArrayList<Puerto> puertos = new ArrayList<>();
        for (int i = 0; i < 1; i++) {
            Puerto puerto = new Puerto("Puerto"+i);
            puertos.add(puerto);
        }
        super.setPuertos(puertos);
        puertaEnlace = "";
    }

    public String getPuertaEnlace() {
        return puertaEnlace;
    }

    public void setPuertaEnlace(String puertaEnlace) {
        this.puertaEnlace = puertaEnlace;
    }

    /**
     * Este Inicializador se utiliza para los comandos configure
     */
    public Nodo() {
        super();
        ArrayList<Puerto> puertos = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            Puerto puerto = new Puerto("Puerto"+i);
            puertos.add(puerto);
        }
        super.setPuertos(puertos);
        puertaEnlace = "";
    }

    @Override
    public String Propiedades() {
        String a = "";
        a = "<br><b>Nombre de dispositivo:</b> " + getNombre();
        a += "<br><b>Dirección IP:</b>" + getPuertos().get(0).getDireccion().getIp();
        a += "<br><b>Máscara de red:</b> " + getPuertos().get(0).getDireccion().getMascara();
        a += "<br><b>Puerta de Enlace:</b> " + getPuertaEnlace();
        return a;
    }

    @Override
    public String toolTip() {
        String a = "";
        a = "<html><b>Nombre de dispositivo:</b> " + getNombre();
        a += "<br><b>Dirección IP:</b>" + getPuertos().get(0).getDireccion().getIp();
        a += "<br><b>Máscara de red:</b> " + getPuertos().get(0).getDireccion().getMascara();
        a += "<br><b>Puerta de Enlace:</b> " + getPuertaEnlace() + "</html>";

        return a;
    }

    @Override
    public String toString() {
        if (this.getPuertos().get(0).getDireccion().getIp().equals("")) {
            return this.getNombre();
        } else {
            return this.getPuertos().get(0).getDireccion().getIp();
        }
    }
}
