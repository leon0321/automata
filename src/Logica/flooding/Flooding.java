/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica.flooding;

import Logica.Dispositivo;
import Logica.ProtocoloBGP.BGP;
import static Logica.ProtocoloBGP.BGP.createPaths;
import Logica.ProtocoloBGP.Rute;
import java.util.ArrayList;

/**
 *
 * @author Carlos Tatis Gordon
 */
public class Flooding {

    public Flooding() {
    }

    public static ArrayList enviarVecinos(Dispositivo d0, Dispositivo df, ArrayList list) {
        list = BGP.filtrarNodos(list);
        ArrayList paths = new ArrayList<>();
        list.remove(d0);

        for (int j = 0; j < list.size(); j++) {
            Dispositivo d1 = (Dispositivo) list.get(j);
            if (d0 != d1) {
                Rute routers = new Rute();
                routers.add(d0);
                ArrayList paths1 = new ArrayList<>();
                createPaths(d0, d1, routers, paths1);
                //if ((paths1.get(paths1.size() - 1) == df)) {
                    paths.addAll(paths1);
               // }
            }
        }
        System.out.println("cantidad de caminos " + paths.size());
        return paths;
    }
}
